package businesscomponents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.apache.xpath.operations.Div;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.internal.WrapsDriver;
//import org.openqa.selenium.internal.seleniumemulation.WaitForPageToLoad;
import org.openqa.selenium.lift.find.DivFinder;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.css.sac.SelectorList;
import com.cognizant.framework.Status;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

/**
 * Functional Components class
 * 
 * @author Cognizant
 */
public class FunctionalComponents extends ReusableLibrary {
	Actions actions = new Actions(driver);
	/**
	 * Constructor to initialize the component library
	 * 
	 * @param scriptHelper
	 *            The {@link ScriptHelper} object passed from the
	 *            {@link DriverScript}
	 */
	protected static Set setOfOldHandles = null;
	protected static Set setOfNewHandles = null;
	static WebElement webelement;

	public FunctionalComponents(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	// Saves Old Handles
	public void saveOldHandles(WebDriver driver) {
		if (setOfOldHandles != null) {
			setOfOldHandles.clear();
		}
		setOfOldHandles = driver.getWindowHandles(); // Save all the browser
														// window ID's
		// report.updateTestLog("hai", ""+setOfOldHandles.size(), Status.DONE);
	}

	// Saves New Handles
	public void saveNewHandles(WebDriver driver) {
		if (setOfNewHandles != null) {
			setOfNewHandles.clear();
		}
		setOfNewHandles = driver.getWindowHandles(); // Save all the browser
														// window ID's

	}

	// Focus on new view
	public void focusOnNewView(WebDriver driver) {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// To do Auto-generated catch block
			e.printStackTrace();
		}
		if (setOfNewHandles != null) {
			setOfNewHandles.removeAll(setOfOldHandles);
			// this method removeAll() take one set and puts it in another set
			// and if there are same
			// positions it will erase them and leaves only that are not equals
		} else {

			System.out
					.println("setOfNewHandles is null. Can't compare old and new handles. New handle may have not enough time to load and save. Maybe you should add some time to load new window by adding Thread.Sleep(3000); - wait for 3 second ");
		}

		if (!setOfNewHandles.isEmpty()) {
			String newWindowHandle = (String) setOfNewHandles.iterator().next();
			// Finding current window
			driver.switchTo().window(newWindowHandle);
			// Focusing on current window.
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			driver.manage().window().maximize();

		}
	}

	public  void setAttribute(WebElement element, String attributeName, String value) 
	{         WrapsDriver wrappedElement = (WrapsDriver) element; 
	        
	       JavascriptExecutor driver1 = (JavascriptExecutor) wrappedElement.getWrappedDriver(); 
	        driver1.executeScript("arguments[0].setAttribute(arguments[1], arguments[2])", element, attributeName, value); 
	} 
	
	// Waits for till the element is displayed
	public void waitForElementDisplay(String elementId)
			throws InterruptedException {
		for (int i = 0; i < 600; i++) {
			try {
				if (driver.findElement(By.id(elementId)).isDisplayed())
					break;
				else
					Thread.sleep(100);
			} catch (Exception e) {
				Thread.sleep(100);
			}
		}
	}

	// Waits till the page is loaded
	public void waitForLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(pageLoadCondition);
	}

	// SelectOptionFromListByVisibleText
	public void SelectOptionFromListByVisibleText(String ElementIdentifier,
			String OptionText) {
		WebElement Dropdown = driver.findElement(By.id(ElementIdentifier));
		Select objSelect = new Select(Dropdown);
		objSelect.selectByVisibleText(OptionText);
	}

	// Clean up
	public void cleanUp() {
		driver.quit();
	}

	// Identification of Text
	public WebElement ElementPresent(String Identifier) {
		// String Validation = null;
		try {
			WebElement Element = driver.findElement(By.xpath(Identifier));
			return Element;
		} catch (Exception e) {
			return null;
		}
	}

	// Condition to verify the element is present or not
	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			// If element exists return true
			return true;
		} catch (Exception e) {
			// If element not exists return False.
			return false;

		}

	}

	// Select value from unordered list
	public void selectValueFromUnorderedList(WebElement unorderedList,
			final String value) {
		List<WebElement> options = unorderedList.findElements(By.tagName("li"));

		for (WebElement option : options) {
			if (value.equals(option.getText())) {
				option.click();
				break;
			}
		}
	}

	// Login
	// *[@id='hmh-header']/div/div[1]/a
	public void homepage() {
		driver.findElement(By.xpath("//*[@id='main']/header/nav/ul[2]/li[1]/a")).click();
		waitForLoad(driver);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void login() {

		String userName = dataTable.getData("General_Data", "Username");
		String password = dataTable.getData("General_Data", "Password");

		homepage();
		driver.findElement(By.xpath("//*[@id='greyheaderbar_0_signUpLink']/span")).click();
		waitForLoad(driver);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("txtSiEmail")).sendKeys(userName);
		driver.findElement(By.id("pwdSiPassword")).sendKeys(password);
		driver.findElement(By.id("lbtSiSignin")).click();
		waitForLoad(driver);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.navigate().to("javascript:document.getElementById('overridelink').click()");  
		if (isElementPresent(By.id("mainTitle"))) {
			report.updateTestLog("SSl","Navigated to SSL error page" ,Status.PASS);
			
		} 
		else if (isElementPresent(By.xpath("//*[@id='form_signin']/span"))) {
			String error = driver.findElement(
					By.xpath("//*[@id='form_signin']/span")).getText();
			report.updateTestLog("Login","Invalid login crendentials & the error message - " + error,Status.PASS);
		} 
		else if (isElementPresent(By.xpath("//*[@id='signedOut']/li[1]/a/span"))) {
			String user = driver.findElement(
					By.xpath("//*[@id='signedOut']/li[1]/a/span")).getText();
			String[] updatedUser=user.split(",");
			String usershortname= updatedUser[1]; 
			report.updateTestLog("Login", "The user name is - " + usershortname,
					Status.PASS);
			report.updateTestLog("Login", "Enter login credentials: "
					+ "Username = " + userName + ", " + "Password = "
					+ password, Status.PASS);
					}
		// driver.findElement(By.xpath("//a[contains(text(),'SIGN OUT')]")).click();
	}

	public void repeatedOrders() throws InterruptedException {
		addToCart();
		persCheckOut();
	}

	public void addToCart() throws InterruptedException {
		String ISBN = dataTable.getData("General_Data", "ISBN");
		waitForLoad(driver);
		driver.findElement(By.id("searchEntry")).sendKeys(ISBN);
		driver.findElement(By.xpath("//*[@id='header_0_sectionSearch']/div/input")).click();
		waitForLoad(driver);
		driver.findElement(By.xpath("//*[@id='main']/section/div/section/article/section[5]/div/div/div/figure/a")).click();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (isElementPresent(By.xpath("//*[@id='body1_0_productcartDiv']/p[2]/a"))){
			driver.findElement(By.xpath("//*[@id='body1_0_productcartDiv']/p[2]/a")).click();
		}
		 else
		 {
			driver.findElement(By.xpath("//*[@id='body1_0_quantitySpan']/p[2]/a")).click();
		}
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)
		Thread.sleep(3000);
		if (driver.findElement(By.id("relogin")).isDisplayed())
				{
			report.updateTestLog("Restricted Product", "User cannot add the restricted product",Status.PASS);
				}
		else if (driver.findElement(By.id("modal")).isDisplayed())
		{
			report.updateTestLog("Product inventory", "Inventory for the product is low",Status.WARNING);
		}
		else  
			{	
				
				try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {
							e.printStackTrace();
						}
				report.updateTestLog("Item added", "Cart Updated",Status.PASS);
		 	        
					try {
							Thread.sleep(3500);
						} catch (InterruptedException e) {
								e.printStackTrace();
							}	

		// for(int i=1; i<= 5; i++)
		// { */
						driver.findElement(By.xpath("//*[@id='cartButton']/a")).click();
							waitForElementDisplay("viewCartAnchorDown");
								String URL= driver.getCurrentUrl();
									if (driver.findElement(By.id("viewCartAnchorDown")).isEnabled()) 
										{
											driver.findElement(By.id("viewCartAnchorDown")).click();
											}
									else if (URL.contains("Stg"))
									{
										driver.navigate().to("http://hmhco-v1.stg.techspa.com/hmhstorefront/cart") ;
									}	
										else if (URL.contains("Pre"))
											{
											driver.navigate().to("http://hmhco-v1.Pre.techspa.com/hmhstorefront/cart") ;
											}	
										else if (URL.contains("Dev"))
										{
											driver.navigate().to("http://hmhco-v1.Dev.techspa.com/hmhstorefront/cart") ;
										}	
									try {
										Thread.sleep(5000);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
									waitForElementDisplay("proceedToCheckout");
		}
	
	report.updateTestLog("Add to cart", ISBN + " added sucesfully",Status.PASS);
	}

	// To update the cart count
	private int cartCount() {

		String cartCountStr = driver.findElement(
				By.xpath("//span[@class='cartCount']")).getText();
		String[] cartCountStrings = cartCountStr.split("\\(");
		String[] cartCountStrings1 = cartCountStrings[1].split("\\)");
		int cartCount = Integer.parseInt(cartCountStrings1[0].trim());
		// TODO Auto-generated method stub
		return cartCount;
	}

	public void checkOutInst() {
		String userName = dataTable.getData("General_Data", "Username");
		
		driver.findElement(By.id("proceedToCheckout")).click();
		
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		String updateURL = driver.getCurrentUrl();
		report.updateTestLog("URL", updateURL, Status.SCREENSHOT);
		if (updateURL.contains("updateCart"))
		{
			driver.navigate().to("https://hmhco-v1.stg.techspa.com/hmhstorefront/checkout/multi/checkout-steps");
		}
		else if (isElementPresent(By.id("deliverySubmitBtn")))
			{
			report.updateTestLog("Shipping accordion", "Shipping accordion loaded",Status.PASS);
			}
		driver.findElement(By.id("deliverySubmitBtn")).click();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		if (isElementPresent(By.id("access_fname"))){
			digital();
		}
		else if (isElementPresent(By.id("contact_email")))
		{
			report.updateTestLog("access code", "Access code accordion not present",
					Status.PASS);	
		}
		driver.findElement(By.id("contact_email")).clear();
		driver.findElement(By.id("contact_email")).sendKeys(userName);
		driver.findElement(By.id("contact_confirm_email")).sendKeys(userName);
		driver.findElement(By.id("contactCodeSubmitBtn")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		report.updateTestLog("Account Contact", "Account Contact accordion successfully loaded",
				Status.PASS);
	}
	
	public void placeOrderPOInst() throws InterruptedException {
		checkOutInst();
		List<WebElement> paymentTypeList = driver.findElements(By.id("paymentTypeInstPO"));
		if(paymentTypeList != null && !paymentTypeList.isEmpty()){
			if(paymentTypeList.get(1).isSelected()){
				if(paymentTypeList.get(1).getAttribute("value").equals("CARD")) {
					paymentTypeList.get(0).click();
					Thread.sleep(2000);
				}
			}
		}
		else
		{
			report.updateTestLog("Payment", "User already selected the Purchase order as payment option",
					Status.PASS);
		}
		driver.findElement(By.id("po_number")).sendKeys("14852637485cf");
		driver.findElement(By.id("po_authorizer")).sendKeys("chandan");
		driver.findElement(By.id("paymentSubmitBtn")).click();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		waitForElementDisplay("reviewAgree");
		driver.findElement(By.id("reviewAgree")).click();
		driver.findElement(By.id("placeOrderLink")).click();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		//WebElement orderPage = driver.findElement(By.xpath("//*[@id='bodyHtml']/section[3]/div/p[1]/strong/span"));
		if (isElementPresent(By
				.xpath("//*[@id='main']/div[1]/div/section[1]/div[3]/p[1]/strong/span"))) {
			String order = driver.findElement(By.xpath("//*[@id='main']/div[1]/div/section[1]/div[3]/p[1]/strong/span")).getText();
			report.updateTestLog("Place order","Order is placed successfully and the order number is"+ " -  " + order, Status.PASS);
		} else {
			report.updateTestLog("Place Order", "The order is failed",
					Status.WARNING);
		}

	}

	public void placeOrderCCInst() throws InterruptedException {
		checkOutInst();
		List<WebElement> paymentTypeList = driver.findElements(By.id("paymentTypeInstPO"));
		if(paymentTypeList != null && !paymentTypeList.isEmpty()){
			if(paymentTypeList.get(0).isSelected()){
				if(paymentTypeList.get(0).getAttribute("value").equals("ACCOUNT")){
					paymentTypeList.get(1).click();
					Thread.sleep(2000);
				}
			}
		}
		else
		{
			report.updateTestLog("Payment", "User already selected the Purchase order as payment option",
					Status.PASS);
		}
//		WebDriverWait wait = new WebDriverWait(driver,50); 
//		html/body/div[2]/section/section[1]/section[2]/article/ul/li[4]/div[2]/form/div[2]/div/div/div[2]/p[2]/input   wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[2]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[2]/input"))).click(); 
		driver.findElement(By.id("payment.nameOnCard")).clear();
		driver.findElement(By.id("cardNumberInput")).clear();
		driver.findElement(By.id("billing_zip")).clear();
		driver.findElement(By.id("payment.cvv")).clear();
		driver.findElement(By.id("billing_instname")).clear();
		driver.findElement(By.id("billing_address1")).clear();
		driver.findElement(By.id("billing_city")).clear();
		driver.findElement(By.id("payment.nameOnCard")).sendKeys("Chandan");
		driver.findElement(By.id("cardNumberInput")).sendKeys("4055011111111111");
		driver.findElement(By.id("payment.cvv")).sendKeys("123");
		new Select(driver.findElement(By.id("payment.expirymonth"))).selectByValue("9");
		new Select(driver.findElement(By.id("payment.expiryYear"))).selectByValue("2019");
		driver.findElement(By.id("billing_instname")).sendKeys("Chandan");
		driver.findElement(By.id("billing_address1")).sendKeys("22 Berkley St");
		driver.findElement(By.id("billing_city")).sendKeys("Boston");
		new Select(driver.findElement(By.id("billing_state"))).selectByValue("MA");
		driver.findElement(By.id("billing_zip")).sendKeys("02116");
		driver.findElement(By.id("paymentSubmitBtn")).click();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		//WebElement review = driver.findElement(By.id("reviewAgree"));
		//wait.until(ExpectedConditions.visibilityOf(review)).click();
		waitForElementDisplay("reviewAgree");
		driver.findElement(By.id("reviewAgree")).click();
		driver.findElement(By.id("placeOrderLink")).click();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		//WebElement orderPage = driver.findElement(By.xpath("//*[@id='bodyHtml']/section[3]/div/p[1]/strong/span"));
		waitForElementDisplay("//*[@id='main']/div[1]/div/section[1]/div[3]/p[1]/strong/span");
		if (isElementPresent(By
				.xpath("//*[@id='main']/div[1]/div/section[1]/div[3]/p[1]/strong/span"))) {
			String order = driver.findElement(By.xpath("//*[@id='main']/div[1]/div/section[1]/div[3]/p[1]/strong/span")).getText();
			report.updateTestLog("Place order","Order is placed successfully and the order number is"+ " -  " + order, Status.PASS);
		} else {
			report.updateTestLog("Place Order", "The order is failed",
					Status.FAIL);
		}

	}
	
	public void persPayPalOrder() throws InterruptedException {

		String ISBN = dataTable.getData("General_Data", "ISBN");
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(By.id("proceedToCheckout")).click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		String updateURL = driver.getCurrentUrl();
		if (updateURL.contains("Updatecart"))
		{
			driver.navigate().to("https://hmhco-v1.stg.techspa.com/hmhstorefront/checkout/multi/checkout-steps");
		}
		else {
		driver.findElement(By.id("deliverySubmitBtn")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		payPal();
			report.updateTestLog("Place Order", "Order placed using payPal",
					Status.PASS);
		}
	}
		
	public void guest() throws InterruptedException {
		String userName = dataTable.getData("General_Data", "Username");
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(By.id("proceedToCheckout")).click();
		waitForLoad(driver);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		String updateURL = driver.getCurrentUrl();
		if (updateURL.contains("Updatecart"))
		{
			driver.navigate().to("https://hmhco-v1.stg.techspa.com/hmhstorefront/checkout/multi/checkout-steps");
		}
		else {
			report.updateTestLog("Guest",   "User in Check out flow", Status.PASS);
		}
		waitForLoad(driver);
		waitForElementDisplay("emailAddress");
		driver.findElement(By.id("emailAddress")).sendKeys(userName);
		driver.findElement(By.id("guest-btn")).click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		String buttontext = driver.findElement(By.id("deliverySubmitBtn")).getAttribute("value");
		if(buttontext.contains("Continue to Payment & Billing"))
		{
			report.updateTestLog("Guest", buttontext + "Loaded ", Status.PASS);
		}
		else if (buttontext.equals("Continue"))
		{
			driver.navigate().refresh();
			report.updateTestLog("Guest", "Accordion not loaded fully", Status.WARNING);
//			driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/aside/div/ul/li/header/a")).click();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
//			driver.findElement(By.id("proceedToCheckout")).click();
//			
			}
		driver.findElement(By.id("shipping_fname")).sendKeys("Chandan");
		driver.findElement(By.id("shipping_lname")).sendKeys("Reddy");
		driver.findElement(By.id("shipping_confirm_email")).sendKeys(userName);
		driver.findElement(By.id("shipping_phone")).click();
		driver.findElement(By.id("shipping_phone")).sendKeys("1478956230");
		driver.findElement(By.id("shipping_address1")).sendKeys("Chan Dara");
		driver.findElement(By.id("shipping_address2")).sendKeys("310 N Larchmont Blvd");		
		driver.findElement(By.id("shipping_city")).sendKeys("Los Angeles");
		new Select(driver.findElement(By.id("shipping_state"))).selectByValue("CA");
		driver.findElement(By.id("shipping_zip")).sendKeys("90004-3045");
		Thread.sleep(4500);
		driver.findElement(By.id("is_billing_address")).click();
		driver.findElement(By.id("deliverySubmitBtn")).click();
					try {
							Thread.sleep(4000);
						} catch (InterruptedException e) {
							e.printStackTrace();

						}
					waitForLoad(driver);
				if (isElementPresent(By.id("access_fname")))
						{
						digital();
						}
				else if (isElementPresent(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[2]/div[2]/form/div[1]/div/div/div[2]/p[2]/input")))
							{
						report.updateTestLog("access code", "Access code accordion not present",Status.PASS);	
						}
				}
			
	public void payPal() throws InterruptedException {
		if (isElementPresent(By.id("access_fname")))
		{
			digital();
		}
		
		if (driver.findElement(By.id("paymentIndType1")).isSelected())
			{
				driver.findElement(By.id("paymentIndType2")).click();
			}
		else{
			report.updateTestLog("PayPal", "User in Payment tab", Status.PASS);
		}
//		if (driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[2]/div[2]/form/div[1]/div/div/div[2]/p[1]/input")).isSelected())
//		{
//			driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[2]/div[2]/form/div[1]/div/div/div[2]/p[2]/input")).click();
//		}
//		else if (driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[3]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[1]/input")).isSelected())
//		{
//			driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[3]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[2]/input")).click();
//		}
//		else if (driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[3]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[1]/input")).isSelected())
//		{
//			driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[3]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[2]/input")).click();
//			}
//		else if (driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[2]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[1]/input")).isSelected())
//		{
//			driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[2]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[2]/input")).click();
//				
//				}
		driver.findElement(By.id("paymentSubmitBtn")).click();
		try {
			Thread.sleep(12000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForElementDisplay("reviewAgree");
		driver.findElement(By.id("reviewAgree")).click();
		driver.findElement(By.id("placeOrderLink")).click();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		waitForElementDisplay("login_email");
		driver.findElement(By.id("login_email")).clear();
		driver.findElement(By.id("login_password")).clear();
		driver.findElement(By.id("login_email")).sendKeys("personal@hmhco.com");
		driver.findElement(By.id("login_password")).sendKeys("houghton1");
		driver.findElement(By.id("submitLogin")).click();
		try {
			Thread.sleep(12000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		driver.findElement(By.id("continue")).click();
		try {
			Thread.sleep(12000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		driver.findElement(By.id("continue")).click();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		waitForElementDisplay("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span");
		if (isElementPresent(By
				.xpath("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span"))) {
			String order = driver
					.findElement(
							By.xpath("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span"))
					.getText();
			report.updateTestLog("Place order",
					"Order is placed successfully and the order number is"
							+ " - " + order, Status.PASS);
		} else {
			report.updateTestLog("Place Order", "The order is failed",
					Status.FAIL);
		}
	}

	public void productSearchk12() {

		// String ISBN = null;
		String ISBN1 = dataTable.getData("General_Data", "ISBN");
		List<String> ISBNList = Arrays.asList(ISBN1.split(","));
		String[] ISBN_split = ISBN1.split(",");
		int IsbnListSize = ISBNList.size();

		for (int it = 0; it <= IsbnListSize - 1; it++) {
			driver.findElement(By.id("searchEntry")).sendKeys(ISBN_split[it]);
			waitForLoad(driver);
			driver.findElement(By.className("searchIconButton")).click();
			waitForLoad(driver);
			driver.findElement(
					By.xpath("//*[@id='content']/div/div[2]/div/div[3]/div[1]/div[3]/div/div[3]/ul/li/a"))
					.click();
			waitForLoad(driver);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// WebElement
			// Element=driver.findElement(By.xpath("//p[@class='srItemSummary']"));
			WebElement Element = driver
					.findElement(By
							.xpath("//*[@id='content']/div/div[2]/div/div[3]/div/div[2]/div[2]/p[4]"));
			Element.getText();

			if (Element != null) {
				String ElementText = Element.getText();
				if (ElementText.contains(ISBN_split[it])) {
					report.updateTestLog("K-12 Products",
							"Search results for the product ISBN :"
									+ ElementText, Status.PASS);
				} else {
					report.updateTestLog(
							"K-12 Products",
							"K-12 product is not found for the given search result",
							Status.FAIL);
				}
			}
		}
	}

	public void multipleItems() throws InterruptedException {

		// String ISBN = null;
		String ISBN200 = dataTable.getData("General_Data", "ISBN");
		List<String> ISBNList = Arrays.asList(ISBN200.split(","));
		String[] ISBN_split = ISBN200.split(",");
		int IsbnListSize = ISBNList.size();

		for (int it = 0; it <= IsbnListSize - 1; it++) {
			driver.findElement(By.id("searchEntry")).sendKeys(ISBN_split[it]);
			waitForLoad(driver);
			driver.findElement(By.xpath("//*[@id='header_0_sectionSearch']/div/input")).click();
			waitForLoad(driver);
			driver.findElement(By.xpath("//*[@id='main']/section/div/section/article/section[5]/div/div/div/figure/a")).click();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			waitForElementDisplay("html/body/form/div[3]/section/section[1]/section/div/section/div/article/aside/div/div");
			if (isElementPresent(By.xpath("//*[@id='body1_0_productcartDiv']/p[2]/a"))){
				driver.findElement(By.xpath("//*[@id='body1_0_productcartDiv']/p[2]/a")).click();
			}
			 else
			 {
				driver.findElement(By.xpath("//*[@id='body1_0_quantitySpan']/p[2]/a")).click();
			}
			Thread.sleep(3000);
			if (isElementPresent(By.id("relogin")))
					{
				report.updateTestLog("Restricted Product", "User cannot add the restricted product",Status.PASS);
					}
			else if(driver.findElement(By.xpath("//*[@id='cartButton']/a")).isEnabled())  
				{	
					
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			report.updateTestLog("Item added", "Cart Updated",Status.PASS);
			 	        
			try {
				Thread.sleep(3500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// for(int i=1; i<= 5; i++)
			// { */
			driver.findElement(By.xpath("//*[@id='cartButton']/a")).click();
			waitForElementDisplay("viewCartAnchorDown");
			String URL= driver.getCurrentUrl();
			if (isElementPresent(By.id("viewCartAnchorDown"))) 
			{
				driver.findElement(By.id("viewCartAnchorDown")).click();
			}
				else if (URL.contains("Stg"))
				{
					driver.navigate().to("http://hmhco-v1.stg.techspa.com/hmhstorefront/cart") ;
				}	
				else if (URL.contains("Pre"))
				{
					driver.navigate().to("http://hmhco-v1.Pre.techspa.com/hmhstorefront/cart") ;
				}	
				else if (URL.contains("Dev"))
				{
					driver.navigate().to("http://hmhco-v1.Dev.techspa.com/hmhstorefront/cart") ;
				}	
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			waitForElementDisplay("proceedToCheckout");
			}
		else if (isElementPresent(By.xpath("//*[@id='modal_content']/p/strong")))
			{
				report.updateTestLog("Product inventory", "Inventory for the product is low",Status.WARNING);
			}
		report.updateTestLog("Add to cart", "Mutiple Products added sucesfully",Status.PASS);
		}
	}
	
	public void signupPersonal() {

		String Email = dataTable.getData("General_Data", "Email");
		String Password = dataTable.getData("General_Data", "Password");

		driver.findElement(By.xpath("//*[@id='greyheaderbar_0_signUpLink']/span")).click();
		driver.findElement(By.id("txtfirstname")).sendKeys("Chandan");
		driver.findElement(By.id("txtlastname")).sendKeys("Kumar");
		driver.findElement(By.id("txtemail")).sendKeys(Email);
		driver.findElement(By.id("pwpassword")).sendKeys(Password);
		driver.findElement(By.id("pwpasswordcnfrm")).sendKeys(Password);
		driver.findElement(By.id("agree")).click();
		driver.findElement(By.id("lbtncreateaccount")).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			waitForElementDisplay("body1_0_thankyouRedirect");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		driver.findElement(By.id("body1_0_thankyouRedirect")).click();
		waitForLoad(driver);
		driver.findElement(By.xpath("//*[@id='signedOut']/li[1]/a/span")).click();
		report.updateTestLog("Logged in", "New Personal user created", Status.PASS);
		//waitForLoad(driver);
		//try {
		//	Thread.sleep(4000);
		//} catch (InterruptedException e) {
		//	e.printStackTrace();
		//}
		//waitForLoad(driver);
		//String user = driver.findElement(By.id("signedInName")).getText();
		//report.updateTestLog("Logged in", "New Personal user created", Status.PASS);

	}

	public void signupInst() {

		String Email = dataTable.getData("General_Data", "Email");
		String Password = dataTable.getData("General_Data", "Password");
		driver.findElement(By.xpath("//*[@id='greyheaderbar_0_signUpLink']/span")).click();
		driver.findElement(By.id("institutional_account")).click();
		driver.findElement(By.id("lbtncreateaccount")).click();
		driver.findElement(By.id("login_email")).sendKeys(Email);
		driver.findElement(By.id("confirm_login_email")).sendKeys(Email);
		driver.findElement(By.id("login_password")).sendKeys(Password);
		driver.findElement(By.id("confirm_login_password")).sendKeys(Password);
		driver.findElement(By.id("agree")).click();
		driver.findElement(By.id("first_name")).sendKeys("Chandan");
		driver.findElement(By.id("last_name")).sendKeys("Reddy");
		new Select(driver.findElement(By.id("birth_month")))
				.selectByValue("12");
		new Select(driver.findElement(By.id("birth_year")))
				.selectByValue("1991");
		driver.findElement(By.id("daytime_phone")).click();
		driver.findElement(By.id("daytime_phone")).sendKeys("9441249125");
		driver.findElement(By.id("shipping_institution")).sendKeys("CTS");
		driver.findElement(By.id("shipping_address1")).sendKeys("Hyd");
		driver.findElement(By.id("shipping_city")).sendKeys("Boston");
		new Select(driver.findElement(By.id("shipping_state")))
				.selectByValue("MA");
		driver.findElement(By.id("shipping_zip")).sendKeys("02266");
		driver.findElement(By.id("is_billing_address")).click();
		driver.findElement(By.id("create_account")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		try {
			waitForElementDisplay("body1_0_thankyouRedirect");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.id("body1_0_thankyouRedirect")).click();
		report.updateTestLog("Logged in", "New Personal user created", Status.PASS);
		String user =driver.findElement(By.xpath("//*[@id='signedOut']/li[1]/a/span")).getText();
		waitForLoad(driver);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		if (user.contains("Chandan")){
		report.updateTestLog("Logged in", "new personal user" + "-" + user + "created successfully", Status.PASS);
		}
		else
		{
		report.updateTestLog("Logged in", "Unable to read user name", Status.FAIL);
				
		}
	}

	public void digital() {
		String ISBN = dataTable.getData("General_Data", "ISBN");
		String userName = dataTable.getData("General_Data", "Username");
		String Password = dataTable.getData("General_Data", "Password");
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		report.updateTestLog("Access code","Inside Access code accordion", Status.PASS);
		waitForLoad(driver);
		driver.findElement(By.id("access_fname")).clear();
		driver.findElement(By.id("access_lname")).clear();
		driver.findElement(By.id("access_email")).clear();
		driver.findElement(By.id("access_fname")).sendKeys("Chandan");
		driver.findElement(By.id("access_lname")).sendKeys("reddy");
		driver.findElement(By.id("access_email")).sendKeys(userName);
		driver.findElement(By.id("access_confirm_email")).sendKeys(userName);
		driver.findElement(By.id("accessCodeSubmitBtn")).click();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		report.updateTestLog("Access code","Access code accordion successfully loaded", Status.PASS);
			}

	public void guestPayment() throws InterruptedException {
		String ISBN = dataTable.getData("General_Data", "ISBN");
		WebDriverWait wait = new WebDriverWait(driver,50);
		guest();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		WebElement payment = driver.findElement(By.id("payment.nameOnCard"));
		wait.until(ExpectedConditions.visibilityOf(payment)).sendKeys("Chandan");
		driver.findElement(By.id("cardNumberInput")).sendKeys("4055011111111111");
		driver.findElement(By.id("payment.cvv")).sendKeys("123");
		new Select(driver.findElement(By.id("payment.expirymonth"))).selectByValue("9");
		new Select(driver.findElement(By.id("payment.expiryYear"))).selectByValue("2019");
		driver.findElement(By.id("paymentSubmitBtn")).click();
		waitForLoad(driver);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForElementDisplay("reviewAgree");
		driver.findElement(By.id("reviewAgree")).click();
		driver.findElement(By.id("placeOrderLink")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForElementDisplay("//*[@id='main']/div[1]/div/section[1]");
		if (isElementPresent(By
				.xpath("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span"))) {
			String order = driver
					.findElement(
							By.xpath("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span"))
					.getText();
			report.updateTestLog("Place order",
					"Order is placed successfully and the order number is"
							+ " - " + order, Status.PASS);
		} else {
			report.updateTestLog("Place Order", "The order is failed",
					Status.FAIL);
		}
	}
	
	public void guestSignup() throws InterruptedException {
		String userName = dataTable.getData("General_Data", "Email");
		String Password = dataTable.getData("General_Data", "Password");
		
		guestPayment();
		waitForElementDisplay("create_account_button");
		driver.findElement(By.id("create_account_button")).click();
		driver.findElement(By.id("pwpassword")).sendKeys(Password);
		driver.findElement(By.id("pwpasswordcnfrm")).sendKeys(Password);
		driver.findElement(By.id("agree")).click();
		driver.findElement(By.id("lbtncreateaccount")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		try {
			waitForElementDisplay("body1_0_thankyouRedirect");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(By.id("body1_0_thankyouRedirect")).click();
		report.updateTestLog("Logged in", "username created ", Status.PASS);
		}

	public void persCheckOut() throws InterruptedException {
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(By.id("proceedToCheckout")).click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		driver.findElement(By.id("deliverySubmitBtn")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		if (isElementPresent(By.id("access_fname")))
		{
			report.updateTestLog("Digital order", "Digital Products are present",Status.DONE);
			digital();
			persPaymentAccordion();
			report.updateTestLog("Digital order", "User placed order with digital product",
					Status.PASS);
		}
		else if (driver.findElement(By.id("paymentIndType1")).isSelected())
			{
			report.updateTestLog("access code", "No Digital Products are present",Status.DONE);
				persPaymentAccordion();
				report.updateTestLog("access code", "Access code accordion not present",Status.PASS);	
					}
		else if (driver.findElement(By.id("paymentIndType2")).isSelected())
				{
			report.updateTestLog("PayPal", "user choose PayPal option",Status.DONE);		
				payPal();
					report.updateTestLog("payPal", "User placed order with digital product",Status.PASS);	
						}
	}
//		else if (driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[2]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[1]/input")).isSelected())
//		{
//			persPaymentAccordion();
//			report.updateTestLog("access code", "Access code accordion not present",Status.PASS);	
//				}
//		else if (driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[3]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[1]/input")).isSelected())
//			{ 
//			persPaymentAccordion();
//		report.updateTestLog("access code", "Access code accordion not present",Status.PASS);	
//				}
//		else if (driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[2]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[2]/input")).isSelected())
//		{
//			payPal();
//			report.updateTestLog("payPal", "User placed order with digital product",Status.PASS);	
//				}
//		else if (driver.findElement(By.xpath("html/body/div[1]/section/section[1]/section[1]/article/ul/li[3]/div[2]/div[2]/form/div[2]/div/div/div[2]/p[2]/input")).isSelected())
//		{
//			payPal();
//			report.updateTestLog("payPal", "User placed order with digital product",Status.PASS);	
//				}
			
	
	public void persPaymentAccordion() throws InterruptedException {
			String NameonCard = dataTable.getData("General_Data", "NameonCard");
			String CVV = dataTable.getData("General_Data", "CVV");
			String Password = dataTable.getData("General_Data", "Password");
			WebDriverWait wait = new WebDriverWait(driver,50);
			report.updateTestLog("Payment", "Inside Payment Accordion", Status.DONE);
			if (driver.findElement(By.id("paymentIndType2")).isSelected())
			{
				driver.findElement(By.id("paymentIndType1")).click();
			}
			else {
				report.updateTestLog("Payment", "User already chose CC as payment option", Status.PASS);
			}
		WebElement shipping = driver.findElement(By.id("searchPaymentCard_chosen"));
		wait.until(ExpectedConditions.visibilityOf(shipping)).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		driver.findElement(By.xpath("//*[@id='searchPaymentCard_chosen']/div/div/input")).sendKeys(NameonCard);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		driver.findElement(By.xpath("//*[@id='searchPaymentCard_chosen']/div/ul/li")).click();
		driver.findElement(By.id("payment_cvv")).sendKeys(CVV);
		driver.findElement(By.id("paymentSubmitBtn")).click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		//WebElement review = driver.findElement(By.id("reviewAgree"));
		waitForElementDisplay("reviewAgree");
		driver.findElement(By.id("reviewAgree")).click();
		//wait.until(ExpectedConditions.visibilityOf(review)).click();
		driver.findElement(By.id("placeOrderLink")).click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForElementDisplay("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span");
		driver.findElement(By.xpath("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span")).click();
		if (isElementPresent(By.xpath("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span"))) {
			String order = driver.findElement(By.xpath("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span")).getText();
			report.updateTestLog("Place order","Order is placed successfully and the order number is"
							+ " - " + order, Status.PASS);
		} else {
			report.updateTestLog("Place Order", "The order is failed",
					Status.FAIL);
		}
	}

	public void personalNewAddress() throws InterruptedException {

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(By.id("proceedToCheckout")).click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		// original code New address
		// new Select
		// (driver.findElement(By.id("shippingAddressDropDownList"))).selectByVisibleText("SHAKERAG ELEMENTARY SCHOOL 10885 ROGERS CIR DULUTH GA 30097-1927");
		driver.findElement(By.id("shipping_fname")).sendKeys("Chandan");
		driver.findElement(By.id("shipping_lname")).sendKeys("reddy");
		driver.findElement(By.id("shipping_phone")).click();
		driver.findElement(By.id("shipping_phone")).sendKeys("7894561230");
		driver.findElement(By.id("shipping_addressName")).sendKeys("trialAddress1");
		driver.findElement(By.id("shipping_address1")).sendKeys("222");
		driver.findElement(By.id("shipping_address2")).sendKeys("Berkley St");
		driver.findElement(By.id("shipping_city")).sendKeys("Boston");
		new Select(driver.findElement(By.id("shipping_state"))).selectByValue("MA");
		driver.findElement(By.id("shipping_zip")).sendKeys("02116");
		// driver.findElement(By.id("shipping_defaultAddress")).click();
		driver.findElement(By.id("deliverySubmitBtn")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		// original code New address
		driver.findElement(By.id("payment.nameOnCard")).clear();
		driver.findElement(By.id("cardNumberInput")).clear();
		driver.findElement(By.id("payment.nameOnCard")).sendKeys("Chandan");
		driver.findElement(By.id("cardNumberInput")).sendKeys(
				"4055011111111111");
		driver.findElement(By.id("payment.cvv")).sendKeys("123");
		new Select(driver.findElement(By.id("payment.expirymonth")))
				.selectByValue("9");
		new Select(driver.findElement(By.id("payment.expiryYear")))
				.selectByValue("2019");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		driver.findElement(By.id("add_payment_btn")).click();
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		// driver.findElement(By.id("payment_cvv")).sendKeys("123");
		driver.findElement(By.id("paymentSubmitBtn")).click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		waitForElementDisplay("reviewAgree");
		driver.findElement(By.id("reviewAgree")).click();
		driver.findElement(By.id("placeOrderLink")).click();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		waitForLoad(driver);
		waitForElementDisplay("//*[@id='main']/div[1]/div/section[1]/div[3]/p[1]/strong/span");
		if (isElementPresent(By.xpath("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span"))) {
			String order = driver
					.findElement(By.xpath("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span")).getText();
			report.updateTestLog("Place order",
					"Order is placed successfully and the order number is"
							+ " - " + order, Status.PASS);
		} else {
			report.updateTestLog("Place Order", "The order is failed",
					Status.FAIL);
		}

	}

	public void orderHistory() throws InterruptedException {
		personalNewAddress();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		
		String orderid = driver.findElement(By.xpath("//*[@id='main']/div[1]/div/section[1]/div[2]/p[1]/strong/span")).getText();
		String ordertime = driver.findElement(By.xpath("//*[@id='text-order-date']/strong")).getText();
		myAccount();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(By.xpath("//*[@id='sidebarinner']/nav/ul/li[4]/h6/a")).click();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String historypage = driver
				.findElement(
						By.xpath("//*[@id='form_order_history']/ul/li[2]/div/div[3]/div/div[1]/div[2]"))
				.getText();
		if (historypage.contains(orderid) & historypage.contains(ordertime)) {
			report.updateTestLog("order history",
					"Order status successfully updated and the order number is"
							+ " - " + orderid, Status.PASS);
		} else {
			report.updateTestLog("order history", "Order status not updated",
					Status.FAIL);
		}

	}

	public void editAddressBook() throws InterruptedException {
		myAccount();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(
				By.xpath("//*[@id='sidebarinner']/nav/ul/li[2]/h6/a"))
				.click();
		try {
			Thread.sleep(12000);;
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div/div/div[1]/a[2]"))
				.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("address2")).clear();
		driver.findElement(By.id("address2")).sendKeys("New Light House");
		driver.findElement(By.id("add_address_btn")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		waitForElementDisplay("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div/div");
		String newAdd = driver
				.findElement(
						By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div/div"))
				.getText();
		if (newAdd.contains("New Light House")) {
			report.updateTestLog("Address Book",
					"Address successfully updated", Status.PASS);
		} else {
			report.updateTestLog("Address Book", "Address not updated",
					Status.FAIL);
		}
	}
	
	public void newAddressBook() throws InterruptedException {
		String FName = dataTable.getData("General_Data", "FirstName");
		String LName = dataTable.getData("General_Data", "LastName");
		String nickName = dataTable.getData("General_Data", "NickName");
		String AddressL1 = dataTable.getData("General_Data", "Address1");
		String city = dataTable.getData("General_Data", "City");
		myAccount();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(
				By.xpath("//*[@id='sidebarinner']/nav/ul/li[2]/h6/a")).click();
		try {
			Thread.sleep(12000);;
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		waitForElementDisplay("//*[@id='form_address_book']/ul/li[1]/div[2]/div[1]/div[2]/p/a");
		driver.findElement(By.xpath("//*[@id='form_address_book']/ul/li[1]/div[2]/div[1]/div[2]/p/a"))
				.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("tdName")).sendKeys(FName);
		driver.findElement(By.id("tdLName")).sendKeys(LName);
		driver.findElement(By.id("addressNickName")).sendKeys(nickName);
		driver.findElement(By.id("tdPhone")).click();
		driver.findElement(By.id("tdPhone")).sendKeys("7485916230");
		driver.findElement(By.id("address1")).sendKeys(AddressL1);
		driver.findElement(By.id("city")).sendKeys(city);
		new Select(driver.findElement(By.id("state"))).selectByValue("MA");
		driver.findElement(By.id("zipCode")).sendKeys("02266");
		driver.findElement(By.id("add_address_btn")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		waitForElementDisplay("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div/div/div[2]/p[1]");
		String newAdd = driver
				.findElement(
						By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div/div/div[2]/p[1]"))
				.getText();
		if (newAdd.contains(nickName)) {
			report.updateTestLog("Address Book",
					"New Address added successfully", Status.PASS);
		} else {
			report.updateTestLog("Address Book", "unable to add new address",
					Status.FAIL);
		}
	}
	
	public void myAccount() throws InterruptedException {
		try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			waitForLoad(driver);
			//driver.findElement(By.xpath("//*[@id='signedOut']/li[1]/a/span")).click();
			Actions action = new Actions(driver);
			WebElement item = driver.findElement(By.xpath("//*[@id='signedOut']/li[1]/a/span"));
			WebDriverWait wait = new WebDriverWait(driver,10); 
			WebElement inside = driver.findElement(By.xpath("//*[@id='signedOut']/li[1]/div/div/ul/li[2]/a"));
			//action.moveToElement(item);
			//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			action.moveToElement(item).build().perform();
			
			//driver.findElement(By.xpath("//*[@id='signedOut']/li[1]/div/div/ul/li[2]/a")).click();
			wait.until(ExpectedConditions.visibilityOf(inside)).click(); //this will wait for elememt to be visible for 20 seconds
			//inside.click();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			waitForLoad(driver);
			String accountUrl = driver.getCurrentUrl();
			if (accountUrl.contains("account")){
				report.updateTestLog("My Account","User inside My account", Status.PASS);
				}
			else
				{
				driver.navigate().to("https://hmhco-v1.stg.techspa.com/account/edit-account");
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				waitForLoad(driver);
				report.updateTestLog("My Account","User redirected to My account", Status.PASS);
				}
		}
	
	public void deleteAddressBook() throws InterruptedException {
			myAccount();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			waitForLoad(driver);
			driver.findElement(
					By.xpath("//*[@id='sidebarinner']/nav/ul/li[2]/h6/a"))
					.click();
			try {
				Thread.sleep(12000);;
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			waitForLoad(driver);
			driver.findElement(
				By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div/div/div[1]/a[1]"))
				.click();
		driver.findElement(By.id("confirm_delete_btn")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		String deleteAdd = driver
				.findElement(
						By.xpath("//*[@id='form_address_book']/ul/li[1]/div[2]/div[1]/div[1]/p"))
				.getText();
		if (deleteAdd
				.contains("You do not have any addresses stored in your address book. We will save the addresses you use during your next purchase")) {
			report.updateTestLog("Address Book",
					"Address successfully deleted", Status.PASS);
		} else {
			report.updateTestLog("Address Book", "Address not deleted",
					Status.FAIL);
		}
	}

	public void newPayment() throws InterruptedException {
		String nickName = dataTable.getData("General_Data", "NickName");
		String Cardnumber = dataTable.getData("General_Data", "CardNumber");
		String CVV = dataTable.getData("General_Data", "CVV");
		WebDriverWait wait = new WebDriverWait(driver,25);
		myAccount();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(
				By.xpath("//*[@id='sidebarinner']/nav/ul/li[3]/h6/a"))
				.click();
		try {
			Thread.sleep(12000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(
				By.xpath("//*[@id='form_payment_option']/ul/li[1]/div[2]/div[1]/div[2]/p/a")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		WebElement Payment =driver.findElement(By.id("nameOnCard"));
		wait.until(ExpectedConditions.visibilityOf(Payment)).click(); 		
		driver.findElement(By.id("nameOnCard")).sendKeys(nickName);
		driver.findElement(By.id("cardNumber")).sendKeys(Cardnumber);
		driver.findElement(By.id("cvv")).sendKeys(CVV);
		new Select(driver.findElement(By.id("expirymonth"))).selectByValue("09");
		new Select(driver.findElement(By.id("expiryYear"))).selectByValue("2019");
		driver.findElement(By.id("defaultAddress")).click();
		driver.findElement(By.id("billingAddress_chosen")).sendKeys(nickName);
		report.updateTestLog("Payment Option","Payment Option page billing address selction done successfully", Status.PASS);
		}
	
	public void addingPayment() throws InterruptedException {
		newPayment();
		addNewPayment();
		report.updateTestLog("Payment Option", "New Payment option added to user", Status.PASS);
	}
	
	public void addNewPayment() throws InterruptedException {
			String nickName = dataTable.getData("General_Data", "NickName");
			driver.findElement(By.id("add_address_btn")).click();
			try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		String paymentAdd = driver.findElement(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div")).getText();
		if (paymentAdd.contains(nickName)) 
		{
			report.updateTestLog("Payment Option",
					"Payment successfully Added", Status.PASS);
		} 
		else
		{
			report.updateTestLog("Payment Book","Unable to add new Payment option", Status.FAIL);
		}
		
		
	}

	public void billingNewAddress() throws InterruptedException {
		String FName = dataTable.getData("General_Data", "FirstName");
		String LName = dataTable.getData("General_Data", "LastName");
		String nickName = dataTable.getData("General_Data", "NickName");
		String AddressL1 = dataTable.getData("General_Data", "Address1");
		String city = dataTable.getData("General_Data", "City");
		
		newPayment();
		driver.findElement(By.xpath("//*[@id='add-address-fields']/div[6]/div[2]/a[2]")).click();
		driver.findElement(By.id("tdpaymentName")).sendKeys(FName);
		driver.findElement(By.id("tdpaymentLName")).sendKeys(LName);
		driver.findElement(By.id("paymentaddressNickName")).sendKeys(nickName);
		driver.findElement(By.id("tdpaymentPhone")).click();
		driver.findElement(By.id("tdpaymentPhone")).sendKeys("7485916230");
		driver.findElement(By.id("paymentAddress")).sendKeys(AddressL1);
		driver.findElement(By.id("paymentcity")).sendKeys(city);
		new Select(driver.findElement(By.id("paymentstate"))).selectByValue("MA");
		driver.findElement(By.id("paymentzipCode")).sendKeys("02266");
		driver.findElement(By.id("SubmitPaymentAddress")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		String newAdd = driver.findElement(By.id("billingAddress_chosen")).getText();
		if (newAdd.contains(nickName)) 
		{
			report.updateTestLog("Payment Option","New billing address added successfully", Status.PASS);
		} 
		else 
		{
			report.updateTestLog("Payment Option", "Unable to add new billing address",Status.FAIL);
			}
		addNewPayment();
		waitForLoad(driver);
		report.updateTestLog("Payment Option","New billing address and new Payment added successfully", Status.PASS);
	}
	
	public void deletePayment() throws InterruptedException
	{
		myAccount();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		driver.findElement(
				By.xpath("//*[@id='sidebarinner']/nav/ul/li[3]/h6/a"))
				.click();
		try {
			Thread.sleep(12000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		waitForElementDisplay("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div/div[1]/a[1]");
		driver.findElement(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div/div[1]/a[1]")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("confirm_delete_btn")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForLoad(driver);
		 String deletePayment = driver.findElement(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[1]/div[2]/div[1]/div[1]/p/strong")).getText();
		if (deletePayment.contains("You do not have any payment methods on file. We will save the payment method you use during your next purchase")) {
			report.updateTestLog("Payment Option",
					"Payment successfully deleted", Status.PASS);
		} else {
			report.updateTestLog("Payment Option","Payment option not deleted", Status.FAIL);	
	}
	
	}
	
	public void emailChange() throws InterruptedException {
		String NewEmailID= dataTable.getData("General_Data", "Email");
		myAccount();
		waitForElementDisplay("edit-btn-email");
		driver.findElement(By.id("edit-btn-email")).click();
		driver.findElement(By.id("login_email")).clear();		
		driver.findElement(By.id("login_email")).sendKeys(NewEmailID);
		driver.findElement(By.id("login_email_confirm")).sendKeys(NewEmailID);
		driver.findElement(By.id("email-btn")).click();
		Thread.sleep(1500);
		waitForElementDisplay("edit-btn-email");
		driver.navigate().refresh();
		waitForElementDisplay("edit-btn-email");
		String userName = driver.findElement(By.id("login_email")).getAttribute("value");
		report.updateTestLog("Email Change", userName, Status.DONE);
		if (userName.contains(NewEmailID))
				{
			report.updateTestLog("Email Change","User successfully changed the Email ID", Status.PASS);
				}
		else {
			report.updateTestLog("Email Change","User unable to change the Email ID", Status.FAIL);
			}
		
		}
	
	public void passwordChange() throws InterruptedException {
		String OldPassword= dataTable.getData("General_Data", "Password");
		String NewPassword= dataTable.getData("General_Data", "Email");
		myAccount();
		waitForElementDisplay("edit-btn-password");
		driver.findElement(By.id("edit-btn-password")).click();
		driver.findElement(By.id("login_password")).clear();
		driver.findElement(By.id("login_password")).sendKeys(OldPassword);
		driver.findElement(By.id("login_password_new")).sendKeys(NewPassword);
		driver.findElement(By.id("login_password_confirm")).sendKeys(NewPassword);
		driver.findElement(By.id("password-btn")).click();
		Thread.sleep(1500);
		if (isElementPresent(By.id("edit-btn-password")))
			{
			report.updateTestLog("Password Change","User successfully changed the Password", Status.PASS);
			}
		else {
			report.updateTestLog("Password Change","User unable to change the Password", Status.FAIL);
			}
		}

	
}
