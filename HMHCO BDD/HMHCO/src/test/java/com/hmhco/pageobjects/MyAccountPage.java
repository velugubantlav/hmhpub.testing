package com.hmhco.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class MyAccountPage {
	private static WebElement element=null;
	
		
	public static WebElement addressbook_link(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='sidebarinner']/nav/ul/li[2]/h6/a")));
   	 element=driver.findElement(By.xpath(".//*[@id='sidebarinner']/nav/ul/li[2]/h6/a"));
   	 return element;
    }

	public static WebElement orderhistory_link(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='sidebarinner']/nav/ul/li[4]/h6/a")));
   	 element=driver.findElement(By.xpath(".//*[@id='sidebarinner']/nav/ul/li[4]/h6/a"));
   	 return element;
    }
	
	public static WebElement searchorder(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("searchorder")));
   	 element=driver.findElement(By.id("searchorder"));
   	 return element;
    }
	
	public static WebElement searchorderbytime(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("searchorder")));
   	 element=driver.findElement(By.id("searchorder"));
   	 return element;
    }
	
	
	public static WebElement addaddress_link(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='form_address_book']/ul/li[1]/div[2]/div[1]/div[2]/p/a")));
   	 element=driver.findElement(By.xpath(".//*[@id='form_address_book']/ul/li[1]/div[2]/div[1]/div[2]/p/a"));
   	 return element;
    }
	public static WebElement editaddress_link(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div/div[1]/a[2]")));
   	 element=driver.findElement(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div/div[1]/a[2]"));
   	 return element;
    }
	public static WebElement deleteaddress_link(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div/div[1]/a[1]")));
   	 element=driver.findElement(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div/div[1]/a[1]"));
   	 return element;
    }
	
	public static WebElement setasdefault_link(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("A1")));
   	 element=driver.findElement(By.id("A1"));
   	 return element;
    }
	
	public static WebElement removedefault_link(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div/div[2]/p[3]/a")));
   	 element=driver.findElement(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div/div[2]/p[3]/a"));
   	 return element;
    }
	
	public static WebElement addresstitle(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div/div[2]/p[1]")));
   	 element=driver.findElement(By.xpath("html/body/div[1]/section/section/section[1]/div/article/form/ul/li[2]/div/div[1]/div/div/div[1]/div/div[2]/p[1]"));
   	 return element;
    }
		
		
	public static WebElement saveaddress_button(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("add_address_btn")));
   	 element=driver.findElement(By.id("add_address_btn"));
   	 return element;
    }
	
	public static WebElement canceladdress_button(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("add_address_btn_cancel")));
   	 element=driver.findElement(By.id("add_address_btn_cancel"));
   	 return element;
    }
	public static WebElement firstname(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("tdName")));
   	 element=driver.findElement(By.id("tdName"));
   	 return element;
    }
	
	public static WebElement lastname(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("tdName")));
   	 element=driver.findElement(By.id("tdLName"));
   	 return element;
    }
	
	public static WebElement addressnickname(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("addressNickName")));
   	 element=driver.findElement(By.id("addressNickName"));
   	 return element;
    }
	
	public static WebElement statustext(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='form_address_book']/ul/li[1]/div[2]/div[1]/div[1]/h5")));
   	 element=driver.findElement(By.xpath(".//*[@id='form_address_book']/ul/li[1]/div[2]/div[1]/div[1]/h5"));
   	 return element;
    }
	
			
	}


