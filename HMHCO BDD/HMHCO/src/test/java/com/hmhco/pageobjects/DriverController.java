package com.hmhco.pageobjects;

import com.hmhco.uitests.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverController {

	public WebDriver driver = null;
	
	
	
	private static DriverController dv = new DriverController();
	
	
	protected DriverController(){
		super();
	}
	
	public WebDriver getAndResetDriver(){
		if(driver != null){
			System.out.println(driver.getTitle());
			driver.quit();
		}
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		return driver;
	}
	
	
	public WebDriver getDriver(){
		return driver;
	}	
	
	
	public static DriverController getInstance(){
			return dv;
	}
	
	
	
		
	
}
