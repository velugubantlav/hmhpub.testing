package com.hmhco.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Educators_homepage {

	private static WebElement element=null;
	
	public static WebElement searchbox(WebDriver driver)
	{
	   element = driver.findElement(By.id("searchEntry"));
	   return element;	
	}
	
	public static WebElement search_button(WebDriver driver)
	{
	   element = driver.findElement(By.xpath(".//*[@id='header_0_sectionSearch']/div/input"));
	   return element;	
	}
	
	
	public static WebElement firstproduct(WebDriver driver)
	{
	   element = driver.findElement(By.xpath(".//*[@id='main']/section/div/section/article/section[5]/div/div[1]/div[1]/h3/a"));
	   return element;	
	}
	
	public static WebElement addtocart_button(WebDriver driver)
	{
		WebDriverWait wait=new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(".//*[@id='body1_0_productcartDiv']/div/p[2]/a"))));
		element = driver.findElement(By.xpath(".//*[@id='body1_0_productcartDiv']/div/p[2]/a"));
	   return element;	
	}
	
	public static WebElement cart_button(WebDriver driver)
	{
		WebDriverWait wait=new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(".//*[@id='cartButton']/a"))));
		element = driver.findElement(By.xpath(".//*[@id='cartButton']/a"));
	    return element;	
	}
	
	public static WebElement checkout_button(WebDriver driver)
	{
		WebDriverWait wait=new WebDriverWait(driver,20);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("checkoutAnchorDown")));
		element = driver.findElement(By.id("checkoutAnchorDown"));
	   return element;	
	}
	
	public static WebElement freeshpngmsg(WebDriver driver)
	{
		WebDriverWait wait=new WebDriverWait(driver,20);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='cart']/section[2]/div[1]/p")));
		element = driver.findElement(By.xpath(".//*[@id='cart']/section[2]/div[1]/p"));
	   return element;	
	}
	
	public static WebElement finalprice(WebDriver driver)
	{
		WebDriverWait wait=new WebDriverWait(driver,20);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("sub-total-mini-cart")));
		element = driver.findElement(By.id("sub-total-mini-cart"));
	   return element;	
	}		
	
	public static WebElement signin_name(WebDriver driver)
    {
   	 WebDriverWait wait=new WebDriverWait(driver, 10);
   	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='signedOut']/li[1]/a/span")));
   	 element=driver.findElement(By.xpath(".//*[@id='signedOut']/li[1]/a/span"));
   	 return element;
    }
	
		
	public static void addtocart(WebDriver driver,String keyword) throws InterruptedException
	{
		searchbox(driver).sendKeys(keyword);
		search_button(driver).click();
		Actions mousehover=new Actions(driver);
		mousehover.moveToElement(Educators_homepage.firstproduct(driver)).build().perform();
		WebDriverWait wait=new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='body1_0_lstvResults_ctrl0_P3_0']/a"))).click();
				
	}
			
	public static void proceedtocheckout(WebDriver driver) throws InterruptedException
	   {
	   cart_button(driver).click();
	   Thread.sleep(1000);
	   if(checkout_button(driver).isEnabled())
		{
		   checkout_button(driver).click();
		}
		else
		{
			System.out.println("not displayed");
		}
	    }
		
	public static void navigatetoMyAccount(WebDriver driver) throws InterruptedException
		{
		Actions mousehover=new Actions(driver);
		mousehover.moveToElement(Educators_homepage.signin_name(driver)).build().perform();
		WebDriverWait wait=new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='signedOut']/li[1]/div/div/ul/li[2]/a"))).click();
		}
		
}
