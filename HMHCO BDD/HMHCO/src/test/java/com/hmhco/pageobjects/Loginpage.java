package com.hmhco.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Loginpage

{
			
	//login page objects
	
	private static WebElement element=null;

	public static WebElement login_signinlink(WebDriver driver)
	{	
	element = driver.findElement(By.id("greyheaderbar_0_signLink"));
	return element;
	}
	
	public static WebElement login_signinemail(WebDriver driver)
	{	
	element = driver.findElement(By.id("lblEmail"));
	return element;
	}
	public static WebElement login_signinpwd(WebDriver driver)
	{	
	element = driver.findElement(By.id("pwdSiPassword"));
	return element;
	}
	public static WebElement login_signinbutton(WebDriver driver)
	{	
	element = driver.findElement(By.id("lbtSiSignin"));
	return element;
	}
	public static WebElement main_searchbox(WebDriver driver)
	{	
	element = driver.findElement(By.id("searchEntry"));
	return element;
	}
	public static void signin(WebDriver driver,String uname,String pwd) throws Exception
	{
		login_signinlink(driver).click();
		Thread.sleep(500);
		login_signinemail(driver).sendKeys(uname);
		login_signinpwd(driver).sendKeys(pwd);
		login_signinbutton(driver).click();
	}
		
		
}

