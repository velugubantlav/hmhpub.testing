package com.hmhco.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Widgetpage {
     
	private static WebElement element=null;
	
	public static WebElement widget(WebDriver driver)
	{
	   element = driver.findElement(By.xpath(".//*[@id='main']/section/div/section/article/section/div/div"));
	   return element;
	}
 
	public static WebElement widget_title(WebDriver driver)
	{
	   element = driver.findElement(By.id("body1_0_rightsectiondata_0_lblTitle"));
	   return element;
	}
	
	public static WebElement category(WebDriver driver)
	{
	   WebDriverWait wait=new WebDriverWait(driver,20);
	   wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='body1_0_rightsectiondata_0_ChildItemDiv']/div[1]/ul/li[3]/h5/a")));
	   element = driver.findElement(By.xpath(".//*[@id='body1_0_rightsectiondata_0_ChildItemDiv']/div[1]/ul/li[3]/h5/a"));
	   return element;
	}
   
	

}

