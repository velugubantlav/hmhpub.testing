package com.hmhco.pageobjects;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Checkoutpage {

private static WebElement element=null;


     public static WebElement addaddress_link(WebDriver driver)
     {
    	 WebDriverWait wait=new WebDriverWait(driver, 10);
    	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("shippingAddAddressLink")));
    	 element=driver.findElement(By.id("shippingAddAddressLink"));
    	 return element;
     }
     
     public static WebElement defaultaddress(WebDriver driver)
     {
    	 WebDriverWait wait=new WebDriverWait(driver, 10);
    	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='shippingAddressDropDownList_chosen']/a")));
    	 element=driver.findElement(By.xpath(".//*[@id='shippingAddressDropDownList_chosen']/a"));
    	return element;
     }
	
     public static WebElement editaddress_link(WebDriver driver)
     {
    	element=driver.findElement(By.id("shippingEditAddressLink"));
    	return element;
     }
     
     public static WebElement deleteaddress_link(WebDriver driver)
     {
    	element=driver.findElement(By.id("shippingDeleteAddressLink"));
    	return element;
     }
     
     public static WebElement deletecnfrmtn_link(WebDriver driver)
     {
    	WebDriverWait wait=new WebDriverWait(driver, 10);
     	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("delivery_confirm_delete_btn"))); 
    	element=driver.findElement(By.id("delivery_confirm_delete_btn"));
    	return element;
     }
     
     
     public static WebElement setdefault_link(WebDriver driver)
     {
    	element=driver.findElement(By.id("setDefaultAddress"));
    	return element;
     }
     
     
     public static WebElement removedefault_link(WebDriver driver)
     {
    	element=driver.findElement(By.id("removeDefaultAddress"));
    	return element;
     }
     
     public static WebElement saveaddress_button(WebDriver driver)
     {
    	WebDriverWait wait=new WebDriverWait(driver, 10);
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("shipping_address_btn"))); 
    	element=driver.findElement(By.id("shipping_address_btn"));
    	return element;
     }
     
     public static WebElement shippingcancel_button(WebDriver driver)
     {
    	element=driver.findElement(By.id("shipping_address_btn_cancel"));
    	return element;
     }
            
          
	public static WebElement firstname(WebDriver driver)
	{
		WebDriverWait wait=new WebDriverWait(driver, 10);
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("shipping_fname")));
	   element = driver.findElement(By.id("shipping_fname"));
	   return element;	
	}
	
	public static WebElement lastname(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_lname"));
	   return element;	
	}
	
	public static WebElement phone(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_phone"));
	   return element;	
	}
	
	public static WebElement address(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_addressName"));
	   return element;	
	}
	
	public static WebElement address_err(WebDriver driver)
	{
	   element = driver.findElement(By.xpath(".//*[@id='p_addressName']/span"));
	   return element;	
	}
	
	
	public static WebElement address1(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_address1"));
	   return element;	
	}
	
	public static WebElement address2(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_address2"));
	   return element;	
	}
	
	public static WebElement city(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_city"));
	   return element;	
	}
	
	public static void selectstate(WebDriver driver,String statename)
	{
	   Select state = new Select(driver.findElement(By.id("shipping_state")));
	   state.selectByVisibleText(statename);
	   //return element;	
	}
	
	public static WebElement zipcode(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_zip"));
	   return element;	
	}
	
	public static WebElement billing_chkbox(WebDriver driver)
	{
	   element = driver.findElement(By.id("is_billing_address"));
	   return element;	
	}
	
	
	public static WebElement continuetopayment(WebDriver driver)
	{
	   
	   element = driver.findElement(By.id("deliverySubmitBtn"));
	   return element;	
	}
		
	public static WebElement defaultaddressname(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_addressName_read_only"));
	   return element;	
	}
	
	public static WebElement guestemail(WebDriver driver)
	{
	   element = driver.findElement(By.id("emailAddress"));
	   return element;	
	}
	
	public static WebElement guestcnfrmemail(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_confirm_email"));
	   return element;	
	}
	
	public static void selectintuitive_valid_adrs(WebDriver driver)
	{
	   WebElement ele=driver.findElement(By.xpath("html/body/ul/li[5]/a"));
	   ele.click();
			
	}
	
	public static WebElement intuitive_invalid_adrs(WebDriver driver)
	{
	   element = driver.findElement(By.xpath(".//*[@id='item_1']/a"));
	   return element;	
	}
	
		
	public static WebElement guestemail_err(WebDriver driver)
	{
	   element = driver.findElement(By.xpath(".//*[@id='form_guest']/div[1]/div/p/span[1]"));
	   return element;	
	}
	 
	public static WebElement guestcheckout_button(WebDriver driver)
	{
	   element = driver.findElement(By.id("guest-btn"));
	   return element;	
	}
	
	public static WebElement guestfname_err(WebDriver driver)
	{
	   element = driver.findElement(By.xpath(".//*[@id='p_shippingAddress.firstName']/span"));
	   return element;	
	}
	
	public static WebElement guestlname_err(WebDriver driver)
	{
	   element = driver.findElement(By.xpath(".//*[@id='p_shippingAddress.lastName']/span"));
	   return element;	
	}
	
	public static WebElement guestphone_err(WebDriver driver)
	{
	   element = driver.findElement(By.xpath(".//*[@id='p_shippingAddress.phone']/span"));
	   return element;	
	}
	
	public static WebElement inst_name(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_instname"));
	   return element;	
	}
	
	public static WebElement instname_err(WebDriver driver)
	{
	   element = driver.findElement(By.xpath(".//*[@id='p_companyName']/span"));
	   return element;	
	}
	
	public static WebElement customadrsselect_btn(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_defaultAddress_custom"));
	   return element;	
	}
	
	public static WebElement suggestedadrsselect_btn(WebDriver driver)
	{
	   element = driver.findElement(By.id("shipping_defaultAddress_suggested"));
	   return element;	
	}	
	
	public static WebElement customadrsedit_btn(WebDriver driver)
	{
	   element = driver.findElement(By.id("edit_shipping_custom_address"));
	   return element;	
	}
	
	public static WebElement verifyaddresscontinue_btn(WebDriver driver)
	{
	   element = driver.findElement(By.id("continueshippingSgtBtn"));
	   return element;	
	}
	
	public static WebElement billingaddress2(WebDriver driver)
	{
	   element = driver.findElement(By.id("billing_address2"));
	   return element;	
	}	
	
		
	public static void shippingaccordion(WebDriver driver)
	{
		firstname(driver).sendKeys("ksk");
		lastname(driver).sendKeys("ksk");
		phone(driver).click();
		phone(driver).sendKeys("9523585668");
		address(driver).sendKeys("nottingham");
		address1(driver).sendKeys("my home hub");
		city(driver).sendKeys("boston");
		selectstate(driver,"New York");
		zipcode(driver).sendKeys("10002");
		continuetopayment(driver).click();
		
	}
	
	public static void click_continuepayment(WebDriver driver)
	{
       String s=continuetopayment(driver).getAttribute("value");
       System.out.println(s);
       if(s.equals("Continue to Payment & Billing")){
    	   continuetopayment(driver).click();
       }
       else{
    	   driver.navigate().refresh();
    	   driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
    	   continuetopayment(driver).click();    	  
       }
	}
	
	public static boolean isElementPresent(WebDriver driver,String x)
	{
	    try
	    {
	        driver.findElement(By.xpath(x));
	        return true;
	    }
	    catch(org.openqa.selenium.NoSuchElementException e)
	    {
	        return false;
	    }
	}
	
	
}
