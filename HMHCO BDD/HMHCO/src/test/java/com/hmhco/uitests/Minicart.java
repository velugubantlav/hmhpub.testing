package com.hmhco.uitests;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hmhco.pageobjects.Checkoutpage;
import com.hmhco.pageobjects.DriverController;
import com.hmhco.pageobjects.Educators_homepage;
import com.hmhco.pageobjects.Loginpage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Minicart extends DriverController {

	@Given("^the User has added only Trade products in the Cart$")
	public void the_User_has_added_only_Trade_products_in_the_Cart() throws Exception {
	    getAndResetDriver();
	    driver.get("http://hmhco-v1.stg.techspa.com/at-home"); 
	    Educators_homepage.addtocart(driver, "9780544022638");
	    
	}

	@Given("^the value of products ranges between \\$(\\d+) and \\$(\\d+)$")
	public void the_value_of_products_ranges_between_$_and_$(int arg1, int arg2) throws Exception {
	    int min=arg1; int max=arg2;
	    Actions act=new Actions(driver);
	    act.moveToElement(Educators_homepage.cart_button(driver));
	    //System.out.println(Educators_homepage.finalprice(driver).getText());
	    Thread.sleep(1000);
	    act.moveToElement(Educators_homepage.firstproduct(driver)).build().perform();
	    driver.findElement(By.xpath(".//*[@id='main']/section/div/section/article/section[5]/div/div/div/div/p[1]/a[2]")).click();
	    driver.findElement(By.xpath(".//*[@id='main']/section/div/section/article/section[5]/div/div/div/div/p[1]/a[2]")).click();
	    driver.findElement(By.xpath(".//*[@id='main']/section/div/section/article/section[5]/div/div/div/div/p[1]/a[2]")).click();
	    driver.findElement(By.xpath(".//*[@id='main']/section/div/section/article/section[5]/div/div/div/div/p[1]/a[2]")).click();
	    WebDriverWait wait=new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='body1_0_lstvResults_ctrl0_P3_0']/a"))).click();
	}

	@When("^the User views the Mini-Cart$")
	public void the_User_views_the_Mini_Cart() throws Exception {
		WebDriverWait wait=new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='cartButton']/a")));
		Actions act=new Actions(driver);
	    act.moveToElement(Educators_homepage.cart_button(driver)).build().perform();
	    System.out.println(Educators_homepage.finalprice(driver).getText());
	    Thread.sleep(2000);
	}

	@Then("^the free shipping message should be displayed to the User in the Mini-Cart area$")
	public void the_free_shipping_message_should_be_displayed_to_the_User_in_the_Mini_Cart_area()  {
	    if(Educators_homepage.freeshpngmsg(driver).isDisplayed()){
	    	System.out.println(Educators_homepage.freeshpngmsg(driver).getText());
	    	System.out.println("free shipping message is displayed");
	    }
	    else{
	    	System.out.println("free shipping message is not displayed");
	    }
	}
	
	@Given("^the User has added only K(\\d+) products in the Cart$")
	public void the_User_has_added_only_K_products_in_the_Cart(int arg1) throws Throwable {
		getAndResetDriver();
	    driver.get("http://hmhco-v1.stg.techspa.com/at-home"); 
	    Educators_homepage.addtocart(driver, "9780618957620");
	}	
	
	@Given("^the Guest/ Registered Individual User has added Trade & K(\\d+) products in the Cart$")
	public void the_Guest_Registered_Individual_User_has_added_Trade_K_products_in_the_Cart(int arg1) throws Throwable {
		getAndResetDriver();
	    driver.get("http://hmhco-v1.stg.techspa.com/at-home"); 
	    Educators_homepage.addtocart(driver, "9780544022638");
	    Educators_homepage.addtocart(driver, "9780618957620");
	}	
	
	@Given("^the Guest/ Registered Individual User has added Physical products in the Cart worth \\$(\\d+)$")
	public void the_Guest_Registered_Individual_User_has_added_Physical_products_in_the_Cart_worth_$(int arg1) throws Throwable {
		getAndResetDriver();
	    driver.get("http://hmhco-v1.stg.techspa.com/at-home"); 
	    Educators_homepage.addtocart(driver, "9780544022638");
		
	}

	@Given("^the User has added Digital products in the Cart worth \\$(\\d+),total value of the Cart is \\$(\\d+)$")
	public void the_User_has_added_Digital_products_in_the_Cart_worth_$_total_value_of_the_Cart_is_$(int arg1, int arg2) throws Throwable {
		Educators_homepage.addtocart(driver, "9780030998867");
	}

	@Then("^the free shipping message should NOT be displayed to the User in the Mini-Cart area$")
	public void the_free_shipping_message_should_NOT_be_displayed_to_the_User_in_the_Mini_Cart_area() throws Throwable {
		String s="You've earned free shipping!";
		if(Educators_homepage.freeshpngmsg(driver).isDisplayed() & Educators_homepage.freeshpngmsg(driver).getText().contains(s)){
	    	System.out.println(Educators_homepage.freeshpngmsg(driver).getText());
	    	System.out.println("free shipping message is displayed");
	    }
	    else{
	    	System.out.println("free shipping message is not displayed");
	    }
	}
	
	@Given("^an Institutional User has added products in the Cart$")
	public void an_Institutional_User_has_added_products_in_the_Cart() throws Throwable {
		getAndResetDriver();
	    driver.get("http://hmhco-v1.stg.techspa.com/at-home"); 
	    Loginpage.signin(driver, "instauta1@mailinator.com", "password1");
	    Actions act=new Actions(driver);
	    act.moveToElement(Educators_homepage.cart_button(driver)).build().perform();
	    if(Checkoutpage.isElementPresent(driver, ".//*[@id='cart_items_tbody']/tr[1]/td[6]/a")){
	      driver.findElement(By.xpath(".//*[@id='cart_items_tbody']/tr[1]/td[6]/a")).click();	
	    }
	    
	    Educators_homepage.addtocart(driver, "9780153494185");
	}
	
	@Given("^the value of products ranges between \\$(\\d+) & \\$(\\d+)$")
	public void the_value_of_products_ranges_between_$_$(int arg1, int arg2) throws Throwable {
		Actions act=new Actions(driver);
	    act.moveToElement(Educators_homepage.cart_button(driver));
	    System.out.println(Educators_homepage.finalprice(driver).getText());
	    Thread.sleep(1000);
	}
		
}
