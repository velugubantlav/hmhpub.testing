package com.hmhco.uitests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hmhco.pageobjects.DriverController;
import com.hmhco.pageobjects.Widgetpage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Widget extends DriverController{

	@Given("^the Categories are defined in the system$")
	public void the_Categories_are_defined_in_the_system() throws Throwable {
	    System.out.println("All the categories are defined");
	}

	@Given("^the widget has been implemented for a page$")
	public void the_widget_has_been_implemented_for_a_page() throws Throwable {
	    System.out.println("Widget has been implemented for the page");
	}

	@When("^the User navigates to that page$")
	public void the_User_navigates_to_that_page() throws Throwable {
	    getAndResetDriver();
	    driver.get("http://hmhco-v1.stg.techspa.com/ClassRoom/Widgets/CategoryAdobeDataFeedWidget");
	}

	@Then("^the widget should be loaded on the page$")
	public void the_widget_should_be_loaded_on_the_page() throws Throwable {
	    if(Widgetpage.widget(driver).isDisplayed()){
	    	System.out.println("widget is displayed");
	    }
	
	    else{
	    	System.out.println("widget is not displayed");
	    	}
	    }
	

	@Then("^the Title entered should be displayed$")
	public void the_Title_entered_should_be_displayed() throws Throwable {
	    if(Widgetpage.widget_title(driver).isDisplayed()){
	    	System.out.println("title is displayed:  "+Widgetpage.widget_title(driver).getText());
	    }
	}

	@Then("^Categories should be displayed$")
	public void categories_should_be_displayed() throws Throwable {
	    List<WebElement> cat=Widgetpage.widget(driver).findElements(By.tagName("a"));
	    int catcount=cat.size();
	    System.out.println(catcount);
	    if(catcount>0){
	    	System.out.println("categories are displayed");
	     }
	    else{
	    	System.out.println("categories are not displayed");
	    }
	    	
	}

	@Then("^number of products under each Category will be displayed to the User beside the Category name$")
	public void number_of_products_under_each_Category_will_be_displayed_to_the_User_beside_the_Category_name() throws Throwable {
		List<WebElement> cat=Widgetpage.widget(driver).findElements(By.tagName("a"));
	    int catcount=cat.size();
	    for(int i=0;i<catcount;i++){
	    	//System.out.println(cat.get(i).getText());
	    	String catitem=cat.get(i).getText();
	    	Matcher m = Pattern.compile("\\((.*?)\\)").matcher(catitem);
	    	while(m.find()) {
	    	    //System.out.println(m.group(1));
	    	    if(m.group(1)!=null){
	    	    	System.out.println("number of products under "+ cat.get(i).getText() +" Category is displayed ");
	    	    }
	    	    else{
	    	    	System.out.println("number of products under each Category is not displayed ");
	    	    }
	    	}
	    }
	}

	@Then("^Categories should be ordered alphabetically from top to bottom$")
	public void categories_should_be_ordered_alphabetically_from_top_to_bottom() throws Throwable {
//		List<WebElement> cat=Widgetpage.widget(driver).findElements(By.tagName("a"));
//		int catcount=cat.size();
//	    for(int i=0;i<catcount;i++){
//	    	List<String> ml=cat.get(i).getText();
				
	}

	@Given("^that the User navigates to a page on which the widget has been implemented$")
	public void that_the_User_navigates_to_a_page_on_which_the_widget_has_been_implemented() throws Throwable {
	   getAndResetDriver();
	   driver.get("http://hmhco-v1.stg.techspa.com/ClassRoom/Widgets/CategoryAdobeDataFeedWidget");
	}

	@When("^the User clicks on a Category displayed in the widget$")
	public void the_User_clicks_on_a_Category_displayed_in_the_widget() throws Throwable {
	    Widgetpage.category(driver).click();
	}

	@Then("^the selected Category search page should load$")
	public void the_selected_Category_search_page_should_load() throws Throwable {
	    driver.manage().timeouts().pageLoadTimeout(7, TimeUnit.SECONDS);
	    String keyword=driver.findElement(By.xpath(".//*[@id='body1_0_BreadCrumDiv']/div[1]/p/a[3]")).getText();
	    System.out.println(keyword);
	    if(keyword.equals("FICTION")){
	    	System.out.println("Selected category page is loaded");
	    }
	    else{
	    	System.out.println("Selected category page is not loaded");
	    }
	}

	@Then("^all products under that category should be displayed to the User$")
	public void all_products_under_that_category_should_be_displayed_to_the_User() throws Throwable {
	    String results=driver.findElement(By.xpath(".//*[@id='main']/section/div/section/article/section[3]/div[1]/p/span[1]")).getText();
	    String[] rescount=results.split("-");
	    //System.out.println(rescount[0]+"       " +rescount[1]);
	    if(rescount[0].equals(0) & rescount[1].equals(0)){
	    	System.out.println("Search results are not displayed");
	    }
	    else{
	    	System.out.println("Search results are displayed");
	    }
	}
	
	@When("^the the number of Categories are less than (\\d+) or more than (\\d+)$")
	public void the_the_number_of_Categories_are_less_than_or_more_than(int arg1, int arg2) throws Throwable {
	    int rows=arg1;
	    int cols=arg2;
		System.out.println("no.of rows: "+ rows +" & no.of cols: "+cols);
		
	}
	
	@Then("^the categories should still be displayed using a (\\d+)-column structure$")
	public void the_categories_should_still_be_displayed_using_a_column_structure(int arg1) throws Throwable {
	    int column_count=arg1;
		List<WebElement> catcols=driver.findElement(By.id("body1_0_rightsectiondata_0_ChildItemDiv")).findElements(By.tagName("ul"));
	    System.out.println("No.of columns displayed"+catcols.size());
	    if(column_count==catcols.size()){
	    	System.out.println(column_count+" columns are getting displayed");
	    }
	    else{
	    	System.out.println(column_count+" columns are not getting displayed");
	    }
	}



}

