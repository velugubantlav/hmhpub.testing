package com.hmhco.uitests;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.hmhco.pageobjects.Checkoutpage;
import com.hmhco.pageobjects.DriverController;
import com.hmhco.pageobjects.Educators_homepage;
import com.hmhco.pageobjects.Loginpage;
import com.hmhco.pageobjects.MyAccountPage;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Checkout extends DriverController  {
	
	public Checkout(){
	}
	String modified_address="default24";
  	
@Given("^the User is a registered User of HMHCO$")
	public void the_User_is_a_registered_User_of_HMHCO() throws Throwable {
        getAndResetDriver();
        driver.get("https://hmhco-v1.stg.techspa.com/classroom");
		System.out.println("user is already registered user");	
	}

@Given("^the User is logged in on HMHCO$")
	public void the_User_is_logged_in_on_HMHCO() throws Throwable {
	    Loginpage.signin(driver, "pershmh1233@mailinator.com", "password1");
	}

@Given("^the User has added products to the cart$")
	public void the_User_has_added_products_to_the_cart() throws Throwable {
	    Educators_homepage.addtocart(driver, "Biography");
		
	}

@Given("^the User has already added Shipping Addresses$")
	public void the_User_has_already_added_Shipping_Addresses() throws Throwable {
	    System.out.println("address already added");
		
	}

@When("^the User clicks on Proceed to Checkout$")
	public void the_User_clicks_on_Proceed_to_Checkout() throws Throwable {
	   Educators_homepage.proceedtocheckout(driver);
		
	}

@Then("^the User should see the Shipping Accordion expanded$")
	public void the_User_should_see_the_Shipping_Accordion_expanded() throws Throwable {
	    if(Checkoutpage.addaddress_link(driver).isDisplayed())
	    {
	    	System.out.println("Shipping accordion is displayed");
	    }
	    else{
	        System.out.println("Shipping accordion is not displayed");
	    }
	    }
	

@Then("^the User should see default shipping address selected for Shipping the order$")
	public void the_User_should_see_default_shipping_address_selected_for_Shipping_the_order() throws Throwable {
	    String defaddress=Checkoutpage.defaultaddress(driver).getText();
		if(defaddress!=null){
	    	
	    	System.out.println("default address :" + "'" + defaddress + "' "+ " is selected");
	    }
		else{
			System.out.println("empty");
		}
		
	}

@Then("^the Shipping Address drop-down to select any previously added address$")
	public void the_Shipping_Address_drop_down_to_select_any_previously_added_address() throws Throwable {

          if(Checkoutpage.defaultaddress(driver).isDisplayed()){
        	  
        	 System.out.println("Address Dropdown is displayed");
          }
          else{
           System.out.println("Address dropdown is not displayed");
          }
		
	}

@Then("^the Edit & Delete option should be available for the user$")
	public void the_Edit_Delete_option_should_be_available_for_the_user() throws Throwable {
	     if(Checkoutpage.editaddress_link(driver).isDisplayed() & Checkoutpage.deleteaddress_link(driver).isDisplayed()  ){
	    	 System.out.println("Edit and Delete options are displayed");
	     }
	     else
	     {
	    	 System.out.println("Edit and Delete options are not displayed");
	     }
	}

@Then("^the Remove Default option should be available to the user$")
	public void the_Remove_Default_option_should_be_available_to_the_user() throws Throwable {
	    if(Checkoutpage.removedefault_link(driver).isDisplayed())
	    {
	    	System.out.println("remove default option is displayed");
	    }
	    else{
	    	System.out.println("remove default option is not displayed");
	    }
		
	}

@Then("^Add Address option should be available to the user to add a new shipping address$")
	public void add_Address_option_should_be_available_to_the_user_to_add_a_new_shipping_address() throws Throwable {
		if(Checkoutpage.addaddress_link(driver).isDisplayed())
	    {
	    	System.out.println("add address link is displayed");
	    }
	    else{
	    	System.out.println("add address link is not displayed");
	    }
		driver.close();
	}

@Given("^the user clicks on Proceed to checkout button$")
	public void the_user_clicks_on_Proceed_to_checkout_button() throws Throwable {
	Thread.sleep(3000);    
	Educators_homepage.proceedtocheckout(driver);
	}

@When("^the User clicks on Edit for the selected Shipping Address$")
	public void the_User_clicks_on_Edit_for_the_selected_Shipping_Address() throws Throwable {
    Checkoutpage.editaddress_link(driver).click();
    driver.manage().timeouts().setScriptTimeout(4, TimeUnit.SECONDS);
	}

@Then("^the Shipping accordion should spin up the edit address form with pre-populated address information in the existing fields$")
	public void the_Shipping_accordion_should_spin_up_the_edit_address_form_with_pre_populated_address_information_in_the_existing_fields() throws Throwable {
    if(Checkoutpage.firstname(driver).getText()!=null & Checkoutpage.lastname(driver).getText()!=null)
    {
        System.out.println("information is pre-populated");  
      }
    else
    {
    	System.out.println("information is not pre-populated");
    }
	
	}
	
@Then("^the User should be able to make changes to the address$")
	public void the_User_should_be_able_to_make_changes_to_the_address() throws Throwable {
   
    if(Checkoutpage.address1(driver).isEnabled())
    {
    	System.out.println("user able to edit the address");
    }
    else
    {
    	System.out.println("user unable to edit the address");      
    }
	}

@Then("^the Save Address & Cancel options should be available to the User$")
 	public void the_Save_Address_Cancel_options_should_be_available_to_the_User() throws Throwable {
    if(Checkoutpage.saveaddress_button(driver).isDisplayed() & Checkoutpage.shippingcancel_button(driver).isDisplayed())
    {
    	System.out.println("Save address and Cancel buttons are displayed to the user");
    }
    else
    {
    	System.out.println("Save address and Cancel buttons are not displayed to the user");
    }
    driver.close();
	}

@Given("^the User is editing a Shipping address under the Checkout flow$")
	public void the_User_is_editing_a_Shipping_address_under_the_Checkout_flow() throws Throwable {
       Educators_homepage.proceedtocheckout(driver);
       Checkoutpage.editaddress_link(driver).click();
       driver.manage().timeouts().setScriptTimeout(4, TimeUnit.SECONDS);
       Checkoutpage.address(driver).clear();
       Checkoutpage.address(driver).sendKeys(modified_address);
       
	}

@When("^the User clicks on Save Address for the selected Shipping Address$")
	public void the_User_clicks_on_Save_Address_for_the_selected_Shipping_Address() throws Throwable {
     Checkoutpage.saveaddress_button(driver).click();
	}

@Then("^the changes to the shipping address should be saved$")
	public void the_changes_to_the_shipping_address_should_be_saved() throws Throwable {
    driver.manage().timeouts().setScriptTimeout(4,TimeUnit.SECONDS); 
    System.out.println(Checkoutpage.defaultaddress(driver).getText());
    if(Checkoutpage.defaultaddress(driver).getText().contains(modified_address)){
    	 System.out.println("Modified address is saved");
     }
	else{
		System.out.println("Modified address not saved");
	}
	
	}

@Then("^these changes should be reflected under the Address Book in My Account$")
	public void these_changes_should_be_reflected_under_the_Address_Book_in_My_Account() throws Throwable {
    driver.navigate().to("http://hmhco-v1.stg.techspa.com/account/address-book");
    driver.manage().timeouts().setScriptTimeout(4,TimeUnit.SECONDS);
    if(MyAccountPage.addresstitle(driver).getText().contains(modified_address)){
    	System.out.println("changes are reflected under My Account page");
    }
    else{
    	System.out.println("changes are not reflected under My Account page");
    }
    driver.close();
    }

    
@When("^the User clicks on Cancel for the selected Shipping Address$")
	public void the_User_clicks_on_Cancel_for_the_selected_Shipping_Address() throws Throwable {
	
	Checkoutpage.shippingcancel_button(driver).click();
	    
	}

@Then("^the changes to the shipping address should be discarded$")
	public void the_changes_to_the_shipping_address_should_be_discarded() throws Throwable {
	driver.manage().timeouts().setScriptTimeout(2,TimeUnit.SECONDS);
	
	System.out.println(Checkoutpage.defaultaddress(driver).getText());
	if(Checkoutpage.defaultaddressname(driver).getText()=="default"){
   	 System.out.println("changes are discarded");
    }
	else{
		System.out.println("changes are saved");
	}
	
	
	}

@Then("^the Shipping accordion should spin back to the default state with the address selected as Shipping Address$")
	public void the_Shipping_accordion_should_spin_back_to_the_default_state_with_the_address_selected_as_Shipping_Address() throws Throwable {
	if(Checkoutpage.addaddress_link(driver).isDisplayed())
	{  
	 System.out.println("Shipping accordion is back to the default state");	 
	}
	else
	{
	 System.out.println("Shipping accordion is not yet back to the default state");
	}
	driver.close();
 	}
	
@Given("^the User is on the Checkout flow$")
	public void the_User_is_on_the_Checkout_flow() throws Throwable {
    Thread.sleep(3000);
	Educators_homepage.proceedtocheckout(driver);
    
}

@When("^the User clicks on Delete the selected Shipping Address$")
	public void the_User_clicks_on_Delete_the_selected_Shipping_Address() throws Throwable 
	{
    driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
    Checkoutpage.deleteaddress_link(driver).click();
    driver.manage().timeouts().setScriptTimeout(2, TimeUnit.SECONDS);
    Checkoutpage.deletecnfrmtn_link(driver).click();
    driver.manage().timeouts().setScriptTimeout(8, TimeUnit.SECONDS);
	}

@Then("^the particular address should get deleted and the next address should automatically be selected or if another address is not available, the Shipping accordion should spin up the Add address form$")
	public void the_particular_address_should_get_deleted_and_the_next_address_should_automatically_be_selected_or_if_another_address_is_not_available_the_Shipping_accordion_should_spin_up_the_Add_address_form() throws Throwable {
    
	
	if(Checkoutpage.saveaddress_button(driver).isDisplayed())
	{
		System.out.println("add new address form is displayed");
	}
	else if(Checkoutpage.removedefault_link(driver).isDisplayed())
	{
	    System.out.println("another address is set as default");	
	}
	driver.close();
   	}

@When("^the User chooses any other address apart from default address from the Shipping Address drop-down$")
	public void the_User_chooses_any_other_address_apart_from_default_address_from_the_Shipping_Address_drop_down() throws Throwable {
	
	driver.findElement(By.xpath(".//*[@id='shippingAddressDropDownList_chosen']/a")).click();
	driver.findElement(By.xpath(".//*[@id='shippingAddressDropDownList_chosen']/div/ul/li[3]")).click();
    System.out.println(driver.findElement(By.id("shippingAddressDropDownList_chosen")).getText());
	
	}


@Then("^the Set as Default option should be available to the user$")
	public void the_Set_as_Default_option_should_be_available_to_the_user() throws Throwable {
    
	if(Checkoutpage.setdefault_link(driver).isDisplayed()){
      System.out.println("set as default link is displayed");
	}
	else
	{
	  System.out.println("set as default link is not displayed");
	}
	
	}

@Given("^the Guest User is at Checkout on HMHCO$")
public void the_Guest_User_is_at_Checkout_on_HMHCO() throws Throwable {
    getAndResetDriver();
    driver.get("https://hmhco-v1.stg.techspa.com/classroom");
	Educators_homepage.addtocart(driver, "biography");
    Educators_homepage.proceedtocheckout(driver);
    
	}

@When("^the User provides a valid email address \"(.*?)\"$")
public void the_User_provides_a_valid_email_address(String arg1) throws Throwable {
    String validid=arg1;
    Checkoutpage.guestemail(driver).sendKeys(validid);
    
	}

@When("^the User clicks on Checkout as a Guest$")
public void the_User_clicks_on_Checkout_as_a_Guest() throws Throwable {
    Checkoutpage.guestcheckout_button(driver).click();
	}

@Then("^the system should not give any email validation related error$")
public void the_system_should_not_give_any_email_validation_related_error() throws Throwable {
    driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
    if(Checkoutpage.continuetopayment(driver).isDisplayed()){
       System.out.println("valid email");
    }
    else{
       System.out.println("not valid email");
    }
    driver.quit();
	}
    
@When("^the User provides an invalid email address \"(.*?)\" or the User leaves the field empty \"(.*?)\"$")
public void the_User_provides_an_invalid_email_address_or_the_User_leaves_the_field_empty(String arg1, String arg2) throws Throwable {
    driver.manage().timeouts().pageLoadTimeout(5,TimeUnit.SECONDS);
	Checkoutpage.guestemail(driver).sendKeys(arg1);
    
	}

@Then("^the system should give an error message \"(.*?)\"$")
public void the_system_should_not_give_an_error_message(String arg1) throws Throwable {
    System.out.println(Checkoutpage.guestemail_err(driver).getText());
    System.out.println(arg1);
	if(Checkoutpage.guestemail_err(driver).getText().contains(arg1)){
      System.out.println("Correct error message is displayed");	
    }
    else{
      System.out.println("Incorrect error message is displayed");
    }
	driver.quit();
	}

@When("^the User provides a valid first name$")
public void the_User_provides_a_valid_first_name() throws Throwable {
	the_User_provides_a_valid_email_address("valid@mail.com");
	the_User_clicks_on_Checkout_as_a_Guest();
	Checkoutpage.firstname(driver).sendKeys("First Name");
	Checkoutpage.lastname(driver).click();
}

@Then("^the system should not give any field validation related error$")
public void the_system_should_not_give_any_field_validation_related_error() throws Throwable {
   if(Checkoutpage.isElementPresent(driver, ".//*[@id='p_shippingAddress.firstName']/span")){
	    System.out.println("Field validation error is displayed");
    }
    else{
    	System.out.println("No field validation error is displayed");
    }
   driver.quit();
}

@When("^the User leaves the firstname field empty$")
public void the_User_leaves_the_firstname_field_empty() throws Throwable {
  the_User_provides_a_valid_email_address("valid@mail.com");
  the_User_clicks_on_Checkout_as_a_Guest();
  Thread.sleep(3000);
  Checkoutpage.firstname(driver).sendKeys(" ");
  Checkoutpage.continuetopayment(driver).click();
}

@Then("^the system should give an error message as \"(.*?)\"$")
public void the_system_should_give_an_error_message_as(String arg1) throws Throwable {
    String s=arg1;   
	if(Checkoutpage.guestfname_err(driver).getText().contains(s)){
	   System.out.println("Correct error message is displayed");
   }
	else{
		System.out.println("Incorrect error message is displayed");
	}
	driver.quit();
}

@When("^the User provides an invalid first name$")
public void the_User_provides_an_invalid_first_name() throws Throwable {
	the_User_provides_a_valid_email_address("valid@mail.com");
	the_User_clicks_on_Checkout_as_a_Guest();
	Thread.sleep(3000);
	Checkoutpage.firstname(driver).sendKeys("v");
	Checkoutpage.lastname(driver).click();
    }

@Then("^the system should display a message \"(.*?)\"$")
public void the_system_should_display_a_message(String arg1) throws Throwable {
	String s=arg1;
	System.out.println(Checkoutpage.guestfname_err(driver).getText());
	if(Checkoutpage.guestfname_err(driver).getText().contains(s)){
    	System.out.println("Correct error message is displayed");
    }
	else{
		System.out.println("Incorrect error message is displayed");
	}
	driver.quit();
}

@Then("^the system should display an error message \"(.*?)\"$")
public void the_system_should_display_an_error_message(String arg1) throws Throwable {
    String s=arg1;
	if(Checkoutpage.guestfname_err(driver).getText().contains(s)){
    	System.out.println("Correct error message is displayed");
    }
	else{
		System.out.println("Incorrect error message is displayed");
	}
	driver.quit();
}

@When("^the User provides a valid Last name$")
public void the_User_provides_a_valid_Last_name() throws Throwable {
	the_User_provides_a_valid_email_address("valid@mail.com");
	the_User_clicks_on_Checkout_as_a_Guest();
	Checkoutpage.lastname(driver).sendKeys("Last Name");
	Checkoutpage.click_continuepayment(driver);
}

@Then("^the system should not give any last name field validation related error$")
public void the_system_should_not_give_any_last_name_field_validation_related_error() throws Throwable {
	 if(Checkoutpage.isElementPresent(driver, ".//*[@id='p_shippingAddress.lastName']/span")){
		    System.out.println("Field validation error is displayed");
	    }
	    else{
	    	System.out.println("No field validation error is displayed");
	    }
	 driver.quit();
}

@When("^the User leaves the lastname field empty$")
public void the_User_leaves_the_lastname_field_empty() throws Throwable {
	the_User_provides_a_valid_email_address("valid@mail.com");
	  the_User_clicks_on_Checkout_as_a_Guest();
	  Thread.sleep(3000);
	  Checkoutpage.lastname(driver).sendKeys(" ");
	  Checkoutpage.click_continuepayment(driver);
}

@Then("^the system should give an error message(\\d+) \"(.*?)\"$")
public void the_system_should_give_an_error_message(int arg1, String arg2) throws Throwable {
	String s=arg2;
	System.out.println(Checkoutpage.guestlname_err(driver).getText());
	if(Checkoutpage.guestlname_err(driver).getText().contains(s)){
    	System.out.println("Correct error message is displayed");
    }
	else{
		System.out.println("Incorrect error message is displayed");
	}
	driver.quit();
}

@When("^the User provides an invalid Last name$")
public void the_User_provides_an_invalid_Last_name() throws Throwable {
	the_User_provides_a_valid_email_address("valid@mail.com");
	the_User_clicks_on_Checkout_as_a_Guest();
	Thread.sleep(3000);
	Checkoutpage.lastname(driver).sendKeys("v");
	Checkoutpage.firstname(driver).click();
}

@Then("^the system should give error message \"(.*?)\"$")
public void the_system_should_give_error_message(String arg1) throws Throwable {
	String s=arg1;
	System.out.println(Checkoutpage.guestlname_err(driver).getText());
	if(Checkoutpage.guestlname_err(driver).getText().contains(s)){
    	System.out.println("Correct error message is displayed");
    }
	else{
		System.out.println("Incorrect error message is displayed");
	}
	driver.quit();
}

@When("^the User provides a valid Phone number$")
public void the_User_provides_a_valid_Phone_number() throws Throwable {
	the_User_provides_a_valid_email_address("valid@mail.com");
	the_User_clicks_on_Checkout_as_a_Guest();
	Thread.sleep(5000);
	Checkoutpage.phone(driver).click();
	Checkoutpage.phone(driver).sendKeys("9898987878");
	Checkoutpage.click_continuepayment(driver);
}

@Then("^the system should not give any phone number field validation related error$")
public void the_system_should_not_give_any_phone_number_field_validation_related_error() throws Throwable {
	if(Checkoutpage.isElementPresent(driver, ".//*[@id='p_shippingAddress.phone']/span")){
	    System.out.println("Field validation error is displayed");
    }
    else{
    	System.out.println("No field validation error is displayed");
    }
	driver.quit();
}

@When("^the User provides an invalid Phone number$")
public void the_User_provides_an_invalid_Phone_number() throws Throwable {
	the_User_provides_a_valid_email_address("valid@mail.com");
	the_User_clicks_on_Checkout_as_a_Guest();
	Thread.sleep(5000);
	Checkoutpage.phone(driver).click();
	Checkoutpage.phone(driver).sendKeys("9897878");
	Checkoutpage.click_continuepayment(driver);
}

@Then("^the system should display error message \"(.*?)\"$")
public void the_system_should_display_error_message(String arg1) throws Throwable {
	String s=arg1;
	System.out.println(Checkoutpage.guestphone_err(driver).getText());
	if(Checkoutpage.guestphone_err(driver).getText().contains(s)){
    	System.out.println("Correct error message is displayed");
    }
	else{
		System.out.println("Incorrect error message is displayed");
	}
	driver.quit();
}

@Given("^the Registered User is at Checkout on HMHCO$")
public void the_Registered_User_is_at_Checkout_on_HMHCO() throws Throwable {
    getAndResetDriver();
    driver.get("https://hmhco-v1.stg.techspa.com/classroom");
    Loginpage.signin(driver, "pershmh1233@mailinator.com", "password1");
    Educators_homepage.addtocart(driver, "biography");
    Educators_homepage.proceedtocheckout(driver);
}

@Given("^the User is creating a new Shipping or Billing Address$")
public void the_User_is_creating_a_new_Shipping_or_Billing_Address() throws Throwable {
    Checkoutpage.addaddress_link(driver).click();
}

@When("^the User provides a valid address nickname$")
public void the_User_provides_a_valid_address_nickname() throws Throwable {
    Checkoutpage.address(driver).sendKeys("valid");
    Checkoutpage.saveaddress_button(driver).click();
}

@Then("^the system should not give any address field validation related error$")
public void the_system_should_not_give_any_address_field_validation_related_error() throws Throwable {
    if(Checkoutpage.isElementPresent(driver, ".//*[@id='p_addressName']/span")){
    	System.out.println("field validation message is displayed");
    }
    else{
    	System.out.println("field validation message is not displayed");
    }
    driver.quit();
}

@When("^the User leaves the field empty$")
public void the_User_leaves_the_field_empty() throws Throwable {
	Checkoutpage.address(driver).sendKeys(" ");
    Checkoutpage.saveaddress_button(driver).click();
}

@Then("^the system should display an error message as \"(.*?)\"$")
public void the_system_should_display_an_error_message_as(String arg1) throws Throwable {
    String s=arg1;
	if(Checkoutpage.address_err(driver).getText().contains(s)){
    	System.out.println("correct error message is displayed");
    }
	else{
		System.out.println("Incorrect error message is displayed");
	}
	driver.quit();
}

@Given("^the Institutional User is at Checkout on HMHCO$")
public void the_Institutional_User_is_at_Checkout_on_HMHCO() throws Throwable {
    getAndResetDriver();
    driver.get("https://hmhco-v1.stg.techspa.com/classroom");
    Loginpage.signin(driver, "insthmh1233@mailinator.com", "password1");
    Educators_homepage.addtocart(driver, "biography");
    Educators_homepage.proceedtocheckout(driver);
}

@When("^the User provides a valid Institution name$")
public void the_User_provides_a_valid_Institution_name() throws Throwable {
    Checkoutpage.addaddress_link(driver).click();
    Checkoutpage.inst_name(driver).sendKeys("valid");
    Checkoutpage.saveaddress_button(driver).click();
    
}

@Then("^the system should not give any name field validation related error$")
public void the_system_should_not_give_any_name_field_validation_related_error() throws Throwable {
   if(Checkoutpage.isElementPresent(driver, ".//*[@id='p_companyName']/span")){
	   System.out.println("Field validation error is displayed");
   }
   else{
	   System.out.println("Field validation error is not displayed");
   }
   driver.quit();
}

@When("^the User leaves the institution field empty$")
public void the_User_leaves_the_institution_field_empty() throws Throwable {
	Checkoutpage.addaddress_link(driver).click();
    Checkoutpage.inst_name(driver).sendKeys(" ");
    Checkoutpage.saveaddress_button(driver).click();
}

@Then("^the system should give a message \"(.*?)\"$")
public void the_system_should_give_a_message(String arg1) throws Throwable {
   String s=arg1;
   if(Checkoutpage.instname_err(driver).getText().contains(s)){
	   System.out.println("Correct error message is displayed");
   }
   else{
	   System.out.println("Incorrect error message is displayed");
   }
   driver.quit();
}

@When("^the User provides an invalid Institution name$")
public void the_User_provides_an_invalid_Institution_name() throws Throwable {
	Checkoutpage.addaddress_link(driver).click();
    Checkoutpage.inst_name(driver).sendKeys("s");
    Checkoutpage.saveaddress_button(driver).click();
}

@Then("^system should give an error message \"(.*?)\"$")
public void system_should_give_an_error_message(String arg1) throws Throwable {
	String s=arg1;
	   if(Checkoutpage.instname_err(driver).getText().contains(s)){
		   System.out.println("Correct error message is displayed");
	   }
	   else{
		   System.out.println("Incorrect error message is displayed");
	   }
	   driver.quit();
}

@Given("^the User has selected a suggested Intuitive Address$")
public void the_User_has_selected_a_suggested_Intuitive_Address() throws Throwable {
	String validmail="valid@mail.com";
	Checkoutpage.guestemail(driver).sendKeys(validmail);
	Checkoutpage.guestcheckout_button(driver).click();
	Checkoutpage.firstname(driver).sendKeys("ksk");
	Checkoutpage.lastname(driver).sendKeys("ch");
	Checkoutpage.guestcnfrmemail(driver).sendKeys(validmail);
	Checkoutpage.phone(driver).click();
	Checkoutpage.phone(driver).sendKeys("9887548754");
	Checkoutpage.address1(driver).sendKeys("1945 Commonwealth Ave");
	//Checkoutpage.address1(driver).sendKeys(" Commonwealth Ave");
	Thread.sleep(2000);
	Checkoutpage.selectintuitive_valid_adrs(driver);
}

@When("^the User makes changes to the selected address$")
public void the_User_makes_changes_to_the_selected_address() throws Throwable {
	Thread.sleep(2000);
	Checkoutpage.address2(driver).sendKeys("2");
      
}

@When("^the User clicks on Continue to Payment & Billing button$")
public void the_User_clicks_on_Continue_to_Payment_Billing_button() throws Throwable {
    Checkoutpage.continuetopayment(driver).click();
}

@Then("^the System should send the Shipping Address to Address Verification service$")
public void the_System_should_send_the_Shipping_Address_to_Address_Verification_service() throws Throwable {
    if(Checkoutpage.isElementPresent(driver, ".//*[@id='form_shipping']/div[4]/div[1]/div/h3")){
    	System.out.println("Address has been sent to verification service");
    }
}

@Then("^the Suggested Address details should be displayed to the User \\(Verify Your Shipping Address page\\)$")
public void the_Suggested_Address_details_should_be_displayed_to_the_User_Verify_Your_Shipping_Address_page() throws Throwable {
	if(Checkoutpage.isElementPresent(driver, ".//*[@id='shipping_defaultAddress_suggested']")){
    	System.out.println("Suggested address is displayed");
    }
}

@Then("^only one Suggested Address should be displayed to the User, along with the User entered Address$")
public void only_one_Suggested_Address_should_be_displayed_to_the_User_along_with_the_User_entered_Address() throws Throwable {
    List<WebElement> columns= driver.findElements(By.xpath(".//*[@id='grid']/div"));
    if(columns.size()==2){
    	System.out.println("only one suggested address is displayed");
    }
    
}

@Then("^by default the Suggested Address should be appear as selected on the Verify Your Shipping Address page$")
public void by_default_the_Suggested_Address_should_be_appear_as_selected_on_the_Verify_Your_Shipping_Address_page() throws Throwable {
	if(Checkoutpage.isElementPresent(driver, ".//*[@id='suggested']/div/div[1]/b/span")){
    	System.out.println("Suggested address is selected as default");
    }
}

@Then("^the User should have the option of choosing to save the Suggested Address as is or choosing to save the User entered address or choose and edit the User entered address$")
public void the_User_should_have_the_option_of_choosing_to_save_the_Suggested_Address_as_is_or_choosing_to_save_the_User_entered_address_or_choose_and_edit_the_User_entered_address() throws Throwable {
   if(Checkoutpage.customadrsselect_btn(driver).isDisplayed() & Checkoutpage.suggestedadrsselect_btn(driver).isDisplayed() & Checkoutpage.customadrsedit_btn(driver).isDisplayed()){
	   System.out.println("User have the option of choosing to save the Suggested Address as is or choosing to save the User entered address or choose and edit the User entered address");
   }
   else{
	   System.out.println("options are not displayed");
   }
}

@Given("^the User submitted a changed Shipping Address$")
public void the_User_submitted_a_changed_Shipping_Address() throws Throwable {
	String validmail="valid@mail.com";
	Checkoutpage.guestemail(driver).sendKeys(validmail);
	Checkoutpage.guestcheckout_button(driver).click();
	Checkoutpage.firstname(driver).sendKeys("ksk");
	Checkoutpage.lastname(driver).sendKeys("ch");
	Checkoutpage.guestcnfrmemail(driver).sendKeys(validmail);
	Checkoutpage.phone(driver).click();
	Checkoutpage.phone(driver).sendKeys("9887548754");
	Checkoutpage.address1(driver).sendKeys("1945 Commonwealth Ave");
	//Checkoutpage.address1(driver).sendKeys(" Commonwealth Ave");
	Thread.sleep(2000);
	Checkoutpage.selectintuitive_valid_adrs(driver);
	Thread.sleep(2000);
	Checkoutpage.address2(driver).sendKeys("2");
	Checkoutpage.billing_chkbox(driver).click();
	Checkoutpage.continuetopayment(driver).click();
	
	
}

@Given("^the System is showing the Suggested Address details to the User$")
public void the_System_is_showing_the_Suggested_Address_details_to_the_User() throws Throwable {
    if(Checkoutpage.suggestedadrsselect_btn(driver).isDisplayed()){
    	System.out.println("suggested address is displayed");
    }
}

@When("^the User chooses to use the Suggested Address$")
public void the_User_chooses_to_use_the_Suggested_Address() throws Throwable {
    Checkoutpage.suggestedadrsselect_btn(driver).click();
    Checkoutpage.verifyaddresscontinue_btn(driver).click();
}

@Then("^the Suggested Address should be saved as the Shipping Address for the order$")
public void the_Suggested_Address_should_be_saved_as_the_Shipping_Address_for_the_order() throws Throwable {
    System.out.println(Checkoutpage.billingaddress2(driver).getText());
	if(Checkoutpage.billingaddress2(driver).getText().isEmpty()){
    	System.out.println("suggested address is saved as shipping address ");
    }
    else{
    	System.out.println("suggested address is not saved as shipping address ");
    }
}


@When("^the User chooses to save the Custom Address$")
public void the_User_chooses_to_save_the_Custom_Address() throws Throwable {
    Checkoutpage.customadrsselect_btn(driver).click();
    Checkoutpage.verifyaddresscontinue_btn(driver).click();
}

@Then("^the Custom Address should be saved as the Shipping Address for the order$")
public void the_Custom_Address_should_be_saved_as_the_Shipping_Address_for_the_order() throws Throwable {
    if(Checkoutpage.billingaddress2(driver).getText().isEmpty()){
    	System.out.println("custom address is not saved as shipping address");
    }
    else{
    	System.out.println("custom address is saved as shipping address");
    }
}

}


