package com.hmhco.uitests;

import java.util.List;

import org.junit.internal.runners.model.EachTestNotifier;
import org.omg.CORBA.INTERNAL;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hmhco.pageobjects.Checkoutpage;
import com.hmhco.pageobjects.DriverController;
import com.hmhco.pageobjects.Educators_homepage;
import com.hmhco.pageobjects.Loginpage;
import com.hmhco.pageobjects.MyAccountPage;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MyAccount extends DriverController  {

	
public String newaddress="newaddressnickname";
public String newfname="firstname";
	
	
@Given("^User is a registered User of HMHCO$")
public void user_is_a_registered_User_of_HMHCO() throws Throwable {
	getAndResetDriver();
    driver.get("https://hmhco-v1.pre.techspa.com/classroom");
	System.out.println("user is already registered user");	
}

@Given("^User is logged in on HMHCO$")
public void user_is_logged_in_on_HMHCO() throws Throwable {
	Loginpage.signin(driver, "pershmh@gmail.com", "password1");
}

@Given("^User has already added Shipping Addresses$")
public void user_has_already_added_Shipping_Addresses() throws Throwable {
	System.out.println("address already added");
}

@Given("^User is on My Account page$")
public void user_is_on_My_Account_page() throws Throwable {
	Educators_homepage.navigatetoMyAccount(driver);
}

@When("^User clicks on Address Book from the left menu$")
public void user_clicks_on_Address_Book_from_the_left_menu() throws Throwable {
    MyAccountPage.addressbook_link(driver).click();
}

@Then("^the Address Book should be displayed to the User along with already added addresses$")
public void the_Address_Book_should_be_displayed_to_the_User_along_with_already_added_addresses() throws Throwable {
    Thread.sleep(3000);
	WebElement ele=driver.findElement(By.xpath(".//*[@id='form_address_book']/ul/li[2]/div/section[1]/div/p"));
    System.out.println(ele.getText());
    String s=ele.getText();
    String[] count=s.split(" ");
    int x=Integer.parseInt(count[3]);
    if(x>0){
    	System.out.println(x + " addresses are displayed");
        }
    else{
    	System.out.println(x + " addresses are displayed");
    }
}

@Then("^Add New Address option should be available to the user to add a new shipping address$")
public void add_New_Address_option_should_be_available_to_the_user_to_add_a_new_shipping_address() throws Throwable {
    if(MyAccountPage.addaddress_link(driver).isDisplayed()){
    	System.out.println("Add New Address option is displayed");
     }
    else{
    	System.out.println("Add New Address option is not displayed");
    }
}

@Then("^User should see Edit & Delete option should be available for the user$")
public void user_should_see_Edit_Delete_option_should_be_available_for_the_user() throws Throwable {
    if(MyAccountPage.editaddress_link(driver).isDisplayed() & MyAccountPage.deleteaddress_link(driver).isDisplayed()){
    	System.out.println("Edit & Delete options are displayed");
    }
    else{
    	System.out.println("Edit & Delete options are not displayed");
    }
}

@Then("^User should see Remove Default option for the address which is set as default$")
public void user_should_see_Remove_Default_option_for_the_address_which_is_set_as_default() throws Throwable {
    if(MyAccountPage.removedefault_link(driver).getText().equals("remove default")){
    	System.out.println("remove default link is displayed");
        }
    else{
    	System.out.println("remove default link is not displayed");
    }
}

@Then("^User should see Set As Default option for the other addresses$")
public void user_should_see_Set_As_Default_option_for_the_other_addresses() throws Throwable {
    if(MyAccountPage.setasdefault_link(driver).isDisplayed())
    {
    	System.out.println("set as default link is displayed");
    }
    else{
    	System.out.println("set as default link is not displayed");
    }
    driver.quit();
}

@Given("^User is on the Address Book page under My Account$")
public void user_is_on_the_Address_Book_page_under_My_Account() throws Throwable {
	Educators_homepage.navigatetoMyAccount(driver);
	MyAccountPage.addressbook_link(driver).click();
	
}

@When("^User clicks on Edit for a Shipping Address$")
public void user_clicks_on_Edit_for_a_Shipping_Address() throws Throwable {
	MyAccountPage.editaddress_link(driver).click();
	}

@Then("^Edit Address form should spin up with pre-populated address information in the existing fields$")
public void edit_Address_form_should_spin_up_with_pre_populated_address_information_in_the_existing_fields() throws Throwable {
    if(MyAccountPage.saveaddress_button(driver).isDisplayed() & MyAccountPage.firstname(driver).getText()!=null)
    {
    	System.out.println("Edit Address form is spinned-up with pre-populated info");
    }
    else{
    System.out.println("sdfhfsdjhdsfhdsjhdjf");
    }
    }


@Then("^User should be able to make changes to the address$")
public void user_should_be_able_to_make_changes_to_the_address() throws Throwable {
    if(MyAccountPage.firstname(driver).isEnabled() & MyAccountPage.lastname(driver).isEnabled()){
    	System.out.println("Address fields are editable");
    	}
    else{
    	System.out.println("Address fields are not editable");
    }
 	}

@Then("^Save Address & Cancel options should be available to the User$")
public void save_Address_Cancel_options_should_be_available_to_the_User() throws Throwable {
    if(MyAccountPage.saveaddress_button(driver).isDisplayed() & MyAccountPage.canceladdress_button(driver).isDisplayed()){
    	System.out.println("Save Address & Cancel options are displayed");
    }
    else{
    	System.out.println("Save Address & Cancel options are not displayed");
    }
    driver.quit();
    }


@Given("^User is editing a Shipping address$")
public void user_is_editing_a_Shipping_address_under_Address_Book_in_My_Account() throws Throwable {
     MyAccountPage.editaddress_link(driver).click();
   }

@When("^User clicks on Save Address for the edited Shipping Address$")
public void user_clicks_on_Save_Address_for_the_edited_Shipping_Address() throws Throwable {
	MyAccountPage.addressnickname(driver).clear();
	MyAccountPage.addressnickname(driver).sendKeys(newaddress);
	Thread.sleep(2000);
	MyAccountPage.saveaddress_button(driver).click();
	}

@Then("^changes to the shipping address should be saved$")
public void changes_to_the_shipping_address_should_be_saved() throws Throwable {
   String address=MyAccountPage.addresstitle(driver).getAttribute("title");
   System.out.println(address);
   if(address.contains(newaddress)){
		System.out.println("new address is saved");
    }
	else{
	System.out.println("new address is not saved");
	
	}
	}

@Then("^these changes should be reflected under Address Book in My Account$")
public void these_changes_should_be_reflected_under_Address_Book_in_My_Account() throws Throwable {
	String address=MyAccountPage.addresstitle(driver).getAttribute("title");
	System.out.println(address);
	if(address.contains(newaddress)){
		System.out.println("changes are reflected");
    }
	else{
	System.out.println("changes are not reflected");
	
	}
	}

@Then("^these changes should be reflected under the Shipping Address in Checkout flow$")
public void these_changes_should_be_reflected_under_the_Shipping_Address_in_Checkout_flow() throws Throwable {
    Educators_homepage.proceedtocheckout(driver);
    String defaultaddress=Checkoutpage.defaultaddress(driver).getText();
    System.out.println(defaultaddress);
    String[] addressname=defaultaddress.split(" ");
    if(addressname[0].contains(newaddress))
    {
    	System.out.println("changes are reflected in checkout page");
    }
    else{
    	System.out.println("changes are not reflected in checkout page");
    }
    driver.quit();
	}

@When("^User clicks on Cancel for the edited Shipping Address$")
public void user_clicks_on_Cancel_for_the_edited_Shipping_Address() throws Throwable {
	MyAccountPage.firstname(driver).sendKeys(newfname);
	MyAccountPage.canceladdress_button(driver).click();
	}

@Then("^changes to the shipping address should be discarded$")
public void changes_to_the_shipping_address_should_be_discarded() throws Throwable {
	String oldfname=MyAccountPage.firstname(driver).getText();
	if(oldfname.contains(newfname)){
	System.out.println("changes are saved");
	}
	else
	{
	System.out.println("changes are discarded");
	}
	}

@Then("^Edit Address form should close$")
public void edit_Address_form_should_close() throws Throwable {
	if(MyAccountPage.statustext(driver).isDisplayed())
	{
    	System.out.println("edit address form is not closed");
    }
	else
	{
	System.out.println("edit address form is closed");
 	}
	}


@Then("^default screen for Address Book should be displayed$")
public void default_screen_for_Address_Book_should_be_displayed() throws Throwable {
	if(MyAccountPage.addaddress_link(driver).isDisplayed())
	{
    	System.out.println("default screen for Address Book is displayed");
    }
	else
	{
	System.out.println("default screen for Address Book is not displayed");
 	}
	}
@When("^User clicks on Delete for a Shipping Address$")
public void user_clicks_on_Delete_for_a_Shipping_Address() throws Throwable {
    MyAccountPage.deleteaddress_link(driver).click();
	Thread.sleep(2000);
    driver.findElement(By.id("confirm_delete_btn")).click();
    
	}

@Then("^particular address should get deleted$")
public void particular_address_should_get_deleted() throws Throwable {
    System.out.println("address got deleted");
}

@Given("^the order filter/search has been implemented$")
public void the_order_filter_search_has_been_implemented() throws Throwable {
    getAndResetDriver();
    driver.get("https://hmhco-v1.pre.techspa.com/classroom");
}

@Given("^the User is logged-in on HMHCO$")
public void the_User_is_logged_in_on_HMHCO() throws Throwable {
    Loginpage.signin(driver, "pershmh@gmail.com", "password1");
}

@Given("^the User has previous orders to search$")
public void the_User_has_previous_orders_to_search() throws Throwable {
    Educators_homepage.navigatetoMyAccount(driver);
    MyAccountPage.orderhistory_link(driver).click();
    String s=driver.findElement(By.xpath(".//*[@id='form_order_history']/ul/li[2]/div/section[1]/div/p")).getText();
    String[] s1=s.split(" ");
    if(Integer.parseInt(s1[4])>0){
    	System.out.println("previous orders available");	
    }
    else{
    	System.out.println("previous orders are not available");
    	driver.close();    	
    }
        
}

@When("^the user is entering the keyword to filter through the order history$")
public void the_user_is_entering_the_keyword_to_filter_through_the_order_history() throws Throwable {
    MyAccountPage.searchorder(driver).sendKeys("0008");
    
}

@Then("^the system should suggest the matching keywords product title, sub-title, author name from User order history$")
public void the_system_should_suggest_the_matching_keywords_product_title_sub_title_author_name_from_User_order_history() throws Throwable {
    if(Checkoutpage.isElementPresent(driver, "html/body/ul[2]/li[3]/a")){
    	System.out.println("suggestions are displayed");
    }
    else{
    	System.out.println("suggestions are not displayed");
    }
    driver.quit();
}

@Then("^the system should show to the User the matching search results from User order history$")
public void the_system_should_show_to_the_User_the_matching_search_results_from_User_order_history() throws Throwable {
	//Select s=new Select(MyAccountPage.searchorder(driver));
	if(Checkoutpage.isElementPresent(driver, "html/body/ul[2]/li[3]/a")){
    	System.out.println("suggestions are displayed");
    	
    	if(driver.findElement(By.xpath("html/body/ul[2]/li[3]/a")).getText().contains("0008")){    		
    		//xpath(//*[contains(./@id,'ui-id-')]/@id);    		
    		System.out.println("matching results are displayed");
    	}
    	else{
    		System.out.println("matching results are displayed");
    	}
    	    }
    else{
    	System.out.println("suggestions are not displayed");
    }
	driver.close();
}

@Then("^the orders will appear in reverse chronological order from the most recent to oldest$")
public void the_orders_will_appear_in_reverse_chronological_order_from_the_most_recent_to_oldest() throws Throwable {
}
 
@Given("^user enters the time period to filter through the order history$")
public void user_enters_the_time_period_to_filter_through_the_order_history() throws Throwable {
    
}

@Then("^the system should show to the User the orders which belong to the time period$")
public void the_system_should_show_to_the_User_the_orders_which_belong_to_the_time_period() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
}

@Then("^the keyword should be ignored$")
public void the_keyword_should_be_ignored() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
}

}
