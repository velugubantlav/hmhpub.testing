package com.hmhco.uitests;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@Cucumber.Options(
		format = {"pretty","html:target/html/"},tags={"@WP-2582-1"},
		features ="src/test/resources")

public class cukesrunner {
       	
}
