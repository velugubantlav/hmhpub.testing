Feature: My Account page

  @WP-119
  Scenario: My Account_Set As Default option
    Given User is a registered User of HMHCO
    And User is logged in on HMHCO
    And User has already added Shipping Addresses
    And User is on My Account page
    When User clicks on Address Book from the left menu
    Then the Address Book should be displayed to the User along with already added addresses
    And Add New Address option should be available to the user to add a new shipping address
    And User should see Edit & Delete option should be available for the user
    And User should see Remove Default option for the address which is set as default
    And User should see Set As Default option for the other addresses

  @WP-119
  Scenario: My Account_Save/Cancel Address option
    Given User is a registered User of HMHCO
    And User is logged in on HMHCO
    And User has already added Shipping Addresses
    And User is on the Address Book page under My Account
    When User clicks on Edit for a Shipping Address
    Then Edit Address form should spin up with pre-populated address information in the existing fields
    And User should be able to make changes to the address
    And Save Address & Cancel options should be available to the User

  @WP-119
  Scenario: My Account_Sync with check out page
    Given User is a registered User of HMHCO
    And User is logged in on HMHCO
    And User is on the Address Book page under My Account
    And User is editing a Shipping address
    When User clicks on Save Address for the edited Shipping Address
    Then changes to the shipping address should be saved
    And these changes should be reflected under Address Book in My Account
    And these changes should be reflected under the Shipping Address in Checkout flow

  @WP-119
  Scenario: My Account_Discard changes
    Given User is a registered User of HMHCO
    And User is logged in on HMHCO
    And User is on the Address Book page under My Account
    And User is editing a Shipping address
    When User clicks on Cancel for the edited Shipping Address
    Then changes to the shipping address should be discarded
    And Edit Address form should close
    And default screen for Address Book should be displayed

  @WP-119
  Scenario: My Account_Delete existing address
    Given User is a registered User of HMHCO
    And User is logged in on HMHCO
    And User has already added Shipping Addresses
    And User is on the Address Book page under My Account
    When User clicks on Delete for a Shipping Address
    Then particular address should get deleted

  @WP-1468
  Scenario: User enters keyword
    Given the order filter/search has been implemented
    And the User is logged-in on HMHCO
    And the User has previous orders to search
    When the user is entering the keyword to filter through the order history
    Then the system should suggest the matching keywords product title, sub-title, author name from User order history

  @WP-1468
  Scenario: chronological address display of results
    Given the order filter/search has been implemented
    And the User is logged-in on HMHCO
    And the User has previous orders to search
    When the user is entering the keyword to filter through the order history
    Then the system should show to the User the matching search results from User order history
    And the orders will appear in reverse chronological order from the most recent to oldest

  @WP-1468
  Scenario: time period based search
    Given the order filter/search has been implemented
    And the User is logged-in on HMHCO
    And the User has previous orders to search
    When the user is entering the keyword to filter through the order history
    And user enters the time period to filter through the order history
    Then the system should show to the User the orders which belong to the time period
    And the keyword should be ignored
    And the orders will appear in reverse chronological order from the most recent to oldest
