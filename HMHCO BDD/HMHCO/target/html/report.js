$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("com/hmhco/uitests/checkout.feature");
formatter.feature({
  "line": 1,
  "name": "Check Out page",
  "description": "",
  "id": "check-out-page",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 192,
  "name": "Choose to save the Custom (User entered) Address",
  "description": "",
  "id": "check-out-page;choose-to-save-the-custom-(user-entered)-address",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 191,
      "name": "@WP-2582-1"
    }
  ]
});
formatter.step({
  "line": 193,
  "name": "the Guest User is at Checkout on HMHCO",
  "keyword": "Given "
});
formatter.step({
  "line": 194,
  "name": "the User submitted a changed Shipping Address",
  "keyword": "And "
});
formatter.step({
  "line": 195,
  "name": "the System is showing the Suggested Address details to the User",
  "keyword": "And "
});
formatter.step({
  "line": 196,
  "name": "the User chooses to save the Custom Address",
  "keyword": "When "
});
formatter.step({
  "line": 197,
  "name": "the Custom Address should be saved as the Shipping Address for the order",
  "keyword": "Then "
});
formatter.match({
  "location": "Checkout.the_Guest_User_is_at_Checkout_on_HMHCO()"
});
formatter.result({
  "duration": 42749517391,
  "status": "passed"
});
formatter.match({
  "location": "Checkout.the_User_submitted_a_changed_Shipping_Address()"
});
formatter.result({
  "duration": 23410321112,
  "status": "passed"
});
formatter.match({
  "location": "Checkout.the_System_is_showing_the_Suggested_Address_details_to_the_User()"
});
formatter.result({
  "duration": 3061565630,
  "status": "passed"
});
formatter.match({
  "location": "Checkout.the_User_chooses_to_save_the_Custom_Address()"
});
formatter.result({
  "duration": 353442418,
  "status": "passed"
});
formatter.match({
  "location": "Checkout.the_Custom_Address_should_be_saved_as_the_Shipping_Address_for_the_order()"
});
formatter.result({
  "duration": 1305130880,
  "status": "passed"
});
});