Feature: My Account page

  @WP-119
  Scenario: Edit address in My Account page1
    Given User is a registered User of HMHCO
    And User is logged in on HMHCO
    And User has already added Shipping Addresses
    And User is on My Account page
    When User clicks on Address Book from the left menu
    Then the Address Book should be displayed to the User along with already added addresses
    And Add New Address option should be available to the user to add a new shipping address
    And User should see Edit & Delete option should be available for the user
    And User should see Remove Default option for the address which is set as default
    And User should see Set As Default option for the other addresses

  @WP-119
  Scenario: Edit address in My Account page2
    Given User is a registered User of HMHCO
    And User is logged in on HMHCO
    And User has already added Shipping Addresses
    And User is on the Address Book page under My Account
    When User clicks on Edit for a Shipping Address
    Then Edit Address form should spin up with pre-populated address information in the existing fields
    And User should be able to make changes to the address
    And Save Address & Cancel options should be available to the User

  @WP-119
  Scenario: Edit address in My Account page3
    Given User is a registered User of HMHCO
    And User is logged in on HMHCO
    And User is on the Address Book page under My Account
    And User is editing a Shipping address
    When User clicks on Save Address for the edited Shipping Address
    Then changes to the shipping address should be saved
    And these changes should be reflected under Address Book in My Account
    And these changes should be reflected under the Shipping Address in Checkout flow

  @WP-119
  Scenario: Edit address in My Account page4
    Given User is a registered User of HMHCO
    And User is logged in on HMHCO
    And User is on the Address Book page under My Account
    And User is editing a Shipping address
    When User clicks on Cancel for the edited Shipping Address
    Then changes to the shipping address should be discarded
    And Edit Address form should close
    And default screen for Address Book should be displayed

  @WP-119
  Scenario: Edit address in My Account page5
    Given User is a registered User of HMHCO
    And User is logged in on HMHCO
    And User has already added Shipping Addresses
    And User is on the Address Book page under My Account
    When User clicks on Delete for a Shipping Address
    Then particular address should get deleted
