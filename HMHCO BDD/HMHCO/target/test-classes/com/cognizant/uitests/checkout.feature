Feature: Check Out page
  
  
  @WP-219
  Scenario: Edit shipping address in checkout flow1
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User has added products to the cart
    And the User has already added Shipping Addresses
    When the User clicks on Proceed to Checkout
    Then the User should see the Shipping Accordion expanded
    And the User should see default shipping address selected for Shipping the order
    And the Shipping Address drop-down to select any previously added address
    And the Edit & Delete option should be available for the user
    And the Remove Default option should be available to the user
    And Add Address option should be available to the user to add a new shipping address

  @WP-219
  Scenario: Edit2
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User has already added Shipping Addresses
    And the User has added products to the cart
    And the user clicks on Proceed to checkout button
    When the User clicks on Edit for the selected Shipping Address
    Then the Shipping accordion should spin up the edit address form with pre-populated address information in the existing fields
    And the User should be able to make changes to the address
    And the Save Address & Cancel options should be available to the User

  @WP-219
  Scenario: Edit3
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User is editing a Shipping address under the Checkout flow
    When the User clicks on Save Address for the selected Shipping Address
    Then the changes to the shipping address should be saved
    And these changes should be reflected under the Address Book in My Account

  @WP-219
  Scenario: Edit4
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User is editing a Shipping address under the Checkout flow
    When the User clicks on Cancel for the selected Shipping Address
    Then the changes to the shipping address should be discarded
    And the Shipping accordion should spin back to the default state with the address selected as Shipping Address

  @WP-219
  Scenario: Edit5
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User has already added Shipping Addresses
    And the User has added products to the cart
    And the User is on the Checkout flow
    When the User clicks on Delete the selected Shipping Address
    Then the particular address should get deleted and the next address should automatically be selected or if another address is not available, the Shipping accordion should spin up the Add address form
    
  @WP-219
  Scenario: Edit6
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User has already added Shipping Addresses
    And the User has added products to the cart
    And the User is on the Checkout flow
    When the User chooses any other address apart from default address from the Shipping Address drop-down
    Then the Edit & Delete option should be available for the user
    And the Set as Default option should be available to the user
    And Add Address option should be available to the user to add a new shipping address
