Feature: Widget page

  @WP-2177
  Scenario: 
    Given the Categories are defined in the system
    And the widget has been implemented for a page
    When the User navigates to that page
    Then the widget should be loaded on the page
    And the Title entered should be displayed
    And Categories should be displayed
    And number of products under each Category will be displayed to the User beside the Category name
    And Categories should be ordered alphabetically from top to bottom

  @WP-2177
  Scenario: 
    Given that the User navigates to a page on which the widget has been implemented
    When the User clicks on a Category displayed in the widget
    Then the selected Category search page should load
    And all products under that category should be displayed to the User

  @WP-2177
  Scenario: 
    Given that the User navigates to a page on which the widget has been implemented
    When the the number of Categories are less than 4 or more than 4
    Then the categories should still be displayed using a 4-column structure
