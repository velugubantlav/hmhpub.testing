Feature: Check Out page

  @WP-219
  Scenario: Checkout_Shipping address_Field Validations
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User has added products to the cart
    And the User has already added Shipping Addresses
    When the User clicks on Proceed to Checkout
    Then the User should see the Shipping Accordion expanded
    And the User should see default shipping address selected for Shipping the order
    And the Shipping Address drop-down to select any previously added address
    And the Edit & Delete option should be available for the user
    And the Remove Default option should be available to the user
    And Add Address option should be available to the user to add a new shipping address

  @WP-219
  Scenario: Checkout_Edit & Save existing shipping address
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User has already added Shipping Addresses
    And the User has added products to the cart
    And the user clicks on Proceed to checkout button
    When the User clicks on Edit for the selected Shipping Address
    Then the Shipping accordion should spin up the edit address form with pre-populated address information in the existing fields
    And the User should be able to make changes to the address
    And the Save Address & Cancel options should be available to the User

  @WP-219
  Scenario: Checkout_Sync with My account page
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User is editing a Shipping address under the Checkout flow
    When the User clicks on Save Address for the selected Shipping Address
    Then the changes to the shipping address should be saved
    And these changes should be reflected under the Address Book in My Account

  @WP-219
  Scenario: Checkout_Edit & Unsave existing shipping address
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User is editing a Shipping address under the Checkout flow
    When the User clicks on Cancel for the selected Shipping Address
    Then the changes to the shipping address should be discarded
    And the Shipping accordion should spin back to the default state with the address selected as Shipping Address

  @WP-219
  Scenario: Checkout_Delete existing shipping address
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User has already added Shipping Addresses
    And the User has added products to the cart
    And the User is on the Checkout flow
    When the User clicks on Delete the selected Shipping Address
    Then the particular address should get deleted and the next address should automatically be selected or if another address is not available, the Shipping accordion should spin up the Add address form

  @WP-219
  Scenario: Checkout_Add new address
    Given the User is a registered User of HMHCO
    And the User is logged in on HMHCO
    And the User has already added Shipping Addresses
    And the User has added products to the cart
    And the User is on the Checkout flow
    When the User chooses any other address apart from default address from the Shipping Address drop-down
    Then the Edit & Delete option should be available for the user
    And the Set as Default option should be available to the user
    And Add Address option should be available to the user to add a new shipping address

  @WP-759
  Scenario: Valid Email ID Used
    Given the Guest User is at Checkout on HMHCO
    When the User provides a valid email address "valid@mail.com"
    And the User clicks on Checkout as a Guest
    Then the system should not give any email validation related error

  @WP-759
  Scenario: Invalid Email ID
    Given the Guest User is at Checkout on HMHCO
    When the User provides an invalid email address "invalid" or the User leaves the field empty "     "
    And the User clicks on Checkout as a Guest
    Then the system should give an error message "Please enter your email address in the format account@domain.com."

  @WP-759
  Scenario: Valid First Name
    Given the Guest User is at Checkout on HMHCO
    When the User provides a valid first name
    Then the system should not give any field validation related error

  @WP-759
  Scenario: No First Name
    Given the Guest User is at Checkout on HMHCO
    When the User leaves the firstname field empty
    Then the system should give an error message as "Please enter your first name."

  @WP-759
  Scenario: Invalid First Name
    Given the Guest User is at Checkout on HMHCO
    When the User provides an invalid first name
    Then the system should display a message "Please enter a valid first name."

  @WP-759
  Scenario: Valid Last Name
    Given the Guest User is at Checkout on HMHCO
    When the User provides a valid Last name
    Then the system should not give any last name field validation related error

  @WP-759
  Scenario: No Last Name
    Given the Guest User is at Checkout on HMHCO
    When the User leaves the lastname field empty
    Then the system should give an error message1 "Please enter your last name."

  @WP-759
  Scenario: Invalid Last Name
    Given the Guest User is at Checkout on HMHCO
    When the User provides an invalid Last name
    Then the system should give error message "Please enter a valid last name."

  @WP-759
  Scenario: Valid Phone Used
    Given the Guest User is at Checkout on HMHCO
    When the User provides a valid Phone number
    Then the system should not give any phone number field validation related error

  @WP-759 @WP-2474
  Scenario: Invalid Phone#
    Given the Guest User is at Checkout on HMHCO
    When the User provides an invalid Phone number
    Then the system should display error message "Please enter a valid phone number in the format xxx-xxx-xxxx."

  @WP-759
  Scenario: Valid Address Nickname
    Given the Registered User is at Checkout on HMHCO
    And the User is creating a new Shipping or Billing Address
    When the User provides a valid address nickname
    Then the system should not give any address field validation related error

  @WP-759
  Scenario: No Address Nickname
    Given the Registered User is at Checkout on HMHCO
    And the User is creating a new Shipping or Billing Address
    When the User leaves the field empty
    Then the system should display an error message as "Please enter your nickname."

  @WP-759
  Scenario: Valid Institution Name
    Given the Institutional User is at Checkout on HMHCO
    When the User provides a valid Institution name
    Then the system should not give any name field validation related error

  @WP-759
  Scenario: No Institution Name
    Given the Institutional User is at Checkout on HMHCO
    When the User leaves the institution field empty
    Then the system should give a message "Please enter your institution name."

  @WP-759
  Scenario: Invalid Institution Name
    Given the Institutional User is at Checkout on HMHCO
    When the User provides an invalid Institution name
    Then system should give an error message "Please enter a valid institution name."

  @WP-2584
  Scenario: Submit without making changes to the Selected Address
    Given the Guest User is at Checkout on HMHCO
    And the User has selected a suggested Intuitive Address for Billing Address
    When the User clicks on Review Order button without making any changes/ addition to the selected address
    Then the System should save the address for Billing the Order
    And the System should NOT send the Shipping Address to Address Verification service

  @WP-2582
  Scenario: Make Changes to the selected Intuitive Address
    Given the Guest User is at Checkout on HMHCO
    And the User has selected a suggested Intuitive Address
    When the User makes changes to the selected address
    And the User clicks on Continue to Payment & Billing button
    Then the System should send the Shipping Address to Address Verification service
    And the Suggested Address details should be displayed to the User (Verify Your Shipping Address page)
    And only one Suggested Address should be displayed to the User, along with the User entered Address
    And by default the Suggested Address should be appear as selected on the Verify Your Shipping Address page
    And the User should have the option of choosing to save the Suggested Address as is or choosing to save the User entered address or choose and edit the User entered address

  @WP-2582
  Scenario: Choose to save the Suggested Address
    Given the Guest User is at Checkout on HMHCO
    And the User submitted a changed Shipping Address
    And the System is showing the Suggested Address details to the User
    When the User chooses to use the Suggested Address
    Then the Suggested Address should be saved as the Shipping Address for the order

  @WP-2582-1
  Scenario: Choose to save the Custom (User entered) Address
    Given the Guest User is at Checkout on HMHCO
    And the User submitted a changed Shipping Address
    And the System is showing the Suggested Address details to the User
    When the User chooses to save the Custom Address
    Then the Custom Address should be saved as the Shipping Address for the order
