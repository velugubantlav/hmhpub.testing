Feature: Minicart page

  @WP-2416
  Scenario: Only Trade products in the Cart
    Given the User has added only Trade products in the Cart
    And the value of products ranges between $25 and $200
    When the User views the Mini-Cart
    Then the free shipping message should be displayed to the User in the Mini-Cart area

  @WP-2416
  Scenario: Only K12 products in the Cart
    Given the User has added only K12 products in the Cart
    And the value of products ranges between $25 and $200
    When the User views the Mini-Cart
    Then the free shipping message should be displayed to the User in the Mini-Cart area

  @WP-2416
  Scenario: Trade & K12 products in the Cart
    Given the Guest/ Registered Individual User has added Trade & K12 products in the Cart
    And the value of products ranges between $25 and $200
    When the User views the Mini-Cart
    Then the free shipping message should be displayed to the User in the Mini-Cart area

  @WP-2416
  Scenario: Physical & Digital products in the Cart
    Given the Guest/ Registered Individual User has added Physical products in the Cart worth $15
    And the User has added Digital products in the Cart worth $10,total value of the Cart is $25
    When the User views the Mini-Cart
    Then the free shipping message should NOT be displayed to the User in the Mini-Cart area

  @WP-2416
  Scenario: Institutional User having products in the Cart
    Given an Institutional User has added products in the Cart
    And the value of products ranges between $25 & $200
    When the User views the Mini-Cart
    Then the free shipping message should NOT be displayed to the User in the Mini-Cart area
