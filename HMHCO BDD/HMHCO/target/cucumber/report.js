$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("com/cognizant/uitests/loginpage.feature");
formatter.feature({
  "line": 1,
  "name": "Login",
  "description": "",
  "id": "login",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "User Login",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I am an already existing user",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 8,
  "name": "Login to HMHCO application",
  "description": "",
  "id": "login;login-to-hmhco-application",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "login page is loaded",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "the username is \"ksk285041@gmail.com\"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "the password \"password1\"",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "search button is clicked",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user should be logged in",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});