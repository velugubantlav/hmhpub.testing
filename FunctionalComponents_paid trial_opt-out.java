package businesscomponents;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.apache.xpath.operations.Div;
import org.openqa.selenium.*;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.internal.seleniumemulation.WaitForPageToLoad;
import org.openqa.selenium.lift.find.DivFinder;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.css.sac.SelectorList;

import com.cognizant.framework.Status;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 *  Functional Components class  
 * @author Cognizant 
 */

public class FunctionalComponents extends ReusableLibrary
{
	Actions actions = new Actions(driver);
	/**
	 * Constructor to initialize the component library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	 protected static Set setOfOldHandles = null;
		protected static Set setOfNewHandles = null;
		static WebElement  webelement;
		
	public FunctionalComponents(ScriptHelper scriptHelper)
		{
		super(scriptHelper);
		}

	
	// Saves Old Handles
	public void saveOldHandles(WebDriver driver)
			{
				if (setOfOldHandles != null)
					{
						setOfOldHandles.clear();
					}
				setOfOldHandles = driver.getWindowHandles(); // Save all the browser window ID's
				//report.updateTestLog("hai", ""+setOfOldHandles.size(), Status.DONE);
			}	
			
	// Saves New Handles
	public void saveNewHandles(WebDriver driver) 
	  {
			if (setOfNewHandles != null)
			{
				setOfNewHandles.clear();
			}
			setOfNewHandles = driver.getWindowHandles();  // Save all the browser window ID's
			
		}

	// Focus on new view		
	public void focusOnNewView(WebDriver driver)
	  {
			try 
				{
					Thread.sleep(3000);
				} 
			catch (InterruptedException e) 
				{
					// To do Auto-generated catch block
					e.printStackTrace();
				}
			if (setOfNewHandles != null) 
				{
					setOfNewHandles.removeAll(setOfOldHandles); 
					// this method removeAll() take one set and puts it in another set and if there are same
					// positions it will erase them and leaves only that are not equals
				} 
			else 
				{
 
					System.out.println("setOfNewHandles is null. Can't compare old and new handles. New handle may have not enough time to load and save. Maybe you should add some time to load new window by adding Thread.Sleep(3000); - wait for 3 second ");
				}

			if (!setOfNewHandles.isEmpty()) 
			{
				String newWindowHandle = (String) setOfNewHandles.iterator().next();
				//Finding current window
				driver.switchTo().window(newWindowHandle);		   
				//Focusing on current window.		
				try 
				{
					Thread.sleep(3000);
				} 
				catch (InterruptedException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				driver.manage().window().maximize();
				
			}
		}
		
	// Waits for till the element is displayed
	public void waitForElementDisplay(String elementId) throws InterruptedException 
	{
		for(int i=0; i<600; i++)
		{
		try
		{
		if(driver.findElement(By.id(elementId)).isDisplayed())
		break;
		else
			Thread.sleep(100);
		}
		catch (Exception e) 
		{
			Thread.sleep(100);
		}
		}
	}
	
	// Waits till the page is loaded
	public void waitForLoad(WebDriver driver) 
	{
	    ExpectedCondition<Boolean> pageLoadCondition = new
	        ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver driver) {
	                return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	            }
	        };
	    WebDriverWait wait = new WebDriverWait(driver, 60);
	    wait.until(pageLoadCondition);
	}
	
	// SelectOptionFromListByVisibleText	
	public void SelectOptionFromListByVisibleText(String ElementIdentifier, String OptionText)
	{
		WebElement Dropdown =  driver.findElement(By.id(ElementIdentifier));
		Select objSelect = new Select(Dropdown);
		objSelect.selectByVisibleText(OptionText);
	}
	
	// Clean up
	public void cleanUp() {
	        driver.quit();
	    }
	 
	// Identification of Text
	public WebElement ElementPresent(String Identifier)
	 {
//		 String Validation = null; 
		 try
		 {
			 WebElement Element = driver.findElement(By.xpath(Identifier));
			 return Element;
		 }
		 catch(Exception e)
		 {
			return null; 
		 }
	 }

	// Condition to verify the element is present or not
	public boolean isElementPresent(By by){
	try {
		driver.findElement(by); 
		//If element exists return true
		return true;
	} catch (Exception e) {
		//If element not exists return False.
		return false;			
		
	}
	
}
	// Using selenium.DeleteAllVisibleCookies in Webdriver

	public void DeleteAllVisibleCookies()
	{
		driver.manage().deleteAllCookies();
	}
	
	// Select value from unordered list
	public void selectValueFromUnorderedList(WebElement unorderedList, final String value) {
	    List<WebElement> options = unorderedList.findElements(By.tagName("li"));

	    for (WebElement option : options) {
	        if (value.equals(option.getText())) {
	            option.click();
	            break;
	        }
	    }
	}
	

		
//======================================--------------------	Subscriptions Paid trial --------------------=================================
		
// Sign Up button

	public void signUp()
	{
	       driver.manage().window().maximize();
	       driver.findElement(By.xpath("html/body/header/div/p/a[2]/span")).click();
	       waitForLoad(driver);
	       try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	     //
	}

// Subscriptions - Try it free button
	

// Subscriptions - Monthly button
public void monthly(){
	
	if(isElementPresent(By.xpath("html/body/form/section[2]/div[1]/div[1]/div/div/a/span"))){
		String button = driver.findElement(By.xpath("html/body/form/section[2]/div[1]/div[1]/div/h3/span")).getText();
	    driver.findElement(By.xpath("html/body/form/section[2]/div[1]/div[1]/div/div/a/span")).click();
	    waitForLoad(driver);
	       try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    report.updateTestLog("GoMath", "User clicked on - " +button, Status.PASS);
	}
	else{
		report.updateTestLog("GoMath", "Monthly button is not displayed", Status.FAIL);
	}
}

// Subscriptions - Six Months button
public void sixMonths(){
	
	if(isElementPresent(By.xpath("html/body/form/section[2]/div[1]/div[2]/div[2]/div/a/span"))){
		String button = driver.findElement(By.xpath("html/body/form/section[2]/div[1]/div[2]/div[2]/h3/span")).getText();
	    driver.findElement(By.xpath("html/body/form/section[2]/div[1]/div[2]/div[2]/div/a/span")).click();
	    waitForLoad(driver);
	       try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    report.updateTestLog("GoMath", "User clicked on - " +button, Status.PASS);
	}
	else{
		report.updateTestLog("GoMath", "Six Months button is not displayed", Status.FAIL);
	}
}

// Subscriptions -  One Year button
public void oneYear(){
	
	if(isElementPresent(By.xpath("html/body/form/section[2]/div[1]/div[3]/div[2]/div/a/span"))){
		String button = driver.findElement(By.xpath("html/body/form/section[2]/div[1]/div[3]/div[2]/h3/span")).getText();
	    driver.findElement(By.xpath("html/body/form/section[2]/div[1]/div[3]/div[2]/div/a/span")).click();
	    waitForLoad(driver);
	       try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    
	    report.updateTestLog("GoMath", "User clicked on - " +button, Status.PASS);
	}
	else{
		report.updateTestLog("GoMath", "One Year button is not displayed", Status.FAIL);
	}
}

// Subscriptions - Registration
public void registration() throws Exception
{
       
    String firstname = dataTable.getData("General_Data", "FirstName");         
    driver.findElement(By.id("Content_txtFirstName")).sendKeys(firstname);
    
    String lastname = dataTable.getData("General_Data", "LastName");           
    driver.findElement(By.id("Content_txtLastName")).sendKeys(lastname);
    
    String email = dataTable.getData("General_Data", "Email");           
    driver.findElement(By.id("Content_txtEmail")).sendKeys(email);
    
    String year = dataTable.getData("General_Data", "Year");             
    driver.findElement(By.id("Content_txtBorn")).sendKeys(year);
    
    driver.findElement(By.id("Content_chkAgree")).click();
    
    driver.findElement(By.id("register")).click();
    waitForLoad(driver);
    try {
              Thread.sleep(5000);
       } catch (InterruptedException e) {
              e.printStackTrace();
       }
   if (isElementPresent(By.xpath("//*[@id='frmAgeValidation']/section/div/fieldset/div/p[1]/span")))
   {
         String ageValiadationText=driver.findElement(By.xpath("//*[@id='frmAgeValidation']/section/div/fieldset/div/p[1]/span")).getText();
         report.updateTestLog("GMA","Age validation message - " +ageValiadationText,Status.PASS);
   }
   else{
 
  if (isElementPresent(By.id("Content_txtConfirmEmailAddress")))
                 {           
                  driver.findElement(By.id("Content_txtConfirmEmailAddress")).sendKeys(email);
                  String password = dataTable.getData("General_Data", "Password");             
                  driver.findElement(By.id("Content_txtPassword")).sendKeys(password);
                  driver.findElement(By.id("Content_txtConfirmPassword")).sendKeys(password);
                  driver.findElement(By.id("submit")).click();
                  waitForLoad(driver);

              	try {
              		Thread.sleep(6000);
              	} catch (InterruptedException e) {
              		e.printStackTrace();
              	}	
//              	String id = "welcome-create-profile";
//            	waitForElementDisplay(id);
                report.updateTestLog("GMA Registration","User is successfully registered in GMA",Status.PASS);
       
       }
   }
    }


// Subscriptions - My Account page
public void myAccount() throws Exception
{


	String pwd = dataTable.getData("General_Data", "Password");		
	String id = "welcome-create-profile";
	waitForElementDisplay(id);
	if(isElementPresent(By.id("welcome-create-profile"))){
		
		String welcome = driver.findElement(By.xpath("//*[@id='welcome-create-profile']/h1")).getText();
		String child = dataTable.getData("RegisterUser_Data", "Child");		
		driver.findElement(By.id("enter-child-name")).sendKeys(child);
		 //select grade
		driver.findElement(By.id("gradeSelectBoxItText")).click();
		driver.findElement(By.id("gradeSelectBoxItText")).sendKeys("Grade 1");
	//	SelectOptionFromListByVisibleText("gradeSelectBoxItText","Grade 1");
		
		
		
		WebElement selectorElement = driver.findElement(By.xpath("//*[@id='gradeSelectBoxItText']"));

		selectValueFromUnorderedList(selectorElement,"Grade 2");
			
		
		
        
		driver.findElement(By.id("parent-begin-lets-go")).click();
		waitForLoad(driver);		
		report.updateTestLog("GoMath", "Welcome note - " +welcome, Status.PASS);
		String id1 = "ride-rocket";
		waitForElementDisplay(id1);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if(isElementPresent(By.xpath("//*[@id='ride-rocket']/button")))
		{
			driver.findElement(By.xpath("//*[@id='ride-rocket']/button")).click();
			waitForLoad(driver);
			try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String id2 = "start-trigger";
			waitForElementDisplay(id2);
			try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			driver.findElement(By.xpath("//*[@id='grade-menu']/button")).click();
			waitForLoad(driver);
			driver.findElement(By.xpath("//*[@id='user-menu']/button")).click();
			waitForLoad(driver);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String id3 = "pum-parent-dash";
			waitForElementDisplay(id3);
			driver.findElement(By.xpath("//*[@id='pum-parent-dash']/div/h1")).click();
			waitForLoad(driver);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			driver.findElement(By.xpath("//*[@id='parent-controls']/div[3]/button")).click();
			waitForLoad(driver);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(isElementPresent(By.xpath("//*[@id='frmSignIn']/section/div/fieldset/div/p[1]")))
			{
				String note = driver.findElement(By.xpath("//*[@id='frmSignIn']/section/div/fieldset/div/p[1]")).getText();
				report.updateTestLog("GoMath", "My Account password prompt message - " +note, Status.PASS);
				driver.findElement(By.id("Content_txtPassword")).sendKeys(pwd);
				driver.findElement(By.id("Content_btnlogin")).click();
				waitForLoad(driver);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
//				if(isElementPresent(By.xpath("//*[@id='frmMyAccount']/section[1]/div/div/h2")))
//				{
					String des = driver.findElement(By.xpath("//*[@id='Content_gvCancelSubscription']/tbody/tr[2]/td[2]")).getText();
					report.updateTestLog("GoMath", "Subscription description - " +des, Status.PASS);
					String order = driver.findElement(By.xpath("//*[@id='Content_gvCancelSubscription']/tbody/tr[2]/td[5]")).getText();
					report.updateTestLog("GoMath", "Subscription Order number - " +order, Status.PASS);
//				}
			}
			
		}
	}
	else{
		 report.updateTestLog("GoMath", "Welcome not displayed", Status.FAIL);
	}
	}
	
public void checkOut() throws InterruptedException
{
       String firstname=dataTable.getData("General_Data","FirstName");
       String lastname= dataTable.getData("General_Data","LastName");    
       String address=dataTable.getData("General_Data","Address");
       String city=dataTable.getData("General_Data","City");
       String cardnum=dataTable.getData("General_Data","CardNumber");
       String zipcode=dataTable.getData("General_Data","PostalCode");
       String cvv=dataTable.getData("General_Data","CVV");
       
       String substype=driver.findElement(By.xpath("html/body/form/section/div/table/tbody/tr/td[3]/p/span[1]")).getText();
       if(substype.equals("$9.99 per month, recurring") || substype.equals("$49.99 per 6 months, recurring") || substype.equals("$79.99 per year, recurring"))
       {
             
             //===Billing Address details===
             driver.findElement(By.id("Content_tbFirstName")).sendKeys(firstname);
             driver.findElement(By.id("Content_tbLastName")).sendKeys(lastname);
             driver.findElement(By.id("Content_tbAddress1")).sendKeys(address);
             driver.findElement(By.id("Content_tbCity")).sendKeys(city);
             //select state
             WebElement elem=driver.findElement(By.id("sbState"));
             Select statelist=new Select(elem);
             statelist.selectByValue("MA");  //MA for Massachusetts
             driver.findElement(By.id("Content_tbZip")).sendKeys(zipcode);
             //===payment info====
             //select card
             elem=driver.findElement(By.id("sbCCType"));
             Select cctype=new Select(elem);
             cctype.selectByValue("4000");   //4000 for VISA
             driver.findElement(By.id("Content_tbNameOnCard")).sendKeys(firstname+lastname);
             driver.findElement(By.id("Content_tbCCNumber")).sendKeys(cardnum);
             driver.findElement(By.id("Content_tbCVVNumber")).sendKeys(cvv);
             //select month
             elem=driver.findElement(By.id("sbMonth"));
             Select month=new Select(elem);
             month.selectByValue("4");
             //select year
             elem=driver.findElement(By.id("sbYear"));
             Select year=new Select(elem);
             year.selectByValue("2019");
             driver.findElement(By.id("Content_cbAgree")).click();
             driver.findElement(By.id("Content_cbAuthorize")).click();
             driver.findElement(By.id("Content_btnSubscribe")).click();
             waitForLoad(driver);
  	       	 try {
  				Thread.sleep(6000);
  			 } catch (InterruptedException e) {
  				e.printStackTrace();
  			 }
  	       String id1 = "Content_ConfirmOrderShow";
  	 	waitForElementDisplay(id1);
  	       if(driver.findElement(By.xpath("//*[@id='Content_ConfirmOrderShow']")).isDisplayed())	
           {
          	 String suba= driver.findElement(By.id("Content_lblPrice")).getText();
          	 report.updateTestLog("GMA", "Recurring amount -"+suba, Status.PASS);
          	 String subb= driver.findElement(By.id("Content_lblTaxAmount")).getText();
          	 report.updateTestLog("GMA", "Tax amount -"+subb, Status.PASS);
          	 String subc= driver.findElement(By.id("Content_lblTotalAmount")).getText();
          	 report.updateTestLog("GMA", "Total amount -"+subc, Status.PASS);
  	      
  	      
//  	        System.out.println(driver.getCurrentUrl());
//  	        JavascriptExecutor js = (JavascriptExecutor) driver;
//	        js.executeScript("javascript:window.scrollBy(0,600)");
//  	        
//  	        System.out.println("scrolled");
  	        
               	 driver.findElement(By.id("Content_ConfirmOrderShow")).click();
          	 waitForLoad(driver);
    	       	 try {
    				Thread.sleep(4000);
    			 } catch (InterruptedException e) {
    				e.printStackTrace();
    			 }
    	       	 String mesa= driver.findElement(By.xpath("//*[@id='form1']/section/div/h2/span")).getText();
    	       	 String mesb = driver.findElement(By.xpath("//*[@id='form1']/section/div/h3")).getText();
    	       	 String mesc= driver.findElement(By.xpath("//*[@id='form1']/section/div/h4")).getText();
    	       	 report.updateTestLog("GMA", "Message - "+mesa +mesb +mesc, Status.PASS);
    	       	 driver.findElement(By.id("Content_LetsGoButton")).click();
    	       	 
    	       String id = "welcome-create-profile";
    	       waitForElementDisplay(id);
           }
  	       else {
          	 String error = driver.findElement(By.id("Content_lblErrormsg")).getText();
          	 report.updateTestLog("GMA", "Error message - "+error, Status.FAIL);
           }
       }
}

public void userLogin() throws Exception
{
       driver.manage().window().maximize();
       driver.findElement(By.xpath("html/body/header/div/p/a[1]/span")).click();
       try {
              Thread.sleep(2000);
       } catch (InterruptedException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
       }
       String useremail = dataTable.getData("General_Data","Username");
       driver.findElement(By.id("Content_txtEmailAddress")).sendKeys(useremail);
       String passwrd = dataTable.getData("General_Data", "Password");
       driver.findElement(By.id("Content_txtPassword")).sendKeys(passwrd);
       driver.findElement(By.name("login")).click();
       Thread.sleep(3000);
//       String id = "gma-logo-126w";
//		waitForElementDisplay(id);
      System.out.println(driver.getCurrentUrl());
    String institutionalURL = "http://v1.stg.subscriptions.hmhco.com/gomath/RegisterInstitutional";
    String consumerURL = "http://v1.stg.subscriptions.hmhco.com/GoMath/IncompleteOrder";
//    String gmaURL ="http://int.gomathacademy.hmhco.com/gma/ltiprovider/index.html#/parents/nav";
    if(institutionalURL.equals(driver.getCurrentUrl()))
    {
              String message1 = driver.findElement(By.xpath("html/body/form/section/div/fieldset/div/p[1]/span")).getText();
              report.updateTestLog("GMA", "Institutional user successful login message - "+message1, Status.PASS);
              String email = dataTable.getData("General_Data","Email");
              driver.findElement(By.id("Content_txtEmailAddress")).sendKeys(email);
              //String cnfrmemail = dataTable.getData("General_Data", "Emailconfirm");
              driver.findElement(By.id("Content_txtConfirmEmailAddress")).sendKeys(email);
              String passwrd1 = dataTable.getData("General_Data", "Password");
              driver.findElement(By.id("Content_txtPassword")).sendKeys(passwrd1);
              String passwrd2 = dataTable.getData("General_Data", "Password");
              driver.findElement(By.id("Content_txtConfirmPassword")).sendKeys(passwrd2);
              driver.findElement(By.id("submit")).click();
              try {
                     Thread.sleep(2000);
              } catch (InterruptedException e) {
                     // TODO Auto-generated catch block
                     e.printStackTrace();
              }
              if(isElementPresent(By.id("Content_StartSubscriptionButton")))
              {
                          report.updateTestLog("GMA", "Institutional user is able to register", Status.PASS);
                         
              }
       
       }
    else if(consumerURL.equals(driver.getCurrentUrl()))
    {
        String message2 = driver.findElement(By.xpath("html/body/form/section/div/fieldset/div/p/span")).getText();
           report.updateTestLog("GMA","Consumer login message - "+message2, Status.PASS);
           String id1 = "Content_StartSubscriptionButton";
           waitForElementDisplay(id1);
           if(isElementPresent(By.id("Content_StartSubscriptionButton")))
              {
                     
              report.updateTestLog("GMA", "Consumer user is able to login into GMA", Status.PASS);
              driver.findElement(By.id("Content_StartSubscriptionButton")).click();
              waitForLoad(driver);
              Thread.sleep(2000); 
   	       	 
       }
    
    else
    {
    WebElement argwebelement = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button")));
    report.updateTestLog("GMA", "Subscribed user logged into GMA", Status.PASS);
    driver.findElement(By.cssSelector("button")).click();
    waitForLoad(driver);
    Thread.sleep(2000);
    }
    }
    
}

// Subscriptions - Consumer Registarion

public void regCosumer() throws Exception
{
	
		String id = "Content_lblRegisterIntro";
		waitForElementDisplay(id);
		driver.findElement(By.id("Content_chkAgree")).click();
	    
	    driver.findElement(By.id("register")).click();
	    waitForLoad(driver);
	    try {
	              Thread.sleep(3000);
	       } catch (InterruptedException e) {
	              e.printStackTrace();
	       }
	
}

// Subscriptions - Logout

public void logoutSub()
{
    if(isElementPresent(By.id("btnLogout")))   
    {
	driver.findElement(By.id("btnLogout")).click();
    report.updateTestLog("GMA Logout","The user is signed out successfully",Status.PASS);
    }
    else{
    	report.updateTestLog("GMA","User is unable to logout from GMA application",Status.FAIL);
    }
}
 
public void forgotPwd(){
	
	driver.manage().window().maximize();
    driver.findElement(By.xpath("html/body/header/div/p/a[1]/span")).click();
    try {
           Thread.sleep(2000);
    } catch (InterruptedException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
    }
    String useremail = dataTable.getData("General_Data","Username");
    driver.findElement(By.id("Content_txtEmailAddress")).sendKeys(useremail);
    String passwrd = dataTable.getData("General_Data", "Password");
    driver.findElement(By.id("Content_txtPassword")).sendKeys(passwrd);
    driver.findElement(By.name("login")).click();
    waitForLoad(driver);
	    try {
	        Thread.sleep(2000);
	 } catch (InterruptedException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	 }
	//String errormsg1=driver.findElement(By.xpath("html/body/form/section/div/fieldset/div/p[3]/span")).getText();
	    //errormsg1.equals("Please enter a valid email address in the format account@domain.com.") || 
	String errormsg2=driver.findElement(By.xpath("html/body/form/section/div/fieldset/div/p[4]/span[2]")).getText();
    if(errormsg2.equals("We�re sorry�the password you entered is not valid. Please try again."))
    {
	driver.findElement(By.xpath("html/body/form/section/div/fieldset/div/p[5]/a")).click();
	waitForLoad(driver);
    driver.findElement(By.id("body_0_txtFpEmail")).sendKeys(useremail);
	driver.findElement(By.id("body_0_lbtFpContinue")).click();
	waitForLoad(driver);
	 try {
	        Thread.sleep(2000);
	 } catch (InterruptedException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	 }
	String mes = driver.findElement(By.xpath("html/body/form/div[2]/div[2]/div[1]/div/div[1]/span")).getText();
	report.updateTestLog("GoMath", "Forgot password reset Message - " + mes, Status.PASS);
    }
}

public void userAccount() throws Exception{
	driver.manage().window().maximize();
    driver.findElement(By.xpath("html/body/header/div/p/a[1]/span")).click();
    try {
           Thread.sleep(2000);
    } catch (InterruptedException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
    }
    String useremail = dataTable.getData("General_Data","Username");
    driver.findElement(By.id("Content_txtEmailAddress")).sendKeys(useremail);
    String passwrd = dataTable.getData("General_Data", "Password");
    driver.findElement(By.id("Content_txtPassword")).sendKeys(passwrd);
    driver.findElement(By.name("login")).click();
    waitForLoad(driver);
    String id = "pum-parent-dash";
    waitForElementDisplay(id);
    driver.findElement(By.xpath("//*[@id='pum-parent-dash']/div/h1")).click();
	waitForLoad(driver);
	String id1 = "parent-controls";
    waitForElementDisplay(id1);
	driver.findElement(By.xpath("//*[@id='parent-controls']/div[3]/button")).click();
	waitForLoad(driver);
	String id2 = "frmSignIn";
    waitForElementDisplay(id2);
	if(isElementPresent(By.xpath("//*[@id='frmSignIn']/section/div/fieldset/div/p[1]"))){
		String note = driver.findElement(By.xpath("//*[@id='frmSignIn']/section/div/fieldset/div/p[1]")).getText();
		report.updateTestLog("GoMath", "My Account password prompt message - " +note, Status.PASS);
		driver.findElement(By.id("Content_txtPassword")).sendKeys(passwrd);
		driver.findElement(By.id("Content_btnlogin")).click();
		waitForLoad(driver);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
//		if(isElementPresent(By.xpath("//*[@id='frmMyAccount']/section[1]/div/div/h2")))
//		{
			String des = driver.findElement(By.xpath("//*[@id='Content_gvCancelSubscription']/tbody/tr[2]/td[2]")).getText();
			report.updateTestLog("GoMath", "Subscription description - " +des, Status.PASS);
			String order = driver.findElement(By.xpath("//*[@id='Content_gvCancelSubscription']/tbody/tr[2]/td[5]")).getText();
			report.updateTestLog("GoMath", "Subscription Order number - " +order, Status.PASS);
		}
}


	
	
	
	public void reg_monthly_visa() throws Exception
	{
	    driver.manage().window().maximize();
	    driver.findElement(By.xpath(".//*[@id='tryOrBuy']/span")).click();
	    try {
	           Thread.sleep(2000);
	    } 
	    catch (InterruptedException e) {
	           // TODO Auto-generated catch block
	           e.printStackTrace();
	    }
	    driver.findElement(By.xpath("html/body/form/section[2]/div[1]/div[1]/div/div/a/span")).click();
	    String firstname = dataTable.getData("General_Data", "FirstName");         
	    driver.findElement(By.id("Content_txtFirstName")).sendKeys(firstname);
	    String lastname = dataTable.getData("General_Data", "LastName");           
	    driver.findElement(By.id("Content_txtLastName")).sendKeys(lastname);
	    String email = dataTable.getData("General_Data", "Email");           
	    driver.findElement(By.id("Content_txtEmail")).sendKeys(email);
	    String year = dataTable.getData("General_Data", "Year");             
	    driver.findElement(By.id("Content_txtBorn")).sendKeys(year);
	    driver.findElement(By.id("Content_chkAgree")).click();
	    driver.findElement(By.id("register")).click();
	    waitForLoad(driver);
	    try {
	              Thread.sleep(5000);
	       } catch (InterruptedException e) {
	              e.printStackTrace();
	       }
	    driver.findElement(By.id("Content_txtConfirmEmailAddress")).sendKeys(email);
	    String passwrd = dataTable.getData("General_Data", "Password");
	    driver.findElement(By.id("Content_txtPassword")).sendKeys(passwrd);
	    driver.findElement(By.id("Content_txtConfirmPassword")).sendKeys(passwrd);
	   driver.findElement(By.id("submit")).click();
	    waitForLoad(driver);
	        try {
	                  Thread.sleep(5000);
	           } catch (InterruptedException e) {
	                  e.printStackTrace();
	           }
	     driver.findElement(By.id("Content_tbFirstName")).sendKeys(firstname);
	     driver.findElement(By.id("Content_tbLastName")).sendKeys(lastname);
	     String addr = dataTable.getData("General_Data","Address");
	     driver.findElement(By.id("Content_tbAddress1")).sendKeys(addr);
	     String city = dataTable.getData("General_Data", "City");  
	     driver.findElement(By.id("Content_tbCity")).sendKeys(city);
	                //select state
	     WebElement elem=driver.findElement(By.id("sbState"));
	        Select statelist=new Select(elem);
	        statelist.selectByValue("NY");  //NY for New York
	        String zip = dataTable.getData("General_Data", "PostalCode");
	     driver.findElement(By.id("Content_tbZip")).sendKeys(zip);
	        //select card type
	     WebElement elem1=driver.findElement(By.id("sbCCType"));
	     Select cardlist=new Select(elem1);
	     cardlist.selectByValue("4000");
	     //String namecard = dataTable.getData("General_Data", "NameonCard");
	     driver.findElement(By.id("Content_tbNameOnCard")).sendKeys(firstname+" "+lastname);   // name on card
	     String cardno = dataTable.getData("General_Data", "CardNumber");
	     driver.findElement(By.id("Content_tbCCNumber")).sendKeys(cardno);
	     String cvvno = dataTable.getData("General_Data", "CVV");
	     driver.findElement(By.id("Content_tbCVVNumber")).sendKeys(cvvno);
	        //select card expiry date
	     WebElement elem2=driver.findElement(By.id("sbMonth"));
	        Select datelist=new Select(elem2);
	        datelist.selectByValue("6"); 
	        //select card expiry year
	     WebElement elem3=driver.findElement(By.id("sbYear"));
	        Select yearlist=new Select(elem3);
	        yearlist.selectByValue("2022"); 
	     driver.findElement(By.id("Content_cbAgree")).click();
	     driver.findElement(By.id("Content_cbAuthorize")).click();
	     driver.findElement(By.id("Content_btnSubscribe")).click();
	     waitForLoad(driver);
	        try {
	                  Thread.sleep(5000);
	           } catch (InterruptedException e) {
	                  e.printStackTrace();
	           }
	      String id1 = "Content_ConfirmOrderShow";
	                  waitForElementDisplay(id1);
	                  if(driver.findElement(By.xpath("//*[@id='Content_ConfirmOrderShow']")).isDisplayed())        
	           {
	                 String suba= driver.findElement(By.id("Content_lblPrice")).getText();
	                 report.updateTestLog("GMA", "Recurring amount -"+suba, Status.PASS);
	                 String subb= driver.findElement(By.id("Content_lblTaxAmount")).getText();
	                 report.updateTestLog("GMA", "Tax amount -"+subb, Status.PASS);
	                 String subc= driver.findElement(By.id("Content_lblTotalAmount")).getText();
	                 report.updateTestLog("GMA", "Total amount -"+subc, Status.PASS);
	                         driver.findElement(By.id("Content_ConfirmOrderShow")).click();
	                 waitForLoad(driver);
	                                 try {
	                                        Thread.sleep(4000);
	                                     } 
	                                 catch (InterruptedException e)
	                                 {
	                                    e.printStackTrace();
	                                  }
	               String mesa= driver.findElement(By.xpath("//*[@id='form1']/section/div/h2/span")).getText();
	               String mesb = driver.findElement(By.xpath("//*[@id='form1']/section/div/h3")).getText();
	               String mesc= driver.findElement(By.xpath("//*[@id='form1']/section/div/h4")).getText();
	               report.updateTestLog("GMA", "Message - "+mesa +mesb +mesc, Status.PASS);
	               driver.findElement(By.id("Content_LetsGoButton")).click();
	                                 String id = "welcome-create-profile";
	                         waitForElementDisplay(id);
	           }
	                  else 
	                       {
	                 String error = driver.findElement(By.id("Content_lblErrormsg")).getText();
	                 report.updateTestLog("GMA", "Error message - "+error, Status.FAIL);
	                       }
	}

	public void reg_6months_Master() throws Exception
	{
	                driver.manage().window().maximize();
	    driver.findElement(By.xpath(".//*[@id='tryOrBuy']/span")).click();
	    try {
	           Thread.sleep(2000);
	    } catch (InterruptedException e) {
	           // TODO Auto-generated catch block
	           e.printStackTrace();
	    }
	    driver.findElement(By.xpath("html/body/form/section[2]/div[1]/div[2]/div[2]/div/a/span")).click();
	    String firstname = dataTable.getData("General_Data", "FirstName");         
	    driver.findElement(By.id("Content_txtFirstName")).sendKeys(firstname);
	    String lastname = dataTable.getData("General_Data", "LastName");           
	    driver.findElement(By.id("Content_txtLastName")).sendKeys(lastname);
	    String email = dataTable.getData("General_Data", "Email");           
	    driver.findElement(By.id("Content_txtEmail")).sendKeys(email);
	    String year = dataTable.getData("General_Data", "Year");             
	    driver.findElement(By.id("Content_txtBorn")).sendKeys(year);
	    driver.findElement(By.id("Content_chkAgree")).click();
	    driver.findElement(By.id("register")).click();
	    waitForLoad(driver);
	    try {
	              Thread.sleep(5000);
	       } catch (InterruptedException e) {
	              e.printStackTrace();
	       }
	    driver.findElement(By.id("Content_txtConfirmEmailAddress")).sendKeys(email);
	    String passwrd = dataTable.getData("General_Data", "Password");
	    driver.findElement(By.id("Content_txtPassword")).sendKeys(passwrd);
	    driver.findElement(By.id("Content_txtConfirmPassword")).sendKeys(passwrd);
	    driver.findElement(By.id("submit")).click();
	    waitForLoad(driver);
	        try {
	                  Thread.sleep(5000);
	           } catch (InterruptedException e) {
	                  e.printStackTrace();
	           }
	     driver.findElement(By.id("Content_tbFirstName")).sendKeys(firstname);
	     driver.findElement(By.id("Content_tbLastName")).sendKeys(lastname);
	     String addr = dataTable.getData("General_Data","Address");
	     driver.findElement(By.id("Content_tbAddress1")).sendKeys(addr);
	     String city = dataTable.getData("General_Data", "City");  
	     driver.findElement(By.id("Content_tbCity")).sendKeys(city);
	                //select state
	     WebElement elem=driver.findElement(By.id("sbState"));
	        Select statelist=new Select(elem);
	        statelist.selectByValue("NY");  //NY for New York
	        String zip = dataTable.getData("General_Data", "PostalCode");
	     driver.findElement(By.id("Content_tbZip")).sendKeys(zip);
	        //select card type
	     WebElement elem1=driver.findElement(By.id("sbCCType"));
	        Select cardlist=new Select(elem1);
	        cardlist.selectByValue("5000");
	        String namecard = dataTable.getData("General_Data", "NameonCard");
	     driver.findElement(By.id("Content_tbNameOnCard")).sendKeys(firstname+" "+lastname);
	     String cardno = dataTable.getData("General_Data", "CardNumber");
	     driver.findElement(By.id("Content_tbCCNumber")).sendKeys(cardno);
	     String cvvno = dataTable.getData("General_Data", "CVV");
	     driver.findElement(By.id("Content_tbCVVNumber")).sendKeys(cvvno);
	        //select card expiry date
	     WebElement elem2=driver.findElement(By.id("sbMonth"));
	        Select datelist=new Select(elem2);
	        datelist.selectByValue("2"); 
	        //select card expiry year
	     WebElement elem3=driver.findElement(By.id("sbYear"));
	        Select yearlist=new Select(elem3);
	        yearlist.selectByValue("2031"); 
	     driver.findElement(By.id("Content_cbAgree")).click();
	     driver.findElement(By.id("Content_cbAuthorize")).click();
	     driver.findElement(By.id("Content_btnSubscribe")).click();
	     waitForLoad(driver);
	        try {
	                  Thread.sleep(5000);
	           } catch (InterruptedException e) {
	                  e.printStackTrace();
	           }
	      String id1 = "Content_ConfirmOrderShow";
	                  waitForElementDisplay(id1);
	                  if(driver.findElement(By.xpath("//*[@id='Content_ConfirmOrderShow']")).isDisplayed())        
	           {
	                 String suba= driver.findElement(By.id("Content_lblPrice")).getText();
	                 report.updateTestLog("GMA", "Recurring amount -"+suba, Status.PASS);
	                 String subb= driver.findElement(By.id("Content_lblTaxAmount")).getText();
	                 report.updateTestLog("GMA", "Tax amount -"+subb, Status.PASS);
	                 String subc= driver.findElement(By.id("Content_lblTotalAmount")).getText();
	                 report.updateTestLog("GMA", "Total amount -"+subc, Status.PASS);
	                         driver.findElement(By.id("Content_ConfirmOrderShow")).click();
	                 waitForLoad(driver);
	                                 try {
	                                                                Thread.sleep(4000);
	                                                 } catch (InterruptedException e) {
	                                                                e.printStackTrace();
	                                                 }
	                                 String mesa= driver.findElement(By.xpath("//*[@id='form1']/section/div/h2/span")).getText();
	                                 String mesb = driver.findElement(By.xpath("//*[@id='form1']/section/div/h3")).getText();
	                                 String mesc= driver.findElement(By.xpath("//*[@id='form1']/section/div/h4")).getText();
	                                 report.updateTestLog("GMA", "Message - "+mesa +mesb +mesc, Status.PASS);
	                                 driver.findElement(By.id("Content_LetsGoButton")).click();
	                                 String id = "welcome-create-profile";
	                         waitForElementDisplay(id);
	           }
	                  else 
	                       {
	                 String error = driver.findElement(By.id("Content_lblErrormsg")).getText();
	                 report.updateTestLog("GMA", "Error message - "+error, Status.FAIL);
	                       }
	}

	public void reg_yearly_Discover() throws Exception
	{
	                driver.manage().window().maximize();
	    driver.findElement(By.xpath(".//*[@id='tryOrBuy']/span")).click();
	    try {
	           Thread.sleep(2000);
	    } catch (InterruptedException e) {
	           // TODO Auto-generated catch block
	           e.printStackTrace();
	    }
	    driver.findElement(By.xpath("html/body/form/section[2]/div[1]/div[3]/div[2]/div/a/span")).click();
	    String firstname = dataTable.getData("General_Data", "FirstName");         
	    driver.findElement(By.id("Content_txtFirstName")).sendKeys(firstname);
	    String lastname = dataTable.getData("General_Data", "LastName");           
	    driver.findElement(By.id("Content_txtLastName")).sendKeys(lastname);
	    String email = dataTable.getData("General_Data", "Email");           
	    driver.findElement(By.id("Content_txtEmail")).sendKeys(email);
	    String year = dataTable.getData("General_Data", "Year");             
	    driver.findElement(By.id("Content_txtBorn")).sendKeys(year);
	    driver.findElement(By.id("Content_chkAgree")).click();
	    driver.findElement(By.id("register")).click();
	    waitForLoad(driver);
	    try {
	              Thread.sleep(5000);
	       } catch (InterruptedException e) {
	              e.printStackTrace();
	       }
	    driver.findElement(By.id("Content_txtConfirmEmailAddress")).sendKeys(email);
	    String passwrd = dataTable.getData("General_Data", "Password");
	    driver.findElement(By.id("Content_txtPassword")).sendKeys(passwrd);
	    driver.findElement(By.id("Content_txtConfirmPassword")).sendKeys(passwrd);
	    driver.findElement(By.id("submit")).click();
	    waitForLoad(driver);
	        try {
	                  Thread.sleep(5000);
	           } catch (InterruptedException e) {
	                  e.printStackTrace();
	           }
	     driver.findElement(By.id("Content_tbFirstName")).sendKeys(firstname);
	     driver.findElement(By.id("Content_tbLastName")).sendKeys(lastname);
	     String addr = dataTable.getData("General_Data","Address");
	     driver.findElement(By.id("Content_tbAddress1")).sendKeys(addr);
	     String city = dataTable.getData("General_Data", "City");  
	     driver.findElement(By.id("Content_tbCity")).sendKeys(city);
	                //select state
	     WebElement elem=driver.findElement(By.id("sbState"));
	        Select statelist=new Select(elem);
	        statelist.selectByValue("NY");  //NY for New York
	        String zip = dataTable.getData("General_Data", "PostalCode");
	     driver.findElement(By.id("Content_tbZip")).sendKeys(zip);
	        //select card type
	     WebElement elem1=driver.findElement(By.id("sbCCType"));
	        Select cardlist=new Select(elem1);
	        cardlist.selectByValue("6000");
	        String namecard = dataTable.getData("General_Data", "NameonCard");
	     driver.findElement(By.id("Content_tbNameOnCard")).sendKeys(firstname+" "+lastname);
	     String cardno = dataTable.getData("General_Data", "CardNumber");
	     driver.findElement(By.id("Content_tbCCNumber")).sendKeys(cardno);
	     String cvvno = dataTable.getData("General_Data", "CVV");
	     driver.findElement(By.id("Content_tbCVVNumber")).sendKeys(cvvno);
	        //select card expiry date
	     WebElement elem2=driver.findElement(By.id("sbMonth"));
	        Select datelist=new Select(elem2);
	        datelist.selectByValue("12"); 
	        //select card expiry year
	     WebElement elem3=driver.findElement(By.id("sbYear"));
	        Select yearlist=new Select(elem3);
	        yearlist.selectByValue("2110"); 
	     driver.findElement(By.id("Content_cbAgree")).click();
	     driver.findElement(By.id("Content_cbAuthorize")).click();
	     driver.findElement(By.id("Content_btnSubscribe")).click();
	     waitForLoad(driver);
	        try {
	                  Thread.sleep(5000);
	           } catch (InterruptedException e) {
	                  e.printStackTrace();
	           }
	      String id1 = "Content_ConfirmOrderShow";
	                  waitForElementDisplay(id1);
	                  if(driver.findElement(By.xpath("//*[@id='Content_ConfirmOrderShow']")).isDisplayed())        
	           {
	                 String suba= driver.findElement(By.id("Content_lblPrice")).getText();
	                 report.updateTestLog("GMA", "Recurring amount -"+suba, Status.PASS);
	                 String subb= driver.findElement(By.id("Content_lblTaxAmount")).getText();
	                 report.updateTestLog("GMA", "Tax amount -"+subb, Status.PASS);
	                 String subc= driver.findElement(By.id("Content_lblTotalAmount")).getText();
	                 report.updateTestLog("GMA", "Total amount -"+subc, Status.PASS);
	                         driver.findElement(By.id("Content_ConfirmOrderShow")).click();
	                 waitForLoad(driver);
	                                 try {
	                                                                Thread.sleep(4000);
	                                                 } catch (InterruptedException e) {
	                                                                e.printStackTrace();
	                                                 }
	                                 String mesa= driver.findElement(By.xpath("//*[@id='form1']/section/div/h2/span")).getText();
	                                 String mesb = driver.findElement(By.xpath("//*[@id='form1']/section/div/h3")).getText();
	                                 String mesc= driver.findElement(By.xpath("//*[@id='form1']/section/div/h4")).getText();
	                                 report.updateTestLog("GMA", "Message - "+mesa +mesb +mesc, Status.PASS);
	                                 driver.findElement(By.id("Content_LetsGoButton")).click();
	                                 String id = "welcome-create-profile";
	                         waitForElementDisplay(id);
	           }
	                  else 
	                       {
	                 String error = driver.findElement(By.id("Content_lblErrormsg")).getText();
	                 report.updateTestLog("GMA", "Error message - "+error, Status.FAIL);
	                       }
	}

	public void reg_monthly_Amex() throws Exception
	{
	                driver.manage().window().maximize();
	    driver.findElement(By.xpath(".//*[@id='tryOrBuy']/span")).click();
	    try {
	           Thread.sleep(2000);
	    } catch (InterruptedException e) {
	           // TODO Auto-generated catch block
	           e.printStackTrace();
	    }
	    driver.findElement(By.xpath("html/body/form/section[2]/div[1]/div[1]/div/div/a/span")).click();
	    String firstname = dataTable.getData("General_Data", "FirstName");         
	    driver.findElement(By.id("Content_txtFirstName")).sendKeys(firstname);
	    String lastname = dataTable.getData("General_Data", "LastName");           
	    driver.findElement(By.id("Content_txtLastName")).sendKeys(lastname);
	    String email = dataTable.getData("General_Data", "Email");           
	    driver.findElement(By.id("Content_txtEmail")).sendKeys(email);
	    String year = dataTable.getData("General_Data", "Year");             
	    driver.findElement(By.id("Content_txtBorn")).sendKeys(year);
	    driver.findElement(By.id("Content_chkAgree")).click();
	   driver.findElement(By.id("register")).click();
	    waitForLoad(driver);
	    try {
	              Thread.sleep(5000);
	       } catch (InterruptedException e) {
	              e.printStackTrace();
	       }
	    driver.findElement(By.id("Content_txtConfirmEmailAddress")).sendKeys(email);
	    String passwrd = dataTable.getData("General_Data", "Password");
	    driver.findElement(By.id("Content_txtPassword")).sendKeys(passwrd);
	    driver.findElement(By.id("Content_txtConfirmPassword")).sendKeys(passwrd);
	    driver.findElement(By.id("submit")).click();
	    waitForLoad(driver);
	        try {
	                  Thread.sleep(5000);
	           } catch (InterruptedException e) {
	                  e.printStackTrace();
	           }
	     driver.findElement(By.id("Content_tbFirstName")).sendKeys("DOUG");
	     driver.findElement(By.id("Content_tbLastName")).sendKeys("CHASE");
	   //  String addr = dataTable.getData("General_Data","Address");
	     driver.findElement(By.id("Content_tbAddress1")).sendKeys("3 MAIN ST.");
	     String city = dataTable.getData("General_Data", "City");  
	     driver.findElement(By.id("Content_tbCity")).sendKeys(city);
	                //select state
	     WebElement elem=driver.findElement(By.id("sbState"));
	        Select statelist=new Select(elem);
	        statelist.selectByValue("NY");  //NY for New York
	     //String zip = dataTable.getData("General_Data", "PostalCode");
	     driver.findElement(By.id("Content_tbZip")).sendKeys("10453");
	        //select card type
	     WebElement elem1=driver.findElement(By.id("sbCCType"));
	        Select cardlist=new Select(elem1);
	        cardlist.selectByValue("3000");
	        String namecard = dataTable.getData("General_Data", "NameonCard");
	     driver.findElement(By.id("Content_tbNameOnCard")).sendKeys("DOUG CHASE");
	     String cardno = dataTable.getData("General_Data", "CardNumber");
	     driver.findElement(By.id("Content_tbCCNumber")).sendKeys(cardno);
	     String cvvno = dataTable.getData("General_Data", "CVV");
	     driver.findElement(By.id("Content_tbCVVNumber")).sendKeys(cvvno);
	        //select card expiry date
	     WebElement elem2=driver.findElement(By.id("sbMonth"));
	        Select datelist=new Select(elem2);
	        datelist.selectByValue("8"); 
	        //select card expiry year
	     WebElement elem3=driver.findElement(By.id("sbYear"));
	        Select yearlist=new Select(elem3);
	        yearlist.selectByValue("2016"); 
	     driver.findElement(By.id("Content_cbAgree")).click();
	     driver.findElement(By.id("Content_cbAuthorize")).click();
	     driver.findElement(By.id("Content_btnSubscribe")).click();
	     waitForLoad(driver);
	        try {
	                  Thread.sleep(5000);
	           } catch (InterruptedException e) {
	                  e.printStackTrace();
	           }
	      String id1 = "Content_ConfirmOrderShow";
	                  waitForElementDisplay(id1);
	                  if(driver.findElement(By.xpath("//*[@id='Content_ConfirmOrderShow']")).isDisplayed())        
	           {
	                 String suba= driver.findElement(By.id("Content_lblPrice")).getText();
	                 report.updateTestLog("GMA", "Recurring amount -"+suba, Status.PASS);
	                 String subb= driver.findElement(By.id("Content_lblTaxAmount")).getText();
	                 report.updateTestLog("GMA", "Tax amount -"+subb, Status.PASS);
	                 String subc= driver.findElement(By.id("Content_lblTotalAmount")).getText();
	                 report.updateTestLog("GMA", "Total amount -"+subc, Status.PASS);
	                         driver.findElement(By.id("Content_ConfirmOrderShow")).click();
	                 waitForLoad(driver);
	                                 try {
	                                                                Thread.sleep(4000);
	                                                 } catch (InterruptedException e) {
	                                                                e.printStackTrace();
	                                                 }
	                                 String mesa= driver.findElement(By.xpath("//*[@id='form1']/section/div/h2/span")).getText();
	                                 String mesb = driver.findElement(By.xpath("//*[@id='form1']/section/div/h3")).getText();
	                                 String mesc= driver.findElement(By.xpath("//*[@id='form1']/section/div/h4")).getText();
	                                 report.updateTestLog("GMA", "Message - "+mesa +mesb +mesc, Status.PASS);
	                                 driver.findElement(By.id("Content_LetsGoButton")).click();
	                                 String id = "welcome-create-profile";
	                         waitForElementDisplay(id);
	           }
	                  else 
	                       {
	                 String error = driver.findElement(By.id("Content_lblErrormsg")).getText();
	                 report.updateTestLog("GMA", "Error message - "+error, Status.FAIL);
	                       }
	
           }


	public void agevalidation() throws Exception
	{
	       
	    String firstname = dataTable.getData("General_Data", "FirstName");         
	    driver.findElement(By.id("Content_txtFirstName")).sendKeys(firstname);
	    
	    String lastname = dataTable.getData("General_Data", "LastName");           
	    driver.findElement(By.id("Content_txtLastName")).sendKeys(lastname);
	    
	    String email = dataTable.getData("General_Data", "Email");           
	    driver.findElement(By.id("Content_txtEmail")).sendKeys(email);
	    
	    String year = dataTable.getData("General_Data", "Year");             
	    driver.findElement(By.id("Content_txtBorn")).sendKeys(year);
	    
	    driver.findElement(By.id("Content_chkAgree")).click();
	    
	    driver.findElement(By.id("register")).click();
	    waitForLoad(driver);
	    try {
	              Thread.sleep(5000);
	        } 
	    catch (InterruptedException e)
	      {
	              e.printStackTrace();
	       }
	   if (isElementPresent(By.xpath("//*[@id='frmAgeValidation']/section/div/fieldset/div/p[1]/span")))
	   {
	         String ageValiadationText=driver.findElement(By.xpath("//*[@id='frmAgeValidation']/section/div/fieldset/div/p[1]/span")).getText();
	         report.updateTestLog("GMA","Age validation message - " +ageValiadationText,Status.PASS);
	   }
	   else
	   {
		   String ageValiadationfailText="Age validation not working as expected" ;
		   report.updateTestLog("GMA","Age validation message - " +ageValiadationfailText,Status.FAIL);

	   }





	}


public void cancelsub()
{
	driver.findElement(By.id("Content_gvCancelSubscription_itemSelector_0")).click();
    driver.findElement(By.id("Content_Cancel")).click();
    if(isElementPresent(By.xpath("html/body/form/div[4]/div[2]")))
       {
             driver.findElement(By.id("Content_btnCancelSubscription")).click();
             waitForLoad(driver);
           try {
                     Thread.sleep(5000);
              } catch (InterruptedException e) {
                     e.printStackTrace();
              }
            String message = driver.findElement(By.id("Content_lbErrorMsg")).getText();
              report.updateTestLog("GMA", "Cancellation message - "+message, Status.PASS);
       } 
}
   

}
   































































	
		
	

		
		
		
	
	
















	

	
	
	
	
	
