package businesscomponents;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.apache.xpath.operations.Div;
import org.openqa.selenium.*;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.internal.seleniumemulation.WaitForPageToLoad;
import org.openqa.selenium.lift.find.DivFinder;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.css.sac.SelectorList;

import com.cognizant.framework.Status;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
*  Functional Components class  
 * @author Cognizant 
 */

public class FunctionalComponents extends ReusableLibrary
{
	Actions actions = new Actions(driver);
	/**
	 * Constructor to initialize the component library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	 protected static Set setOfOldHandles = null;
		protected static Set setOfNewHandles = null;
		static WebElement  webelement;
		
	public FunctionalComponents(ScriptHelper scriptHelper)
		{
		super(scriptHelper);
		}

	
	// Saves Old Handles
	public void saveOldHandles(WebDriver driver)
			{
				if (setOfOldHandles != null)
					{
						setOfOldHandles.clear();
					}
				setOfOldHandles = driver.getWindowHandles(); // Save all the browser window ID's
				//report.updateTestLog("hai", ""+setOfOldHandles.size(), Status.DONE);
			}	
			
	// Saves New Handles
	public void saveNewHandles(WebDriver driver) 
	  {
			if (setOfNewHandles != null)
			{
				setOfNewHandles.clear();
			}
			setOfNewHandles = driver.getWindowHandles();  // Save all the browser window ID's
			
		}

	// Focus on new view		
	public void focusOnNewView(WebDriver driver)
	  {
			try 
				{
					Thread.sleep(3000);
				} 
			catch (InterruptedException e) 
				{
					// To do Auto-generated catch block
					e.printStackTrace();
				}
			if (setOfNewHandles != null) 
				{
					setOfNewHandles.removeAll(setOfOldHandles); 
					// this method removeAll() take one set and puts it in another set and if there are same
					// positions it will erase them and leaves only that are not equals
				} 
			else 
				{

					System.out.println("setOfNewHandles is null. Can't compare old and new handles. New handle may have not enough time to load and save. Maybe you should add some time to load new window by adding Thread.Sleep(3000); - wait for 3 second ");
				}

			if (!setOfNewHandles.isEmpty()) 
			{
				String newWindowHandle = (String) setOfNewHandles.iterator().next();
				//Finding current window
				driver.switchTo().window(newWindowHandle);		   
				//Focusing on current window.		
				try 
				{
					Thread.sleep(3000);
				} 
				catch (InterruptedException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				driver.manage().window().maximize();
				
			}
		}
		
	// Waits for till the element is displayed
	public void waitForElementDisplay(String elementId) throws InterruptedException 
	{
		for(int i=0; i<600; i++)
		{
		try
		{
		if(driver.findElement(By.id(elementId)).isDisplayed())
		break;
		else
			Thread.sleep(100);
		}
		catch (Exception e) 
		{
			Thread.sleep(100);
		}
		}
	}
	
	// Waits till the page is loaded
	public void waitForLoad(WebDriver driver) 
	{
	    ExpectedCondition<Boolean> pageLoadCondition = new
	        ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver driver) {
	                return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	            }
	        };
	    WebDriverWait wait = new WebDriverWait(driver, 60);
	    wait.until(pageLoadCondition);
	}
	
	// SelectOptionFromListByVisibleText	
	public void SelectOptionFromListByVisibleText(String ElementIdentifier, String OptionText)
	{
		WebElement Dropdown =  driver.findElement(By.id(ElementIdentifier));
		Select objSelect = new Select(Dropdown);
		objSelect.selectByVisibleText(OptionText);
	}
	
	// Clean up
	public void cleanUp() {
	        driver.quit();
	    }
	 
	// Identification of Text
	public WebElement ElementPresent(String Identifier)
	 {
//		 String Validation = null; 
		 try
		 {
			 WebElement Element = driver.findElement(By.xpath(Identifier));
			 return Element;
		 }
		 catch(Exception e)
		 {
			return null; 
		 }
	 }

	// Condition to verify the element is present or not
	public boolean isElementPresent(By by){
	try {
		driver.findElement(by); 
		//If element exists return true
		return true;
	} catch (Exception e) {
		//If element not exists return False.
		return false;			
		
	}
	
}
	// Using selenium.DeleteAllVisibleCookies in Webdriver

	public void DeleteAllVisibleCookies()
	{
		driver.manage().deleteAllCookies();
	}
	
	// Select value from unordered list
	public void selectValueFromUnorderedList(WebElement unorderedList, final String value) {
	    List<WebElement> options = unorderedList.findElements(By.tagName("li"));

	    for (WebElement option : options) {
	        if (value.equals(option.getText())) {
	            option.click();
	            break;
	        }
	    }
	}
	
	
	
//------------------------------------------------------------* Consumer Promo Code*----------------------------------------------------------------------------------------	

	public void defaultCodes()
	{
		driver.manage().window().maximize();
		driver.findElement(By.id("Content_txtConsumerPromoCode")).sendKeys("GOMATH03");
		driver.findElement(By.id("Content_btnRedeeem")).click();
		WebDriverWait wait = new WebDriverWait(driver,120);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("Content_txtAnswer")));
		System.out.println(driver.getCurrentUrl());
		//String verUrl = "http://v1.stg.subscriptions.hmhco.com/gomath/WorkbookVerification";
	    //String verUrl = "http://v1.stg.gomathacademy.com/gomath/WorkbookVerification";
		report.updateTestLog("GoMath","Consumer Workbook Verification page is displayed",Status.PASS);
		String query = driver.findElement(By.id("Content_lblWorkbookQuestion")).getText();
		String text[] = query.split(" ");
		String a = text[9];
		driver.findElement(By.id("Content_txtAnswer")).sendKeys(dataTable.getData("WorkBook_Questions",a));
		driver.findElement(By.id("submit")).click();				
	}

	public void systemCode() throws Exception
	{
		driver.manage().window().maximize();
		String conscode = dataTable.getData("General_Data", "Consumercode");
		driver.findElement(By.id("Content_txtConsumerPromoCode")).sendKeys(conscode);
		driver.findElement(By.id("Content_btnRedeeem")).click();
		Thread.sleep(2000);
		if(isElementPresent(By.id("Content_lblErrormsg")))
		{
		    String mes = driver.findElement(By.id("Content_lblErrormsg")).getText();
	 	    report.updateTestLog("Consumer Promo Expired","Consumer is thrown with message - "+mes, Status.DONE);			
		}
 	}

	public void preReg() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver,120);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("Content_lblBorn")));
	    //String preRegURL = "http://v1.stg.subscriptions.hmhco.com/gomath/PreRegister";
        //String preRegURL = "http://v1.stg.gomathacademy.com/gomath/PreRegister";
		report.updateTestLog("GoMath","Pre-Registration page is displayed", Status.PASS);
		String message1 = driver.findElement(By.xpath("html/body/form/section/div/fieldset/div/span/p")).getText();
		report.updateTestLog("GoMath","Free Access Message - " + message1, Status.DONE);
	    String YOB = dataTable.getData("General_Data","Year");
		driver.findElement(By.id("Content_txtBorn")).sendKeys(YOB);
		String email = dataTable.getData("General_Data","Email");
		driver.findElement(By.id("Content_txtEmail")).sendKeys(email);
		driver.findElement(By.id("Content_chkAgree")).click();
		driver.findElement(By.id("register")).click();
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		WebDriverWait wait1 = new WebDriverWait(driver,120);
//		wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("Content_txtPassword")));
		//String conRegURL = "http://v1.stg.subscriptions.hmhco.com/gomath/Consumer/Register";
		System.out.println(driver.getCurrentUrl());
		String conRegURL = "http://v1.stg.gomathacademy.com/gomath/Consumer/Register";
		if(conRegURL.contains(driver.getCurrentUrl()))
		{
			 String fname = dataTable.getData("General_Data","FirstName");
			 driver.findElement(By.id("Content_txtFirstName")).sendKeys(fname);
			 String lname = dataTable.getData("General_Data","LastName");
			 driver.findElement(By.id("Content_txtLastName")).sendKeys(lname);
			 String password = dataTable.getData("General_Data","Password");
			 driver.findElement(By.id("Content_txtPassword")).sendKeys(password);
			 driver.findElement(By.id("register")).click();
			 report.updateTestLog("Consumer Registration","User is successfully registered in GMA",Status.PASS);
			 WebDriverWait wait2 = new WebDriverWait(driver,120);
			 wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("welcome-create-profile")));
		}	
	}
	  
    public void loginfromworkbook() throws Exception
	{
		driver.manage().window().maximize();
		driver.findElement(By.xpath("html/body/header/div/p/a/span")).click();
		driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
	}

    public void consumerLogin() throws InterruptedException
	{
		//String loginURL = "http://v1.stg.subscriptions.hmhco.com/gomath/Login";
		String loginURL = "http://v1.stg.gomathacademy.com/gomath/Login";
		//String homepageurl = "http://hmhco-v1.stg.techspa.com/gomathacademy";
		String homepageurl = "http://hmhco-v1.stg.gomathacademy.com/";
		//String InstituionalUrl = "http://v1.stg.subscriptions.hmhco.com/gomath/RegisterInstitutional";
		String InstitutionalUrl = "http://v1.stg.gomathacademy.com/gomath/RegisterInstitutional";
	    System.out.println(driver.getCurrentUrl());
		if(loginURL.equals(driver.getCurrentUrl()))
		{
		   report.updateTestLog("GoMath","User Login page is displayed", Status.PASS);
		   if(driver.findElement(By.id("Content_lblErrormsg")).isDisplayed())
		   {
		        String mes = driver.findElement(By.id("Content_lblErrormsg")).getText();
    	        report.updateTestLog("Login page","Validation Message - "+mes, Status.DONE);
		   }
		   if(driver.findElement(By.id("Content_txtEmailAddress")).isEnabled())
		   {
			    String username = dataTable.getData("General_Data","Username");
			    driver.findElement(By.id("Content_txtEmailAddress")).sendKeys(username);
		   }
		   String paswrd = dataTable.getData("General_Data","Password");
	       driver.findElement(By.id("Content_txtPassword")).sendKeys(paswrd);
		   driver.findElement(By.id("btnlogin")).click();
           driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	    }
		else if(InstitutionalUrl.equals(driver.getCurrentUrl()))
		{
	        report.updateTestLog("Consumer Registration","Consumer is already registered as institutional user of HMHco.com",Status.PASS); 
			String mes = driver.findElement(By.xpath("html/body/form/section/div/fieldset/div/p/span")).getText();
			report.updateTestLog("Consumer Registration","Consumer is thrown with message-"+mes,Status.DONE);
			driver.findElement(By.id("Content_btnContinueToCheckout")).click();
			System.out.println(driver.getCurrentUrl());
			if(homepageurl.equals(driver.getCurrentUrl()))
			{
				 report.updateTestLog("Consumer","Home page is displayed",Status.PASS); 
			}
			else
			{
				report.updateTestLog("Consumer","Home page is not displayed",Status.FAIL);
			}
	    }
	    else
		{
		   report.updateTestLog("GoMath","User Login page is not displayed", Status.FAIL);
		}
	}
    
    public void trialExpired() throws InterruptedException
    {
       waitForElementDisplay("Content_LetsGoButton");
       System.out.println(driver.getCurrentUrl());
       //String trialExpUrl = "http://v1.stg.subscriptions.hmhco.com/gomath/TrialExpired";
       String trialExpUrl = "http://v1.stg.gomathacademy.com/gomath/TrialExpired";
	   if(trialExpUrl.equals(driver.getCurrentUrl()))
       {
    	    String mes1 = driver.findElement(By.xpath("html/body/form/section/div/fieldset/div/h5")).getText();
    	    String mes2 = driver.findElement(By.xpath("html/body/form/section/div/fieldset/div/div/h3")).getText();
    	    String pageTitle =  driver.getTitle();
    	    report.updateTestLog("Trial Expired", "Page Title shown is "+pageTitle, Status.DONE);
    	    report.updateTestLog("Trial Expired", "Trial expiry page is displayed", Status.PASS);
		    report.updateTestLog("Trial Expired", "Validation message in Trial expiry page is -"+mes1 +mes2, Status.DONE);
       }
	   else
	   {
		   report.updateTestLog("Trial Expired", "Trial expiry page is not displayed", Status.FAIL);
	   }
	
    }

	public void consumerAccount() throws InterruptedException
	{
		if(isElementPresent(By.id("welcome-create-profile")))
		{
		    report.updateTestLog("Consumer login","Consumer logged into GMA successfully",Status.PASS);
		    String child = dataTable.getData("General_Data","Childname");
		    driver.findElement(By.id("enter-child-name")).sendKeys(child);
		    //driver.findElement(By.id("gradeSelectBoxItText")).sendKeys("Grade 1");
		    driver.findElement(By.id("gradeSelectBoxItText")).click();
			driver.findElement(By.linkText("Grade 3")).click();
		    driver.findElement(By.id("parent-begin-lets-go")).click();
		    //WebDriverWait wait1 = new WebDriverWait(driver,120);
			//wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("ride-rocket")));
		    //Thread.sleep(5000);
		    waitForElementDisplay("ride-rocket");
			driver.findElement(By.xpath("html/body/div[3]/ng-view/div/div[3]/div/div/div/div[1]/button")).click();
			WebDriverWait wait2 = new WebDriverWait(driver,120);
			wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("todays-journey")));
			//Thread.sleep(15000);
//			waitForLoad(driver);
//			try {
//		        Thread.sleep(3000);
//		        } catch (InterruptedException e) {
//		        e.printStackTrace();
//		       }
			//waitForElementDisplay("grade-menu");
			driver.findElement(By.xpath("html/body/div[3]/ng-view/div/div[3]/div[1]/div[2]/div/button")).click();
			waitForElementDisplay("user-menu");
			//Thread.sleep(1000);
			driver.findElement(By.xpath("html/body/div[3]/ng-view/div/div[4]/div[1]/div[4]/div[3]/button")).click();
		    waitForElementDisplay("pum-close-btn");
			driver.findElement(By.xpath("html/body/div[5]/div/div/div/div[3]/div/h1")).click();
			//driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.SECONDS);
			Thread.sleep(2000);	
		 }
		 else if(isElementPresent(By.id("pum-close-btn")))
		 {
			report.updateTestLog("Consumer login","Consumer logged into GMA successfully",Status.PASS);
			driver.findElement(By.xpath("html/body/div[5]/div/div/div/div[3]/div/h1")).click();
		 }
		    driver.findElement(By.xpath("html/body/div[3]/ng-view/div/span/div/div[3]/div[3]/button")).click();
		    String paswrd = dataTable.getData("General_Data","Password");
	        driver.findElement(By.id("Content_txtPassword")).sendKeys(paswrd);
			driver.findElement(By.id("Content_btnlogin")).click();
			WebDriverWait wait = new WebDriverWait(driver,120);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("Content_lbEmail")));
			String useremail = driver.findElement(By.id("Content_lbEmail")).getText();
			report.updateTestLog("Consumer Account","Consumer Email - "+useremail,Status.DONE);
			WebElement element1 = driver.findElement(By.xpath("html/body/form/section[2]/div/div/table/tbody"));
			List<WebElement> l1 = element1.findElements(By.tagName("tr"));
			int subscount = l1.size();
			String subStatus = "Active";
		    for(int i=2;i<=subscount;i++)
		    {
	           String rowValue = null;
	           rowValue = driver.findElement(By.xpath("html/body/form/section[2]/div/div/table/tbody/tr["+i+"]/td[8]")).getText();
	           System.out.println(rowValue);
	           if(rowValue.equalsIgnoreCase(subStatus))
	            {
	               // If the sValue match with the description, it will initiate one more inner loop for all the columns of 'i' row 
	        	   report.updateTestLog("Consumer Account","Subscribed product - "+driver.findElement(By.xpath("html/body/form/section[2]/div/div/table/tbody/tr["+i+"]/td[2]")).getText(),Status.DONE);
	        	   report.updateTestLog("Consumer Account","Subscription Start date - "+driver.findElement(By.xpath("html/body/form/section[2]/div/div/table/tbody/tr["+i+"]/td[3]")).getText(),Status.DONE);
	        	   report.updateTestLog("Consumer Account","Subscription End date - "+driver.findElement(By.xpath("html/body/form/section[2]/div/div/table/tbody/tr["+i+"]/td[4]")).getText(),Status.DONE);
	        	   report.updateTestLog("Consumer Account","Subscription Order No.- "+driver.findElement(By.xpath("html/body/form/section[2]/div/div/table/tbody/tr["+i+"]/td[5]")).getText(),Status.DONE);
	        	   report.updateTestLog("Consumer Account","Subscription Amount - "+driver.findElement(By.xpath("html/body/form/section[2]/div/div/table/tbody/tr["+i+"]/td[7]")).getText(),Status.DONE);
	        	   report.updateTestLog("Consumer Account","Subscription Status - "+driver.findElement(By.xpath("html/body/form/section[2]/div/div/table/tbody/tr["+i+"]/td[8]")).getText(),Status.DONE);
	        	}
	        }
			driver.findElement(By.id("btnLogout")).click();
			report.updateTestLog("Consumer logout","Consumer logged out of GMA",Status.PASS);
	}
		
	









	}
































