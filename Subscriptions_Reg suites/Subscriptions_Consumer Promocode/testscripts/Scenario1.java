package testscripts;

import org.junit.Test;

import com.cognizant.framework.IterationOptions;

import supportlibraries.Browser;
import supportlibraries.DriverScript;


public class Scenario1 extends DriverScript
{
	@Test
	public void runTC1()
	{
		testParameters.setCurrentTestcase("TC1");
		testParameters.setIterationMode(IterationOptions.RunOneIterationOnly);
		testParameters.setBrowser(Browser.firefox);
		
		driveTestExecution();
	}
	
	
	//@Test
	public void runTC2()
	{
		testParameters.setCurrentTestcase("TC2");
		testParameters.setBrowser(Browser.chrome);
		
		driveTestExecution();
	}
	
	//@Test
	public void runTC3()
	{
		testParameters.setCurrentTestcase("TC3");
		
		driveTestExecution();
	}
}